﻿[CmdletBinding()]
Param(
	[Parameter(Mandatory=$false)]
	[string]$ComputerName = $env:COMPUTERNAME,
	[Parameter(Mandatory=$false)]
	[string]$OutputFile = "$env:windir\Temp\LastInput",
	[Parameter(Mandatory=$false)]
	[int]$Samples = 1,
	[Parameter(Mandatory=$false)]
	[int]$Frequency = 5
)

Add-Type @'

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Einstein.Scraps {

public static class UserInput {

[DllImport("user32.dll", SetLastError=false)]
private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

[StructLayout(LayoutKind.Sequential)]
private struct LASTINPUTINFO {
public uint cbSize;
public int dwTime;
}

public static DateTime LastInput {
get {

DateTime bootTime = 
DateTime.UtcNow.AddMilliseconds(-Environment.TickCount);
DateTime lastInput = 
bootTime.AddMilliseconds(LastInputTicks);
return lastInput;
}
}

public static TimeSpan IdleTime {
get {
return DateTime.UtcNow.Subtract(LastInput);
}
}

public static int LastInputTicks {
get {

LASTINPUTINFO lii = new LASTINPUTINFO();
lii.cbSize = (uint)Marshal.SizeOf(typeof(LASTINPUTINFO));

GetLastInputInfo(ref lii);

return lii.dwTime;

}
}

}

}

'@

$null = New-Item -Path $OutputFile -ItemType File -Force
$LockAppProc = $null
$LockAppHostProc = $null
$IsAdmin = ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
$ErrorActionPreference = 'Continue'
if($IsAdmin) {

    $LockAppProc = Get-Process -Name LockApp -IncludeUserName | Where-Object {$_.UserName -like $([Security.Principal.WindowsIdentity]::GetCurrent().Name)} | Select-Object -Property name,id,sessionid,starttime
    $LockAppHostProc = Get-Process -Name LockAppHost -IncludeUserName | Where-Object {$_.UserName -like $([Security.Principal.WindowsIdentity]::GetCurrent().Name)} | Sort-Object -Property StartTime -Descending | Select-Object -Property name,id,sessionid,starttime -First 1
} else {

    $LockAppProc = Get-Process -Name LockApp | Where-Object {$_.StartTime} | Select-Object -Property name,id,sessionid,starttime
    $LockAppHostProc = Get-Process -Name LockAppHost | Where-Object {$_.StartTime} | Sort-Object -Property StartTime -Descending | Select-Object -Property name,id,sessionid,starttime -First 1
}

if($Samples -gt 1) {

    for($i = 0; $i -lt $Samples; $i++) {

        [Einstein.Scraps.UserInput]::LastInput.ToLocalTime() | Out-File -FilePath $OutputFile -NoNewline -Append -Force
        
        Start-Sleep -Seconds $Frequency
    }

    "`r$([Security.Principal.WindowsIdentity]::GetCurrent().Name)" | Out-File -FilePath $OutputFile -Append -Force
    
    if($LockAppProc) {

        "TRUE" | Out-File -FilePath $OutputFile -Append -Force
        "$([Security.Principal.WindowsIdentity]::GetCurrent().Name)" | Out-File -FilePath $OutputFile -Append -Force
        "$($LockAppProc.StartTime)" | Out-File -FilePath $OutputFile -Append -Force
    } else {

        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
    }

    if($LockAppHostProc) {

        "TRUE" | Out-File -FilePath $OutputFile -Append -Force
        "$([Security.Principal.WindowsIdentity]::GetCurrent().Name)" | Out-File -FilePath $OutputFile -Append -Force
        "$($LockAppHostProc.StartTime)" | Out-File -FilePath $OutputFile -Append -Force
    } else {

        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
    }

} else {

    [Einstein.Scraps.UserInput]::LastInput.ToLocalTime() | Out-File -FilePath $OutputFile -NoNewline -Force
    "`r$([Security.Principal.WindowsIdentity]::GetCurrent().Name)" | Out-File -FilePath $OutputFile -Append -Force

    if($LockAppProc) {

        "TRUE" | Out-File -FilePath $OutputFile -Append -Force
        "$([Security.Principal.WindowsIdentity]::GetCurrent().Name)" | Out-File -FilePath $OutputFile -Append -Force
        "$($LockAppProc.StartTime)" | Out-File -FilePath $OutputFile -Append -Force

    } else {

        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
    }

    if($LockAppHostProc) {

        "TRUE" | Out-File -FilePath $OutputFile -Append -Force
        "$([Security.Principal.WindowsIdentity]::GetCurrent().Name)" | Out-File -FilePath $OutputFile -Append -Force
        "$($LockAppHostProc.StartTime)" | Out-File -FilePath $OutputFile -Append -Force
    } else {

        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
        " " | Out-File -FilePath $OutputFile -Append -Force
    }
}

#Get-LastInput