﻿<#
.SYNOPSIS
	This script performs the installation or uninstallation of an application(s).
.DESCRIPTION
	The script is provided as a template to perform an install or uninstall of an application(s).
	The script either performs an "Install" deployment type or an "Uninstall" deployment type.
	The install deployment type is broken down into 3 main sections/phases: Pre-Install, Install, and Post-Install.
	The script dot-sources the AppDeployToolkitMain.ps1 script which contains the logic and functions required to install or uninstall an application.
.PARAMETER DeploymentType
	The type of deployment to perform. Default is: Install.
.PARAMETER DeployMode
	Specifies whether the installation should be run in Interactive, Silent, or NonInteractive mode. Default is: Interactive. Options: Interactive = Shows dialogs, Silent = No dialogs, NonInteractive = Very silent, i.e. no blocking apps. NonInteractive mode is automatically set if it is detected that the process is not user interactive.
.PARAMETER AllowRebootPassThru
	Allows the 3010 return code (requires restart) to be passed back to the parent process (e.g. SCCM) if detected from an installation. If 3010 is passed back to SCCM, a reboot prompt will be triggered.
.PARAMETER TerminalServerMode
	Changes to "user install mode" and back to "user execute mode" for installing/uninstalling applications for Remote Destkop Session Hosts/Citrix servers.
.PARAMETER DisableLogging
	Disables logging to file for the script. Default is: $false.
.EXAMPLE
    powershell.exe -Command "& { & '.\Deploy-Application.ps1' -DeployMode 'Silent'; Exit $LastExitCode }"
.EXAMPLE
    powershell.exe -Command "& { & '.\Deploy-Application.ps1' -AllowRebootPassThru; Exit $LastExitCode }"
.EXAMPLE
    powershell.exe -Command "& { & '.\Deploy-Application.ps1' -DeploymentType 'Uninstall'; Exit $LastExitCode }"
.EXAMPLE
    Deploy-Application.exe -DeploymentType "Install" -DeployMode "Silent"
.NOTES
	Toolkit Exit Code Ranges:
	60000 - 68999: Reserved for built-in exit codes in Deploy-Application.ps1, Deploy-Application.exe, and AppDeployToolkitMain.ps1
	69000 - 69999: Recommended for user customized exit codes in Deploy-Application.ps1
	70000 - 79999: Recommended for user customized exit codes in AppDeployToolkitExtensions.ps1
.LINK 
	http://psappdeploytoolkit.com
#>
[CmdletBinding()]
Param (
	[Parameter(Mandatory=$false)]
	[ValidateSet('Install','Uninstall')]
	[string]$DeploymentType = 'Install',
	[Parameter(Mandatory=$false)]
	[ValidateSet('Interactive','Silent','NonInteractive')]
	[string]$DeployMode = 'Interactive',
	[Parameter(Mandatory=$false)]
	[switch]$AllowRebootPassThru = $false,
	[Parameter(Mandatory=$false)]
	[switch]$TerminalServerMode = $false,
	[Parameter(Mandatory=$false)]
	[switch]$DisableLogging = $false,
	[Parameter(Mandatory=$false)]
	[string]$OutPath = '\\server01\GetLastInput$\Device CSVs'
)

Try {
	## Set the script execution policy for this process
	Try { Set-ExecutionPolicy -ExecutionPolicy 'ByPass' -Scope 'Process' -Force -ErrorAction 'Stop' } Catch {}
	
	##*===============================================
	##* VARIABLE DECLARATION
	##*===============================================
	## Variables: Application
	[string]$appVendor = ''
	[string]$appName = 'Get Last Input Script'
	[string]$appVersion = '1.0'
	[string]$appArch = 'x86'
	[string]$appLang = 'EN'
	[string]$appRevision = '01'
	[string]$appScriptVersion = '1.0.0'
	[string]$appScriptDate = '11/09/2017'
	[string]$appScriptAuthor = 'mbado'
	##*===============================================
	## Variables: Install Titles (Only set here to override defaults set by the toolkit)
	[string]$installName = ''
	[string]$installTitle = ''
	
	##* Do not modify section below
	#region DoNotModify
	
	## Variables: Exit Code
	[int32]$mainExitCode = 0
	
	## Variables: Script
	[string]$deployAppScriptFriendlyName = 'Deploy Application'
	[version]$deployAppScriptVersion = [version]'3.6.9'
	[string]$deployAppScriptDate = '02/12/2017'
	[hashtable]$deployAppScriptParameters = $psBoundParameters
	
	## Variables: Environment
	If (Test-Path -LiteralPath 'variable:HostInvocation') { $InvocationInfo = $HostInvocation } Else { $InvocationInfo = $MyInvocation }
	[string]$scriptDirectory = Split-Path -Path $InvocationInfo.MyCommand.Definition -Parent
	
	## Dot source the required App Deploy Toolkit Functions
	Try {
		[string]$moduleAppDeployToolkitMain = "$scriptDirectory\AppDeployToolkit\AppDeployToolkitMain.ps1"
		If (-not (Test-Path -LiteralPath $moduleAppDeployToolkitMain -PathType 'Leaf')) { Throw "Module does not exist at the specified location [$moduleAppDeployToolkitMain]." }
		If ($DisableLogging) { . $moduleAppDeployToolkitMain -DisableLogging } Else { . $moduleAppDeployToolkitMain }
	}
	Catch {
		If ($mainExitCode -eq 0){ [int32]$mainExitCode = 60008 }
		Write-Error -Message "Module [$moduleAppDeployToolkitMain] failed to load: `n$($_.Exception.Message)`n `n$($_.InvocationInfo.PositionMessage)" -ErrorAction 'Continue'
		## Exit the script, returning the exit code to SCCM
		If (Test-Path -LiteralPath 'variable:HostInvocation') { $script:ExitCode = $mainExitCode; Exit } Else { Exit $mainExitCode }
	}
	
	#endregion
	##* Do not modify section above
	##*===============================================
	##* END VARIABLE DECLARATION
	##*===============================================
		
	If ($deploymentType -ine 'Uninstall') {
		##*===============================================
		##* PRE-INSTALLATION
		##*===============================================
		[string]$installPhase = 'Pre-Installation'
		
		## Show Welcome Message, close Internet Explorer if required, allow up to 3 deferrals, verify there is enough disk space to complete the install, and persist the prompt
		#Show-InstallationWelcome -CloseApps 'iexplore' -AllowDefer -DeferTimes 3 -CheckDiskSpace -PersistPrompt
		
		## Show Progress Message (with the default message)
		Show-InstallationProgress "Installing $installTitle, please wait..."
		
		## <Perform Pre-Installation tasks here>

        $LastInput_Share = $OutPath
        $LastInput_CSV = "$env:TEMP\Get-LastInput_$env:COMPUTERNAME.csv"
        $null = New-Item -Path $LastInput_CSV -ItemType File -Force

		$LoggedOnUsers = Get-LoggedOnUser

		##*===============================================
		##* INSTALLATION 
		##*===============================================
		[string]$installPhase = 'Installation'
		
		## Handle Zero-Config MSI Installations
		If ($useDefaultMsi) {
			[hashtable]$ExecuteDefaultMSISplat =  @{ Action = 'Install'; Path = $defaultMsiFile }; If ($defaultMstFile) { $ExecuteDefaultMSISplat.Add('Transform', $defaultMstFile) }
			Execute-MSI @ExecuteDefaultMSISplat; If ($defaultMspFiles) { $defaultMspFiles | ForEach-Object { Execute-MSI -Action 'Patch' -Path $_ } }
		}
		
		## <Perform Installation tasks here>

        foreach($User in $LoggedOnUsers) {

            if(Test-Path $LastInput_CSV -ErrorAction SilentlyContinue) {

                $LastInput_CSV_Content = Import-Csv $LastInput_CSV
            }

            $LastInput_File = "$env:windir\Temp\LastInput_$($User.UserName)"

            Execute-ProcessAsUser -Path 'powershell.exe' -Parameters "-NoLogo -NonInteractive -NoProfile -ExecutionPolicy Bypass -File `"$dirSupportFiles\Get-LastInput.ps1`" -OutputFile $LastInput_File" -RunLevel HighestAvailable -Wait -UserName $User.NTAccount
                
            $LastInput_Content = Get-Content -Path $LastInput_File
            $LastInput_Time = Get-Date -Date $LastInput_Content[0]
            $LastInput_UserName = $LastInput_Content[1]
            $LastInput_RetrievalTime = Get-Date
            $LastInput_LockApp = $LastInput_Content[2]
            $LastInput_LockAppUser = $LastInput_Content[3]
            $LastInput_LockAppSince = $LastInput_Content[4]
            $LastInput_LockAppHost = $LastInput_Content[5]
            $LastInput_LockAppHostUser = $LastInput_Content[6]
            $LastInput_LockAppHostSince = $LastInput_Content[7]

            $LastInput_hashtable = [pscustomobject]@{
                'Computer Name' = $env:COMPUTERNAME
                'Last Input Retrieval Time' = $LastInput_RetrievalTime.ToString()
                'Last Input User' = $LastInput_UserName
                'Last Input Time' = $LastInput_Time.ToString()
                LockApp = $LastInput_LockApp
                LockAppUser = $LastInput_LockAppUser
                LockAppSince = $LastInput_LockAppSince
                LockAppHost = $LastInput_LockAppHost
                LockAppHostUser = $LastInput_LockAppHostUser
                LockAppHostSince = $LastInput_LockAppHostSince
                SID = $User.SID
                Username = $User.UserName
                DomainName = $User.DomainName
                SessionID = $User.SessionId
                SessionName = $User.SessionName
                ConnectState = $User.ConnectState
                IsCurrentSession = $User.IsCurrentSession
                IsConsoleSession = $User.IsConsoleSession
                IsActiveSession = $User.IsActiveUserSession
                IsUserSession = $User.IsUserSession
                IsLocalAdmin = $User.IsLocalAdmin
                LogonTime = $User.LogonTime
                IdleTime = $User.IdleTime
                DisconnectTime = $User.DisconnectTime
                ClientName = $User.ClientName
                ClientProtocolType = $User.ClientProtocolType
                ClientDirectory = $User.ClientDirectory
                ClientBuildNumber = $User.ClientBuildNumber
            }

            $Modified = $false

            if($LastInput_CSV_Content) {
                $Output = foreach($row in $LastInput_CSV_Content) {

                    if(($row.'Computer Name' -eq $env:COMPUTERNAME) -and ($row.Username -eq $User.UserName)) {

                        $Modified = $true
                        $row = $LastInput_hashtable
                    }

                    $row
                }
            }

            if($Modified -eq $false) {

                $LastInput_hashtable | Export-Csv -Path $LastInput_CSV -NoTypeInformation -Append -ErrorAction Continue

                # Create Registry Keys
                $RegKeyPath = "HKLM:\SOFTWARE\LastInputReporting\$($User.UserName)"
                if(Test-Path -Path $RegKeyPath -ErrorAction SilentlyContinue) {
                    Remove-RegistryKey -Key $RegKeyPath -Recurse
                }
                foreach($NoteProperty in ($LastInput_hashtable.psobject.Members | Where-Object {$_.MemberType -eq 'NoteProperty'})) {
                    Set-RegistryKey -Key $RegKeyPath -Name $NoteProperty.Name -Value $NoteProperty.Value -Type String
                }
                

            } else {

                $Output | Export-Csv -Path $LastInput_CSV -NoTypeInformation -ErrorAction Continue
            }
            

            #$LastInput_hashtable | Export-Csv -Path $LastInput_CSV -NoTypeInformation -Append
        }

		##*===============================================
		##* POST-INSTALLATION
		##*===============================================
		[string]$installPhase = 'Post-Installation'
		
		## <Perform Post-Installation tasks here>

        Copy-File -Path $LastInput_CSV -Destination $LastInput_Share
        Remove-File -Path "$env:windir\Temp\LastInput*"
        Remove-File -Path $LastInput_CSV

		## Display a message at the end of the install
		If (-not $useDefaultMsi) { 
            #Show-InstallationPrompt -Message 'You can customize text to appear at the end of an install or remove it completely for unattended installations.' -ButtonRightText 'OK' -Icon Information -NoWait 
        }
	}
	ElseIf ($deploymentType -ieq 'Uninstall')
	{
		##*===============================================
		##* PRE-UNINSTALLATION
		##*===============================================
		[string]$installPhase = 'Pre-Uninstallation'
		
		## Show Welcome Message, close Internet Explorer with a 60 second countdown before automatically closing
		#Show-InstallationWelcome -CloseApps 'iexplore' -CloseAppsCountdown 60
		
		## Show Progress Message (with the default message)
		Show-InstallationProgress "Uninstalling $installTitle, please wait..."
		
		## <Perform Pre-Uninstallation tasks here>
		
		
		##*===============================================
		##* UNINSTALLATION
		##*===============================================
		[string]$installPhase = 'Uninstallation'
		
		## Handle Zero-Config MSI Uninstallations
		If ($useDefaultMsi) {
			[hashtable]$ExecuteDefaultMSISplat =  @{ Action = 'Uninstall'; Path = $defaultMsiFile }; If ($defaultMstFile) { $ExecuteDefaultMSISplat.Add('Transform', $defaultMstFile) }
			Execute-MSI @ExecuteDefaultMSISplat
		}
		
		# <Perform Uninstallation tasks here>
		#Execute-Process -FilePath "$dirFiles\" -Parameters ""
		#Remove-MSIApplications ""	
		
		##*===============================================
		##* POST-UNINSTALLATION
		##*===============================================
		[string]$installPhase = 'Post-Uninstallation'
		
		## <Perform Post-Uninstallation tasks here>
		
		
	}
	
	##*===============================================
	##* END SCRIPT BODY
	##*===============================================
	
	## Call the Exit-Script function to perform final cleanup operations
	Exit-Script -ExitCode $mainExitCode
}
Catch {
	[int32]$mainExitCode = 60001
	[string]$mainErrorMessage = "$(Resolve-Error)"
	Write-Log -Message $mainErrorMessage -Severity 3 -Source $deployAppScriptFriendlyName
	Show-DialogBox -Text $mainErrorMessage -Icon 'Stop'
	Exit-Script -ExitCode $mainExitCode
}