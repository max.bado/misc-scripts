﻿# Parse the SMART data retrieved (and output to CSV) by the Get-SMARTData function
[CmdletBinding()]
param (
  [Parameter(Mandatory = $false)]
  [ValidateNotNullOrEmpty()]
  [String]$SMARTDataShare = '\\server01\SMARTData$'
)

$CSVs = Get-ChildItem -Path $SMARTDataShare
$SMARTDataList = @()

$SmartDataList = foreach($CSV in $CSVs) {
    $Name = $CSV.Name.split('_')[0]
    $CSVContent = Import-Csv -Path $CSV.FullName

    $SMARTDataObject = [pscustomobject]@{
        Name = $Name
        #SMARTDATA = $CSVContent
        TotalReadGB = [int]($CSVContent | Where-Object {$_.ATTRIBUTE_NAME -eq 'Total_LBAs_Read'}).RAW_VALUE
        TotalWriteGB = [int]($CSVContent | Where-Object {$_.ATTRIBUTE_NAME -eq 'Total_LBAs_Written'}).RAW_VALUE
    }

    $SMARTDataObject
}

$SMARTDataList