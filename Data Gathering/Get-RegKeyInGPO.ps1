﻿# Check for the presence of a registyry key preference value within a domain's GPOs
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$RegKey = 'HKEY_LOCAL_MACHINE\software\policies\NetSupport', 

    [Parameter(Mandatory = $false)]    
    [String]$RegKeyValue
)

$GPOs = Get-GPO -All

$MatchingGPOs = $GPOs | ForEach-Object {
    $GPO = $_
    $Result = $null

    Write-Host "Working on $($GPO.DisplayName)"    
    
    if ($RegKeyValue) {
        $Result = Get-GPRegistryValue -ValueName $RegKeyValue -Name $_.DisplayName -Key 'HKEY_LOCAL_MACHINE\software\policies\NetSupport' -ErrorAction SilentlyContinue
    } else {
        $Result = Get-GPRegistryValue -Name $_.DisplayName -Key 'HKEY_LOCAL_MACHINE\software\policies\NetSupport' -ErrorAction SilentlyContinue
    }

    if ($Result) {
        return $GPO
    }
}

$MatchingGPOs.DisplayName