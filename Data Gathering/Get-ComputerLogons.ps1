﻿function Get-ComputerLogons {
    # Retrieve a list of logons for a computer based on the UserTrack SQL database
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [String]$ComputerName,
        [Parameter(Mandatory = $false)]
        [datetime]$StartDate = '1/1/2017',
        [Parameter(Mandatory = $false)]
        [String]$Path,
        [Parameter(Mandatory = $false)]
        [String]$SQLServer,
        [Parameter(Mandatory = $false)]
        [String]$Database = 'usertrack',
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential,
        [Parameter(Mandatory = $false)]
        [Switch]$Unique,
        [Parameter(Mandatory = $false)]
        [Switch]$Latest
    )

    if (!(Get-Module -Name SqlServer -ErrorAction SilentlyContinue)) {
        Import-Module -Name SqlServer
    }

    if($Latest) {
        $Query = "SELECT TOP(1) * FROM logtable WHERE computer_name = '$ComputerName' ORDER BY date DESC"
    } else {
        $Query = "SELECT * FROM logtable WHERE computer_name = '$ComputerName' ORDER BY date DESC"
    }

    Invoke-Sqlcmd -ServerInstance $SQLServer -Database $Database -Query $Query #| Out-GridView
}