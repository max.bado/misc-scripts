﻿# Get CPU usage for one or more processes on a computer
param (
    [Parameter(Mandatory = $false)]
    [String]$ComputerName = $env:COMPUTERNAME,
    [Parameter(Mandatory = $false)]
    [String]$ProcessName,
    [Parameter(Mandatory = $false)]
    [String]$ProcessId
)

#############################################################################
# Option A: This is if you just have the name of the process; partial name OK

# Option B: This is for if you just have the PID; it will get the name for you

if ($ProcessName -and $ProcessId) {
    Write-Host "Please specify either the Process Name or the Process ID, not both"
    break
}

Invoke-Command -ComputerName $ComputerName -ArgumentList $ProcessName, $ProcessId -ScriptBlock {
    $ProcessName = $args[0]
    $ProcessId = $args[1]

    $CpuCores = (Get-WMIObject Win32_ComputerSystem).NumberOfLogicalProcessors
    if ($ProcessId) {
        $ProcessName = Get-Process -Id $ProcessId | Select-Object -ExpandProperty Name
        $Samples = (Get-Counter "\Process($ProcessName)\% Processor Time").CounterSamples
    } else {
        $Samples = (Get-Counter "\Process($ProcessName*)\% Processor Time").CounterSamples
    }
    
    $Samples | Select-Object -Property InstanceName, @{Name = "CPU %"; Expression = { [Decimal]::Round(($_.CookedValue / $CpuCores), 2) } }

}
