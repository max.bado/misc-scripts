﻿function Get-UserHomeFolderSize {
    # Retrieve a user's home folder size
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [String]$UserName,
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential
    )

    . "$PSScriptRoot\Get-UserInfo.ps1"
    
    $UserInfo = Get-UserInfo -UserName $UserName
    if($UserInfo) {
        $HomeDirectoryRootShare = Split-Path -Path $UserInfo.HomeDirectory
        try {
            # Try mounting the share as the current account
            $HomeDirectoryRootSharePSDrive = New-PSDrive -Name 'HomeDirectoryRootShare' -PSProvider FileSystem -Root $HomeDirectoryRootShare
        } catch {
            try {
                # Try mounting the share as the account provided in the Credential parameter
                $HomeDirectoryRootSharePSDrive = New-PSDrive -Name 'HomeDirectoryRootShare' -PSProvider FileSystem -Root $HomeDirectoryRootShare -Credential $Credential
            } catch {
                # Unable to mount the share. Stopping.
                break
            }
        }
    } else {
        # No user info returned. Stopping.
        break
    }

    if(Test-Path -Path $UserInfo.HomeDirectory -ErrorAction SilentlyContinue) {
        $HomeDirectoryList = Get-ChildItem -Path $UserInfo.HomeDirectory -Recurse -Force
        $HomeDirectorySizeGB = ($HomeDirectoryList | Measure-Object -Property Length -Sum).Sum / 1GB
    } else {
        # Home Directory doesn't exist. Stopping.
        Write-Warning -Message "$($UserInfo.SamAccountName)'s HomeDirectory '$($UserInfo.HomeDirectory)' does not exist."
        break
    }

    Write-Host "$($UserInfo.SamAccountName) Home Folder size in GB:"
    return $HomeDirectorySizeGB
}