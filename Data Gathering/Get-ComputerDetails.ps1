﻿function Get-ComputerDetails {
    # Retrieve various details about a computer. Uses a combination of ConfigMgr, UserTrack SQL table, and AD
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $false)]
        [String[]]$ComputerName,
        [Parameter(Mandatory = $false)]
        [Switch]$TryPing,
        [Parameter(Mandatory = $false)]
        [String]$CMServer,
        [Parameter(Mandatory = $false)]
        [String]$CMSite,
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential,
        [Parameter(Mandatory = $false)]
        [Switch]$NoGather,
        [Parameter(Mandatory = $false)]
        [String]$CMCollection,
        [Parameter(Mandatory = $false)]
        [Switch]$PrintOutput,
        #[Parameter(Mandatory = $false)]
        #[Switch]$LatestLogon,
        [Parameter(Mandatory = $false)]
        [Switch]$NoPrimaryUser


    )

    #if ($LatestLogon) {
    #    . "$PSScriptRoot\Get-ComputerLogons.ps1"
    #}

    if (($ComputerName | Measure-Object).Count -eq 1) {
        $ComputerName = @($ComputerName)
    }

    $CimSession = New-CimSession -ComputerName $CMServer

    if (!$NoGather -or
        (!$Global:ComputerDetailsList -or !$Global:MemoryDetailsList -or !$Global:OSList -or !$Global:UserAffinityList -or !$Global:CombinedDeviceResourcesList -or
            !$Global:OnlineStatusList -or !$Global:ADComputerList)) {
        Write-Verbose "Retreiving ComputerDetailsList"
        $Global:ComputerDetailsList = Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT ResourceID,Manufacturer,Model,Name FROM SMS_G_System_COMPUTER_SYSTEM"
        $Global:ComputerDetailsHash = @{ }
        foreach ($Computer in $ComputerDetailsList) {
            $ComputerDetailsHash[$Computer.Name] = $Computer
        }
        Write-Verbose "Retreiving MemoryDetailsList"
        $Global:MemoryDetailsList = Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT ResourceID, TotalPhysicalMemory FROM SMS_G_System_X86_PC_Memory"
        $Global:MemoryDetailsHash = @{ }
        foreach ($Computer in $MemoryDetailsList) {
            $MemoryDetailsHash[$Computer.ResourceID] = $Computer
        }
        Write-Verbose "Retreiving OSList"
        $Global:OSList = (Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_G_System_OPERATING_SYSTEM")
        $Global:OSHash = @{ }
        foreach ($OS in $OSList) {
            $OSHash[$OS.ResourceID] = $OS
        }
        Write-Verbose "Retreiving UserAffinityList"
        $Global:UserAffinityList = Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT ResourceName,UniqueUserName,Types FROM SMS_UserMachineRelationship"
        $Global:UserAffinityHash = @{ }
        foreach ($UserAffinity in $UserAffinityList) {
            if ($UserAffinityHash[$UserAffinity.ResourceName]) {
                $UserAffinityHash[$UserAffinity.ResourceName] += $UserAffinity
            } else {
                $UserAffinityHash[$UserAffinity.ResourceName] = @($UserAffinity)
            }
            
        }
        Write-Verbose "Retreiving CombinedDeviceResourcesList"
        $Global:CombinedDeviceResourcesList = (Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT ResourceID,LastPolicyRequest,LastActiveTime,LastLogonUser,CurrentLogonUser FROM SMS_CombinedDeviceResources")
        $Global:CombinedDeviceResourcesHash = @{ }
        foreach ($CombinedDeviceResource in $CombinedDeviceResourcesList) {
            $CombinedDeviceResourcesHash[$CombinedDeviceResource.ResourceID] = $CombinedDeviceResource
        }
        Write-Verbose "Retreiving OnlineStatusList"
        $Global:OnlineStatusList = (Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT ResourceID,LastStatusTime,OnlineStatus FROM SMS_CN_ClientStatus")
        $Global:OnlineStatusHash = @{ }
        foreach ($OnlineStatus in $OnlineStatusList) {
            $OnlineStatusHash[$OnlineStatus.ResourceID] = $OnlineStatus
        }
        Write-Verbose "Retreiving ADComputerList"
        $Global:ADComputerList = Get-ADComputer -Filter *
        $Global:ADComputerHash = @{ }
        foreach ($ADComputer in $ADComputerList) {
            $ADComputerHash[$ADComputer.Name] = $ADComputer
        }

        if (!$NoPrimaryUser) {
            Write-Verbose "Retreiving ADUserList"

            # Retrieve Domain list
            if (!$Global:DomainList) {
                $DomainInfo = Get-ADDomain    
                $Global:DomainList = @(
                    [pscustomobject]@{
                        NetBIOSName = $DomainInfo.NetBIOSName
                        DNSName     = $DomainInfo.DNSRoot
                    }
                )
                
                $Global:DomainList += foreach ($ChildDomain in $DomainInfo.ChildDomains) {
                    $DomainInfo = Get-ADDomain -Identity $ChildDomain
                    [pscustomobject]@{
                        NetBIOSName = $DomainInfo.NetBIOSName
                        DNSName     = $DomainInfo.DNSRoot
                    }
                }    
            }

            $Global:ADUserList = foreach ($Domain in $Global:DomainList.DNSName) {
                Get-ADUser -Filter * -Properties Name, DisplayName -Server $Domain
            }        
            $Global:ADUserHash = @{ }
            foreach ($ADUser in $ADUserList) {
                $ADUserHash[$ADUser.SamAccountName] = $ADUser
            }
        }


    }


    if ($CMCollection) {
        $CollectionID = Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT CollectionID FROM SMS_Collection WHERE Name = '$CMCollection'" | Select-Object -ExpandProperty CollectionID
        $ComputerName = Get-CimInstance -CimSession $CimSession -Namespace "root\SMS\Site_$CMSite" -Query "SELECT * FROM SMS_FullCollectionMembership WHERE CollectionID = '$CollectionID'" | Select-Object -ExpandProperty Name
    }

    <#
    if ($LatestLogon) {
        Write-Verbose "Retreiving SystemConsoleUsage"
        $RIDList = foreach ($Name in $ComputerName) {
            $ComputerDetails = $ComputerDetailsHash[$Name]
            $ComputerDetails.ResourceID
        }

        $QueryString = "SELECT * FROM SMS_GH_System_SYSTEM_CONSOLE_USAGE WHERE "
        for ($i = 0; $i -lt $RIDList.Count; $i++) {
            if ($i -eq 0) {
                $QueryString += "ResourceID = $($RIDList[$i]) "
            } else {
                $QueryString += "OR ResourceID = $($RIDList[$i]) "
            }
        }

        $Global:SystemConsoleUsageList = Get-CimInstance -ComputerName $CMServer -Namespace "root\SMS\Site_$CMSite" -Query $QueryString
        $Global:SystemConsoleUsageHash = @{}
        foreach ($SystemConsoleUsage in $SystemConsoleUsageList) {
            if ($SystemConsoleUsageHash[[int]$SystemConsoleUsage.ResourceID]) {
                $SystemConsoleUsageHash[[int]$SystemConsoleUsage.ResourceID] += $SystemConsoleUsage
            } else {
                $SystemConsoleUsageHash[[int]$SystemConsoleUsage.ResourceID] = @($SystemConsoleUsage)
            }
        }
    }
    #>

    foreach ($Computer in $ComputerName) {
        Write-Verbose "    Working on $Computer"
        $ComputerDetails = $null
        $ComputerModel = $null
        $ResourceID = $null
        $OS = $null
        $UserAffinity = $null
        $PrimaryUser = $null
        $PrimaryUserDetails = $null
        #$AssetOwnerDetails = $null
        $MemoryDetails = $null
        $LastPolicyRequest = $null
        $LastActivity = $null
        $LastLogonUser = $null
        $CurrentUser = $null
        $OnlineStatus = $null
        $OnlineStatusTime = $null
        $ADComputer = $null
        $ADUser = $null
        $PingResponse = $null
         
        Write-Verbose "    Retreiving ComputerDetails"
        try {
            $ComputerDetails = $ComputerDetailsHash[$Computer]    
        } catch { }
        
        Write-Verbose "    Retreiving ComputerModel"
        try {
            $ComputerModel = $ComputerDetails.Model
        } catch { }
        
        Write-Verbose "    Retreiving ResourceID"
        try {
            $ResourceID = $ComputerDetails.ResourceID
        } catch { }
        
        Write-Verbose "    Retreiving OS"
        try {
            $OS = $OSHash[$ResourceID]
        } catch { }
        
        Write-Verbose "    Retreiving UserAffinity"
        try {
            $UserAffinity = $UserAffinityHash[$Computer]
        } catch { }

        if (!$NoPrimaryUser) {
            Write-Verbose "    Retreiving PrimaryUser"
            try {
                $PrimaryUser = ($UserAffinity | Where-Object { $_.Types -in '1' }).UniqueUserName
            } catch { }

            try {
                $PrimaryUserDetails = foreach ($PU in $PrimaryUser) {
                    $ADUserHash[$PrimaryUser.split('\')[1]]
                }
                    
            } catch { }
        }

        Write-Verbose "    Retreiving MemoryDetails"
        try {
            $MemoryDetails = $MemoryDetailsHash[$ResourceID].TotalPhysicalMemory
        } catch { }
        
        Write-Verbose "    Retreiving LastPolicyRequest"
        try {
            $LastPolicyRequest = $CombinedDeviceResourcesHash[$ResourceID].LastPolicyRequest
            $LastActivity = $CombinedDeviceResourcesHash[$ResourceID].LastActiveTime
        } catch { }
        
        Write-Verbose "    Retreiving LastLogonUser and CurrentUser"
        try {
            $LastLogonUser = $CombinedDeviceResourcesHash[$ResourceID].LastLogonUser
            $CurrentUser = $CombinedDeviceResourcesHash[$ResourceID].CurrentLogonUser
        } catch { }
        
        Write-Verbose "    Retreiving OnlineStatus"
        try {
            $OnlineStatus = $OnlineStatusHash[$ResourceID].OnlineStatus
            $OnlineStatusTime = $OnlineStatusHash[$ResourceID].LastStatusTime
        } catch { }
        
        Write-Verbose "    Retreiving ADComputer"
        try {
            $ADComputer = $ADComputerHash[$Computer]
        } catch { }

        Write-Verbose "    Done retrieving properties"

        #. "$PSScriptRoot\Get-IdleTime.ps1"

        if ($TryPing) {
            $PingResponse = ping $Computer -4 -n 1
        }
        
        if (!$NoPrimaryUser) {
            if ($PrimaryUser) {
                $objComputer = [pscustomobject]@{
                    'ComputerName'                 = $Computer
                    'Model'                        = $ComputerModel
                    'TotalMemory'                  = $MemoryDetails
                    'PrimaryUser'                  = $PrimaryUser
                    'PrimaryUserDisplayName'       = $PrimaryUserDetails.DisplayName
                    'PrimaryUserName'              = $PrimaryUserDetails.Name
                    'PrimaryUserDistinguishedName' = $PrimaryUserDetails.DistinguishedName
                    'DistinguishedName'            = $ADComputer.DistinguishedName
                    'OSName'                       = $OS.Caption
                    'Version'                      = $OS.Version
                    'LastPolicyRequest'            = $LastPolicyRequest
                    'LastActivity'                 = $LastActivity
                    'LastLogonUser'                = $LastLogonUser
                    'CurrentUser'                  = $CurrentUser
                    'OnlineStatus'                 = $OnlineStatus
                    'OnlineStatusTime'             = $OnlineStatusTime
                    'LastBootUpTime'               = $OS.LastBootUpTime
                    'InstallDate'                  = $OS.InstallDate
                    'WindowsDirectory'             = $OS.WindowsDirectory
                    'PingResponse'                 = $PingResponse
                }
            } else {
                $objComputer = [pscustomobject]@{
                    'ComputerName'                 = $Computer
                    'Model'                        = $ComputerModel
                    'TotalMemory'                  = $MemoryDetails
                    'PrimaryUser'                  = $null
                    'PrimaryUserDisplayName'       = $null
                    'PrimaryUserName'              = $null
                    'PrimaryUserDistinguishedName' = $null
                    'DistinguishedName'            = $ADComputer.DistinguishedName
                    'OSName'                       = $OS.Caption
                    'Version'                      = $OS.Version
                    'LastPolicyRequest'            = $LastPolicyRequest
                    'LastActivity'                 = $LastActivity
                    'LastLogonUser'                = $LastLogonUser
                    'CurrentUser'                  = $CurrentUser
                    'OnlineStatus'                 = $OnlineStatus
                    'OnlineStatusTime'             = $OnlineStatusTime
                    'LastBootUpTime'               = $OS.LastBootUpTime
                    'InstallDate'                  = $OS.InstallDate
                    'WindowsDirectory'             = $OS.WindowsDirectory
                    'PingResponse'                 = $PingResponse
                }
            }   
        } else {
            $objComputer = [pscustomobject]@{
                'ComputerName'      = $Computer
                'Model'             = $ComputerModel
                'TotalMemory'       = $MemoryDetails
                'DistinguishedName' = $ADComputer.DistinguishedName
                'OSName'            = $OS.Caption
                'Version'           = $OS.Version
                'LastPolicyRequest' = $LastPolicyRequest
                'LastActivity'      = $LastActivity
                'LastLogonUser'     = $LastLogonUser
                'CurrentUser'       = $CurrentUser
                'OnlineStatus'      = $OnlineStatus
                'OnlineStatusTime'  = $OnlineStatusTime
                'LastBootUpTime'    = $OS.LastBootUpTime
                'InstallDate'       = $OS.InstallDate
                'WindowsDirectory'  = $OS.WindowsDirectory
                'PingResponse'      = $PingResponse
            }
        }
     

        ## This method isn't super accurate...
        <#
        if ($LatestLogon) {
            $LatestLogonDetails = $null
            $LatestLogonDetails = Get-ComputerLogons -ComputerName $Computer -Latest

            $ConsoleUsageItems = $SystemConsoleUsageHash[[int]$ResourceID]
            $LatestConsoleUsage = $ConsoleUsageItems | Where-Object {$_.RevisionID -eq 1} | Sort-Object -Property TimeStamp -Descending |
            Select-Object -First 1

            if ($LatestLogonDetails) {
                # Add-Member -InputObject $objComputer -MemberType NoteProperty -Name LatestLogonUser -Value $LatestLogonDetails.user_name.trim()
                # Add-Member -InputObject $objComputer -MemberType NoteProperty -Name LatestLogonDate -Value $LatestLogonDetails.date

                Add-Member -InputObject $objComputer -MemberType NoteProperty -Name LatestLogonUser -Value $LatestConsoleUsage.TopConsoleUser
                Add-Member -InputObject $objComputer -MemberType NoteProperty -Name LatestLogonDate -Value $LatestConsoleUsage.TimeStamp
            }
        }
        #>

        if ($PrintOutput) {
            Write-Host $objComputer
            $objComputer
        } else {
            $objComputer
        }

    }

    Remove-CimSession -CimSession $CimSession
}