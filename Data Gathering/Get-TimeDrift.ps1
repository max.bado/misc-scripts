﻿## This is a quick and dirty script I wrote to report on the time difference between a PC and the Domain Controller.
## It will look for events in the event log which recorded the machine changing the time by a significant amount (more than 5 minutes).
## The logic is designed to ignore timejump events which occured close to the machine waking up or powering on.
## The script also reports on the difference between the current system time and the current time on the DC.
## All this info is haphazardly organized and put into a .txt file which is then placed on an accessible share.
## Feel free to do what you want with it.

[CmdletBinding()]
param (
    ## Set a path to a share that's openly accessible by whatever account will be running this script.
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$LogPath = '\\server01\TimeReport$', 
    
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$ComputerName = $env:COMPUTERNAME, 

    ## Pass this switch to temporarily resync the system time with the DC
    [Parameter(Mandatory = $false)]
    [switch]$Resync       
)

$colLogs = @()

$crazyInvisibleCharacter = '‎'

## Create a hash of log properties to search for
$hashprops = @{
    ProviderName = 'Microsoft-Windows-Security-Auditing'
    StartTime    = (Get-Date).AddDays(-4)
    ID           = 4616
}

$logs = Get-WinEvent -FilterHashtable $hashprops -ComputerName $ComputerName

foreach ($log in $logs) {

    ## When grabbing time strings from event logs, you need to be careful because they often contain invsible Unicode characters.
    ## If a string has invisible Unicode characters, you won't be able to properly cast it to a [datetime] object.
    ## The reason for the .Replace('‎','') at the end of this string manipulation is that we're actually replacing
    ## Unicode character [U+200E], also known as "Left-To-Right Mark," with nothing. If you try to move your cursor between the
    ## first set of '‎', you'll notice that it actually takes 3 taps to move across those characters instead of 2.

    [datetime]$prevTime = ($log.Message[($log.Message.IndexOf('Previous'))..($log.Message.IndexOf('New') - 1)] -join '').Split('')[3].Replace($crazyInvisibleCharacter, '') 
    [datetime]$newTime = ($log.Message[($log.Message.IndexOf('New'))..($log.Message.IndexOf('New') + ($log.Message[$log.Message.IndexOf('New')..($log.Message.Length)] -join '').indexof('Z'))] -join '').Split('')[3].Replace($crazyInvisibleCharacter, '')

    ## We want to make sure that the time jump event that we found isn't being caused by the PC waking up from sleep or powering on.
    ## We create a hash of properties to search for a Wake Up event that's within 5 minutes of the timejump event.
    $wakeupHashProp = @{
        ProviderName = 'Microsoft-Windows-Kernel-Power'
        StartTime    = $prevTime.AddMinutes(-5)
        ID           = 107
    }

    Write-Output "Looking for Wake Up Event around $($wakeupHashProp.StartTime)"
    $wakeupLog = Get-WinEvent -FilterHashtable $wakeupHashProp | Select-Object -First 1

    $wakeupTimeSpan = $null
    $wakeupTimeSpan = New-TimeSpan -Start $wakeupLog.TimeCreated -End $prevTime
    
    if (($wakeupTimeSpan -ne $null) -and (($wakeupTimeSpan.TotalMinutes -lt 5) -or ($wakeupTimeSpan.TotalMinutes -gt -5))) {
        Write-Output "Current timejump event is within 5 minutes of a Wake From Sleep event. Ignoring"

    } else {
        $timespan = New-TimeSpan -Start $prevTime -End $newTime

        if (($timespan.TotalMinutes -gt 5) -or ($timespan.TotalMinutes -lt -5)) {
            $oLogs = New-Object -TypeName psobject -Property @{
                TimeCreated  = $log.TimeCreated
                TotalMinutes = $timespan.TotalMinutes
            }

            $colLogs += $oLogs
        }
    }
}

$count = $colLogs.Count

## Query the DC for the time
$nettime = net time
[datetime]$serverdatetime = $nettime[0].Replace('Current time at ', '').Replace(' is ', '|').Split('|')[1] ## Parse the returned string and cast it to a [datetime] object

## Query the system for what time it thinks is correct
$localdatetime = Get-Date

$currenttimespan = New-TimeSpan -Start $localdatetime -End $serverdatetime

## Here we're creating 3 different log files depending on how the time is behaving.
if (($currentimespan.TotalMinutes -gt 5) -or ($currenttimespan.TotalMinutes -lt -5)) {
    $logfile = New-Item -Path "$logPath" -Name "!!!$($env:COMPUTERNAME)_$count.txt" -ItemType File -Force
    Write-Output "Time is currently off by more than 5 minutes" | Out-File $logfile -Append

} elseif ($count -gt 0) {
    $logfile = New-Item -Path "$logPath" -Name "!$($env:COMPUTERNAME)_$count.txt" -ItemType File -Force
    Write-Output "Time is currently correct, but it has flipped out in the past" | Out-File $logfile -Append

} else {
    $logfile = New-Item -Path "$logPath" -Name "$($env:COMPUTERNAME)_$count.txt" -ItemType File -Force
    Write-Output "No errors with time" | Out-File $logfile -Append

}

$compInfo = Get-ComputerInfo

Write-Output $compInfo.WindowsProductName | Out-File $logfile -Append
Write-Output "OS Version: $($compInfo.OsVersion)" | Out-File $logfile -Append
Write-Output "LogonServer: $($compInfo.LogonServer)" | Out-File $logfile -Append
Write-Output "$($nettime[0])" | Out-File $logfile -Append
Write-Output "Local machine time is $($localdatetime.ToLongDateString()) $($localdatetime.ToLongTimeString())" | Out-File $logfile -Append
Write-Output 'Information about the difference between the current system time and the time on the DC:' | Out-File $logfile -Append
Write-Output $timespan | Out-File $logfile -Append
Write-Output "The time has flipped out $count times. Here are those events (if any):" | Out-File $logfile -Append
Write-Output $colLogs | Out-File $logfile -Append


if ($Resync) {
    Invoke-Command -ComputerName $ComputerName -ScriptBlock {
        Stop-Service -Name 'w32time' -Force
        & 'w32tm' '/unregister'
        & 'w32tm' '/register'
        Start-Service -Name 'w32time'
    }
}

