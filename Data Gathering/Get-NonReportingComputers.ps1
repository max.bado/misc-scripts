﻿
# Retrieve and email list of computers which have had no ConfigMgr Client activity since a certain time threshold
[CmdletBinding()]
param (
  [Parameter(Mandatory = $false)]
  [ValidateNotNullOrEmpty()]
  [String]$CollectionName = '[Dev] Labs and Classrooms | Standard | Desktops | LABS 19FA 1809 Image',
  [Parameter(Mandatory = $false)]
  [ValidateNotNullOrEmpty()]
  [datetime]$CutoffDateTime = ([datetime]::Today).AddHours(6),
  [Parameter(Mandatory = $false)]
  [String[]]$Emails,
  [Parameter(Mandatory = $false)]
  [String]$From,
  [Parameter(Mandatory = $false)]
  [String]$SmtpServer
)

. "$PSScriptRoot\Get-ComputerDetails.ps1"

$DetailsList = Get-ComputerDetails -CMCollection $CollectionName -NoPrimaryUser
$NonCompliantList = $DetailsList | Where-Object {
  ((Get-Date -Date ($_.LastPolicyRequest)) -lt $CutoffDateTime) -and ($_.Model -ne 'Virtual Machine')
} | Sort-Object -Property ComputerName
# $NonCompliantList | Out-GridView

# Email the report if specified
if ($Emails) {
  foreach ($Email in $Emails) {
    $Message = $NonCompliantList | Select-Object -Property ComputerName, Model, LastActivity, LastBootUpTime, DistinguishedName |
    ConvertTo-Html -Title "Non-Compliant Machines" -PreContent "The following computers have not checked in as of $($CurrentDateTime):" |
    Out-String
    
    Send-MailMessage -To $Email -From $From -SmtpServer $SmtpServer -Subject 'Non-Reporting Machines' -Body $Message -BodyAsHtml
  }
}

$NonCompliantList