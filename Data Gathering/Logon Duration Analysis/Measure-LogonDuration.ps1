#Requires -Version 4
<#
.SYNOPSIS
  This script measures the amount of time between when a user begins signing in to a Windows session and when they
  are able to interact with the Desktop.
.DESCRIPTION
  This script is designed to be executed as a logon-triggered scheduled task running as an administrative user account.
  The script will retrieve a list of current user sessions and select the currently active session. If there are multiple
  active sessions, such as in the case of a multi-session environment, the script will select the most recent one.
  The script will then parse the event logs to calculate a Seconds To Desktop value.

  The script imports custom types from QueryUser.cs in order to retrieve accurate details about user sessions on the machine.
  The script relies on LogonDurationAnaylsis.ps1 to retrieve granular time data about various phases of the logon process.

  If errors are encountered, the script will output a corresponding error file to the Network Share specified in the
  $NetworkShare parameter.

  If $DBServer, $DBName, and $DBTable parameters are provided, the script will attempt to write the resulting record to
  the database. The script expects the following table schema:

    ComputerName					nchar(16)
    Username						nchar(16)	
    SecondsToDesktop				float	
    LogonStartTime					datetime	
    Phase_UserProfile_Duration		float	
    Phase_UserProfile_StartTime		datetime	
    Phase_UserProfile_EndTime		datetime	
    Phase_UserProfile_TimeDelta		float	
    Phase_GPO_Duration				float	
    Phase_GPO_StartTime				datetime	
    Phase_GPO_EndTime				datetime	
    Phase_GPO_TimeDelta				float	
    Phase_GPScripts_Duration		float	
    Phase_GPScripts_StartTime		datetime	
    Phase_GPScripts_EndTime			datetime	
    Phase_GPScripts_TimeDelta		float	

.PARAMETER NetworkShare
  Network location to dump error files.
.PARAMETER DBServer
  Name of the database server.
.PARAMETER DBName
  Database name.
.PARAMETER DBTable
  Name of the database table.
.PARAMETER LogPath
  Path to the log file. This parameter is not mandatory. The default location for the log file is 
  "$env:TEMP\$($ScriptName).log"
.INPUTS
  None
.OUTPUTS
  None
.NOTES
  Version:        1.0
  Author:         Max Bado
  Creation Date:  5/11/2020
  Purpose/Change: Script cleanup for release
.EXAMPLE  
  1. Run with all defaults. The script doesn't output anything to a share and doesn't try to write anything to a database.
     The script outputs a log to $env:TEMP\$($ScriptName).log
     .\Measure-LogonDuration.ps1

  2. Direct the script to output errors to a specific share and to write the logon duration measurement to a database.
     .\Measure-LogonDuration -NetworkShare '\\server-01\LogonDuration$' -DBServer 'sql-01' -DBName 'PerfMonitor' -DBTable 'LogonDuration'

#>

[CmdletBinding()]
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $false)]
    [string]$NetworkShare,
    [Parameter(Mandatory = $false)]
    [string]$DBServer,
    [Parameter(Mandatory = $false)]
    [string]$DBName,
    [Parameter(Mandatory = $false)]
    [string]$DBTable,
    [Parameter(Mandatory = $false)]
    [string]$LogPath
)
#endregion

#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName
$ErrorActionPreference = 'Continue'

# Set LogPath if it wasn't specified.
if (!$LogPath) {
    $LogPath = "$env:TEMP\$($ScriptName).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogPath).Close()
    } Catch {
        $OldLogPath = $LogPath
        $LogPath = "$env:TEMP\$($ScriptName).log"
        Write-Warning -Path $LogPath -Level Info -Message "Unable to write to $OldLogPath. New Log File location: $LogPath."
    }    
}

# Import external dependencies
try {
    [string[]]$ReferencedAssemblies = 'System.Drawing', 'System.Windows.Forms', 'System.DirectoryServices'    
    Add-Type -Path "$PSScriptRoot\QueryUser.cs" -ReferencedAssemblies $ReferencedAssemblies -IgnoreWarnings -ErrorAction 'Stop'
    
    . "$PSScriptRoot\LogonDurationAnalysis.ps1"
} catch {
    $Err = $_
    Write-Error -Message "Unable to load external dependencies. Stopping." -Exception $_.Exception
    return
}


#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region -------------------------------------------------------[Write-Log Function]-------------------------------------------------------
function Write-Log {
    
    <#
    .Synopsis
       Write-Log writes a message to a specified log file with the current time stamp.
    .DESCRIPTION
       The Write-Log function is designed to add logging capability to other scripts.
       In addition to writing output and/or verbose you can write to a log file for
       later debugging.
    .NOTES
       Created by: Jason Wasser @wasserja
       Modified: 11/24/2015 09:30:19 AM  
    
       Changelog:
        * Code simplification and clarification - thanks to @juneb_get_help
        * Added documentation.
        * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
        * Revised the Force switch to work as it should - thanks to @JeffHicks
    
       To Do:
        * Add error handling if trying to create a log file in a inaccessible location.
        * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
          duplicates.
    .PARAMETER Message
       Message is the content that you wish to add to the log file. 
    .PARAMETER Path
       The path to the log file to which you would like to write. By default the function will 
       create the path and file if it does not exist. 
    .PARAMETER Level
       Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
    .PARAMETER NoClobber
       Use NoClobber if you do not wish to overwrite an existing file.
    .EXAMPLE
       Write-Log -Message 'Log message' 
       Writes the message to c:\Logs\PowerShellLog.log.
    .EXAMPLE
       Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
       Writes the content to the specified log file and creates the path and file specified. 
    .EXAMPLE
       Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
       Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
    .LINK
       https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
    #>
    
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,
    
        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path = 'C:\Logs\PowerShellLog.log',
            
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
            
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )
    
    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
            
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }
    
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }
    
        else {
            # Nothing to see here yet.
        }
    
        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
            
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion
#region -------------------------------------------------------[Get-LoggedOnUsers Function]-------------------------------------------------------
Function Get-LoggedOnUser {
    <#
.SYNOPSIS
    Get session details for all local and RDP logged on users.
.DESCRIPTION
    Get session details for all local and RDP logged on users using Win32 APIs. Get the following session details:
        NTAccount, SID, UserName, DomainName, SessionId, SessionName, ConnectState, IsCurrentSession, IsConsoleSession, IsUserSession, IsActiveUserSession
        IsRdpSession, IsLocalAdmin, LogonTime, IdleTime, DisconnectTime, ClientName, ClientProtocolType, ClientDirectory, ClientBuildNumber
.EXAMPLE
    Get-LoggedOnUser
.NOTES
    Description of ConnectState property:
    Value		 Description
    -----		 -----------
    Active		 A user is logged on to the session.
    ConnectQuery The session is in the process of connecting to a client.
    Connected	 A client is connected to the session.
    Disconnected The session is active, but the client has disconnected from it.
    Down		 The session is down due to an error.
    Idle		 The session is waiting for a client to connect.
    Initializing The session is initializing.
    Listening 	 The session is listening for connections.
    Reset		 The session is being reset.
    Shadowing	 This session is shadowing another session.
    
    Description of IsActiveUserSession property:
    If a console user exists, then that will be the active user session.
    If no console user exists but users are logged in, such as on terminal servers, then the first logged-in non-console user that is either 'Active' or 'Connected' is the active user.
    
    Description of IsRdpSession property:
    Gets a value indicating whether the user is associated with an RDP client session.
.LINK
    http://psappdeploytoolkit.com
#>
    [CmdletBinding()]
    Param (
    )
        
    Begin {
        ## Get the name of this function and write header
        [string]${CmdletName} = $PSCmdlet.MyInvocation.MyCommand.Name
        Write-Log -Level Info -Message "Starting ${CmdletName}" -Path $LogPath
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -CmdletBoundParameters $PSBoundParameters -Header
    }
    Process {
        Try {
            Write-Log -Level Info -Message 'Get session information for all logged on users.' -Path $LogPath
            Write-Output -InputObject ([Custom.QueryUser]::GetUserSessionInfo("$env:ComputerName"))
        } Catch {
            $Err = $_
            Write-Log -Level Erro -Message "Failed to get session information for all logged on users. `n$($Err)" -Path $LogPath
        }
    }
    End {
        Write-Log -Level Info -Message "End ${CmdletName}" -Path $LogPath
    }
}

#endregion

#region
function Write-ToShare {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [string]$Path,
        [Parameter(Mandatory = $true)]
        [string]$Name
    )

    Try {
        $FilePath = Join-Path -Path $Path -ChildPath $Name
        $FullPath = Resolve-Path -Path $FilePath

        [io.file]::OpenWrite($FullPath).Close()
        New-Item -Path $FullPath -ItemType File -Force
    } Catch {
        $Err = $_
        Write-Log -Level Error -Message "Unable to write to $($Path): $($Err.Exception)"
    }   
}
#endregion

#region -----------------------------------------------------------[Execution]------------------------------------------------------------

## VARIABLE DECLARATION

$CurrentTime = Get-Date
$SecondsToDesktop = $null
$AttemptCount = 0
$MaxAttempts = 20
$SleepSeconds = 5

Write-Log -Path $LogPath -Level Warn -Message "-----------------------------------------------------------START $ScriptName------------------------------------------------------------"

## MAIN LOGIC

Try {
    while (($SecondsToDesktop -lt 0) -and ($AttemptCount -le $MaxAttempts)) {
        ## Get currently logged on user using WMI. This should return a value if the session is a console session
        #$DomainAndUsernameFromWMI = (Get-CimInstance -ClassName Win32_ComputerSystem | Select-Object -Property *).UserName
        
        $ActiveUserSession = Get-LoggedOnUser | Where-Object { $_.ConnectState -eq 'Active' } | Sort-Object -Property LogonTime -Descending | Select-Object -First 1
        Write-Log -Path $LogPath -Level Info -Message "-----------------------------------------------------------Active User Sessions------------------------------------------------------------"
        foreach ($Session in $ActiveUserSession) {
            
            Write-Log -Path $LogPath -Level Info -Message @"

NTAccount: $($Session.NTAccount)
SID: $($Session.SID)
UserName: $($Session.UserName)
DomainName: $($Session.DomainName)
SessionId: $($Session.SessionId)
SessionName: $($Session.SessionName)
ConnectState: $($Session.ConnectState)
IsCurrentSession: $($Session.IsCurrentSession)
IsConsoleSession: $($Session.IsConsoleSession)
IsActiveUserSession: $($Session.IsActiveUserSession)
IsUserSession: $($Session.IsUserSession)
IsRdpSession: $($Session.IsRdpSession)
IsLocalAdmin: $($Session.IsLocalAdmin)
LogonTime: $($Session.LogonTime)
IdleTime: $($Session.IdleTime)
DisconnectTime: $($Session.DisconnectTime)
ClientName: $($Session.ClientName)
ClientProtocolType: $($Session.ClientProtocolType)
ClientDirectory: $($Session.ClientDirectory)
ClientBuildNumber: $($Session.ClientBuildNumber)
-----------------------------------------------------
"@
        }
        
        $Username = $ActiveUserSession | Select-Object -ExpandProperty 'UserName'
        $Domain = $ActiveUserSession | Select-Object -ExpandProperty 'DomainName'
        $SID = $ActiveUserSession.SID
        if (!$ActiveUserSession) {
            ## The session is not a console session and is outside the scope of this script. Stopping.
            Write-Log -Path $LogPath -Level Warn -Message "Script did not detect an active session. Stopping."
            Write-ToShare -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_NoActiveSessionError"
            #New-Item -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_NoActiveError" -ItemType File
    
            return
        }    
    
        ## Read in Winlogon events
        $WinlogonEvent = Get-WinEvent -MaxEvents 1 -ProviderName Microsoft-Windows-Winlogon -FilterXPath @"
*[System[(EventID='811') and Security[@UserID = "$($SID)"]]
and EventData[Data[@Name='SubscriberName']='SessionEnv']
and EventData[Data[@Name='Event']='2']]
"@
    
        if (!$WinlogonEvent) {
            ## Unable to find a userinit start event
            Write-Log -Path $LogPath -Level Info -Message "Unable to find userinit start event... Attempt $($AttemptCount) out of $($MaxAttempts)..."

            if ($AttemptCount -eq $MaxAttempts) {
                # Stopping after we reach our maximium attempts
                Write-Log -Path $LogPath -Level Warn -Message "Couldn't find userinit start event after $($MaxAttempts)... Stopping."
                Write-ToShare -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_$($UserName)_WinlogonEventError_$(Get-Date -Format yyyy-MM-ddTHH-mm-ss)"
                #New-Item -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_$($UserName)_WinlogonEventError_$(Get-Date -Format yyyy-MM-ddTHH-mm-ss)" -ItemType File
                return
            }
        }
    
        # Get winlogon start time
        $WinlogonEventStartTime = $WinlogonEvent.TimeCreated
        $WinlogonEventStartTimeFormatted = $WinlogonEvent.TimeCreated.ToUniversalTime().ToString('s')    
        Write-Log -Path $LogPath -Level Info -Message "Logon Start Time: $($WinlogonEventStartTime)"

    
        ## Get most recent LogonUI process termination
        $LogonUIStopEventList = Get-WinEvent -ProviderName Microsoft-Windows-Security-Auditing -FilterXPath @"
*[System[(EventID='4689') and TimeCreated[@SystemTime >= '$WinlogonEventStartTimeFormatted']] 
and EventData[Data[@Name='ProcessName']='C:\Windows\System32\LogonUI.exe']]
"@
    
        $LogonUIStopEvent = $LogonUIStopEventList | Sort-Object -Property TimeCreated | Select-Object -First 1
    
        if (!$LogonUIStopEvent) {
            # Unable to find a LogonUI stop event.
            Write-Log -Path $LogPath -Level Info -Message "Unable to find LogonUI stop event... Attempt $($AttemptCount) out of $($MaxAttempts)..."

            if ($AttemptCount -eq $MaxAttempts) {
                ## Stopping after we reach maximum attempts.
                Write-Log -Path $LogPath -Level Warn -Message "Couldn't find LogonUI stop event after $($MaxAttempts)... Stopping."
                Write-ToShare -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_$($UserName)_LogonUIError_$((Get-Date -Date $WinlogonEventStartTime -Format yyyy-MM-ddTHH-mm-ss))"
                #New-Item -Path $NetworkShare -Name "$($env:COMPUTERNAME)_$($UserName)_LogonUIError_$((Get-Date -Date $WinlogonEventStartTime -Format yyyy-MM-ddTHH-mm-ss))" -ItemType File
                return
            }
        }
    
        # Get LogonUI stop time
        $LogonUIStopTime = $LogonUIStopEvent.TimeCreated    
        Write-Log -Path $LogPath -Level Info -Message "LogonUI Stop Time: $($LogonUIStopTime)"

        # Calculate Time-to-Desktop time
        #$TimeToDesktop = $LogonUIStopTime - $UserInitStartTime
        $TimeToDesktop = $LogonUIStopTime - $WinlogonEventStartTime    
        $SecondsToDesktop = $TimeToDesktop.TotalSeconds
    
        if ($SecondsToDesktop -gt 0) {
            # Got a positive seconds to desktop value. Breaking loop
            Write-Log -Path $LogPath -Level Info -Message "Seconds To Desktop: $($SecondsToDesktop)"
            break
        } else {
            # Did not get a valid seconds to desktop value yet. Trying again after x seconds
            Write-Log -Path $LogPath -Level Warn -Message "Unable to get a valid Seconds To Desktop measurement... Attempt $($AttemptCount) out of $($MaxAttempts)..."            
            Start-Sleep -Seconds $SleepSeconds
        }

        $AttemptCount++
    }


    if ($SecondsToDesktop -lt 0) {
        # Couldn't get a valid seconds to desktop value after the maximum number of attempts. Stopping.
        Write-Log -Path $LogPath -Level Error -Message "Unable to get a valid Seconds to Desktop measurement after $($MaxAttempts) attempts. Stopping."
        Write-ToShare -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_$($UserName)_InvalidSecondsToDesktopError_$((Get-Date -Date $WinlogonEventStartTime -Format yyyy-MM-ddTHH-mm-ss))"

        #New-Item -Path $NetworkShare -Name "$($env:COMPUTERNAME)_$($UserName)_InvalidSecondsToDesktopError_$((Get-Date -Date $WinlogonEventStartTime -Format yyyy-MM-ddTHH-mm-ss))" -ItemType File
        return
    } 
    

    # Get details about logon phases
    Write-Log -Path $LogPath -Level Info -Message "-----------------------------------------------------------Logon Phase Details------------------------------------------------------------"

    $LogonPhaseDetails = Get-LogonDurationAnalysis -Username $Username -UserDomain $Domain
    
    $LogonStartTime = "$(Get-Date -Date $WinlogonEventStartTime)"
    $Phase_UserProfile = $LogonPhaseDetails | Where-Object { $_.PhaseName -eq 'User Profile' }
    $Phase_GPO = $LogonPhaseDetails | Where-Object { $_.PhaseName -eq 'Group Policy' }
    $Phase_GPScripts = $LogonPhaseDetails | Where-Object { $_.PhaseName -eq 'GP Scripts (async)' }
    
    Write-Log -Path $LogPath -Level Info -Message @"

Phase User Profile Duration: $($Phase_UserProfile.Duration)
Phase User Profile StartTime: $($Phase_UserProfile.StartTime)
Phase User Profile EndTime: $($Phase_UserProfile.EndTime)
Phase User Profile TimeDelta: $($Phase_UserProfile.TimeDelta.TotalSeconds)

Phase GPO Duration: $($Phase_GPO.Duration)
Phase GPO StartTime: $($Phase_GPO.StartTime)
Phase GPO EndTime: $($Phase_GPO.EndTime)
Phase GPO TimeDelta: $($Phase_GPO.TimeDelta.TotalSeconds)

Phase GPScripts Duration: $($Phase_GPScripts.Duration)
Phase GPScripts StartTime: $($Phase_GPScripts.StartTime)
Phase GPScripts EndTime: $($Phase_GPScripts.EndTime)
Phase GPScripts TimeDelta: $($Phase_GPScripts.TimeDelta.TotalSeconds)

Group Policy: $Phase_GPO
GP Scripts (async): $Phase_GPScripts
-----------------------------------------------------
"@

    if ($DBServer -and $DBName -and $DBTable) {
        # Write to Database
        $SQLQuery = @"
INSERT INTO [dbo].[$DBTable]
            ([ComputerName]
            ,[Username]
            ,[SecondsToDesktop]
            ,[LogonStartTime]
            ,[MemoryGB]
            ,[Phase_UserProfile_Duration]
            ,[Phase_UserProfile_StartTime]
            ,[Phase_UserProfile_EndTime]
            ,[Phase_UserProfile_TimeDelta]
            ,[Phase_GPO_Duration]
            ,[Phase_GPO_StartTime]
            ,[Phase_GPO_EndTime]
            ,[Phase_GPO_TimeDelta]
            ,[Phase_GPScripts_Duration]
            ,[Phase_GPScripts_StartTime]
            ,[Phase_GPScripts_EndTime]
            ,[Phase_GPScripts_TimeDelta])
        VALUES
            ('$env:COMPUTERNAME'
            ,'$UserName'
            ,'$SecondsToDesktop'
            ,'$LogonStartTime'
            ,'$($Phase_UserProfile.Duration)'
            ,'$($Phase_UserProfile.StartTime)'
            ,'$($Phase_UserProfile.EndTime)'
            ,'$($Phase_UserProfile.TimeDelta.TotalSeconds)'
            ,'$($Phase_GPO.Duration)'
            ,'$($Phase_GPO.StartTime)'
            ,'$($Phase_GPO.EndTime)'
            ,'$($Phase_GPO.TimeDelta.TotalSeconds)'
            ,'$($Phase_GPScripts.Duration)'
            ,'$($Phase_GPScripts.StartTime)'
            ,'$($Phase_GPScripts.EndTime)'
            ,'$($Phase_GPScripts.TimeDelta.TotalSeconds)');
"@
    
        try {
            Write-Log -Path $LogPath -Level Info -Message @"
Trying to write record to...
Database Server: $($DBServer)
Database Name: $($DBName)
Database Table: $($DBTable)
"@
            $SqlConnection = New-Object System.Data.SqlClient.SqlConnection
            $SqlConnection.ConnectionString = "Server = $DBServer; Database = $DBName; Integrated Security = True;"
            $SqlCmd = New-Object System.Data.SqlClient.SqlCommand
            $SqlCmd.CommandText = $SQLQuery
            $SqlCmd.Connection = $SqlConnection
            $SqlAdapter = New-Object System.Data.SqlClient.SqlDataAdapter
            $SqlAdapter.SelectCommand = $SqlCmd
            $DataSet = New-Object System.Data.DataSet
            $null = $SqlAdapter.Fill($DataSet)
        
            Write-Log -Path $LogPath -Level Info -Message "Record written successfully."
        } catch {
            $Err = $_
            Write-Log -Path $LogPath -Level Error -Message "Error writing to the database: $($Err.Exception)"
            Write-ToShare -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_$($UserName)_$('{0:N2}' -f $SecondsToDesktop)_$((Get-Date -Date $WinlogonEventStartTime -Format yyyy-MM-ddTHH-mm-ss))_SQLERROR"

            #New-Item -Path "$NetworkShare\Errors" -Name "$($env:COMPUTERNAME)_$($UserName)_$('{0:N2}' -f $SecondsToDesktop)_$((Get-Date -Date $WinlogonEventStartTime -Format yyyy-MM-ddTHH-mm-ss))_SQLERROR" -ItemType File -Value $Err
        }
    } else {
        Write-Log -Path $LogPath -Level Warn -Message "Won't be attempting to write record to DB since one or more DB variables are missing."
    }
} Catch {
    Write-Log -Path $LogPath -Level Error -Message "Error: $($_.Exception)"
    return
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogPath -Level Info -Message 'Completed Successfully.'
}

#endregion