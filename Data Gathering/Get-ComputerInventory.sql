-- Compile a list of computer details from the ConfigMgr SQL database
SELECT v_GS_COMPUTER_SYSTEM.ResourceID
	,v_GS_COMPUTER_SYSTEM.Manufacturer0
	,v_GS_COMPUTER_SYSTEM.Model0
	,v_GS_COMPUTER_SYSTEM.Name0
	,vSMS_R_System.BuildExt
	,vSMS_R_System.Distinguished_Name0
	,v_CH_ClientSummary.LastPolicyRequest
	-- get first 4 slots of memory
	,(SELECT v_GS_PHYSICAL_MEMORY.Capacity0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 0' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot1_Size
	,(SELECT v_GS_PHYSICAL_MEMORY.Speed0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 0' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot1_Speed

	,(SELECT v_GS_PHYSICAL_MEMORY.Capacity0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 1' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot2_Size
	,(SELECT v_GS_PHYSICAL_MEMORY.Speed0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 1' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot2_Speed

	,(SELECT v_GS_PHYSICAL_MEMORY.Capacity0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 2' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot3_Size
	,(SELECT v_GS_PHYSICAL_MEMORY.Speed0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 2' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot3_Speed

	,(SELECT v_GS_PHYSICAL_MEMORY.Capacity0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 3' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot4_Size
	,(SELECT v_GS_PHYSICAL_MEMORY.Speed0 FROM v_GS_PHYSICAL_MEMORY WHERE v_GS_PHYSICAL_MEMORY.ResourceID = v_GS_COMPUTER_SYSTEM.ResourceID AND v_GS_PHYSICAL_MEMORY.Tag0 = 'Physical Memory 3' AND v_GS_PHYSICAL_MEMORY.FormFactor0 != 0 AND v_GS_PHYSICAL_MEMORY.MemoryType0 != 11) AS MemorySlot4_Speed

	,MachinePrimaryUsers.PrimaryUsers -- list of primary users separated by ','
	,MachineProcessor.ProcessorName


FROM v_GS_COMPUTER_SYSTEM

LEFT JOIN (
	SELECT Main.MachineResourceID,
       LEFT(Main.v_UserMachineRelationship,Len(Main.v_UserMachineRelationship)-1) As "PrimaryUsers"
	FROM
		(
			SELECT DISTINCT ST2.MachineResourceID, 
				(
					SELECT ST1.UniqueUserName + ',' AS [text()]
					FROM dbo.v_UserMachineRelationship ST1
					WHERE ST1.MachineResourceID = ST2.MachineResourceID
					ORDER BY ST1.MachineResourceID
					FOR XML PATH ('')
				) v_UserMachineRelationship
			FROM dbo.v_UserMachineRelationship ST2
		) [Main]
	) AS MachinePrimaryUsers
	ON v_GS_COMPUTER_SYSTEM.ResourceID = MachinePrimaryUsers.MachineResourceID

LEFT JOIN (
	SELECT Main.ResourceID,
       LEFT(Main.v_GS_PROCESSOR,Len(Main.v_GS_PROCESSOR)-1) As "ProcessorName"
	FROM
		(
			SELECT DISTINCT ST2.ResourceID, 
				(
					SELECT ST1.Name0 + ',' AS [text()]
					FROM dbo.v_GS_PROCESSOR ST1
					WHERE ST1.ResourceID = ST2.ResourceID
					ORDER BY ST1.ResourceID
					FOR XML PATH ('')
				) v_GS_PROCESSOR
			FROM dbo.v_GS_PROCESSOR ST2
		) [Main]
	) AS MachineProcessor
	ON v_GS_COMPUTER_SYSTEM.ResourceID = MachineProcessor.ResourceID

JOIN vSMS_R_System
	ON v_GS_COMPUTER_SYSTEM.ResourceID = vSMS_R_System.ItemKey

JOIN v_CH_ClientSummary
	ON v_GS_COMPUTER_SYSTEM.ResourceID = v_CH_ClientSummary.ResourceID

WHERE v_GS_COMPUTER_SYSTEM.Model0 not like '%VMware%' 
	AND v_GS_COMPUTER_SYSTEM.Model0 not like '%virtual%'
ORDER BY v_GS_COMPUTER_SYSTEM.ResourceID

