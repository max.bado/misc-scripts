﻿# Retrieve Logon, Unlock, and Reconnect events on a computer
[CmdletBinding()]
param (
  [Parameter(Mandatory = $false)]
  [ValidateNotNullOrEmpty()]
  [String]$ComputerName = $env:COMPUTERNAME
)


###### RDP Logons ######
$ProviderName = 'Microsoft-Windows-Security-Auditing'
$EventId = '4624'
$LogonType = 10

[xml]$RDPLogonQuery = @"
<QueryList>
  <Query Id="0" Path="Security">
    <Select Path="Security">*[System[Provider[@Name='$ProviderName'] and (EventID=$EventId)] and EventData[Data[@Name='LogonType']=$LogonType]]</Select>
  </Query>
</QueryList>
"@

$RDPLogonEvents = Get-WinEvent -FilterXml $RDPLogonQuery -ComputerName $ComputerName


###### Unlocks ######
$ProviderName = 'Microsoft-Windows-Security-Auditing'
$EventId = '4624'
$LogonType = 7

[xml]$UnlockQuery = @"
<QueryList>
  <Query Id="0" Path="Security">
    <Select Path="Security">*[System[Provider[@Name='$ProviderName'] and (EventID=$EventId)] and EventData[Data[@Name='LogonType']=$LogonType]]</Select>
  </Query>
</QueryList>
"@


$UnlockLogonEvents = Get-WinEvent -FilterXml $UnlockQuery -ComputerName $ComputerName


###### RDP Session Reconnect ######
$ProviderName = 'Microsoft-Windows-Security-Auditing'
$EventId = '4778'

[xml]$ReconnectQuery = @"
<QueryList>
  <Query Id="0" Path="Security">
    <Select Path="Security">*[System[Provider[@Name='Microsoft-Windows-Security-Auditing'] and (EventID=$EventId)]]</Select>
  </Query>
</QueryList>
"@


$ReconnectEvents = Get-WinEvent -FilterXml $ReconnectQuery -ComputerName $ComputerName

$Return = [pscustomobject]@{
  RDPLogonEvents    = $RDPLogonEvents
  UnlockLogonEvents = $UnlockLogonEvents
  ReconnectEvents   = $ReconnectEvents
}

return $Return



