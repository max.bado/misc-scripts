﻿# Retrieve events for BSODs on local or remote computer. Output results to a log file
[CmdletBinding()]
param(
    [parameter(Mandatory = $false)]
    [string]$LogPath = '\\server01\BlueScreen_Logs$',

    [parameter(Mandatory = $false)]
    [string]$ComputerName = $env:COMPUTERNAME,

    [parameter(Mandatory = $false)]
    [int]$Days = 1
)

$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName
$ErrorActionPreference = 'Continue'

$HashProps = @{
    ProviderName = 'Windows Error Reporting'
    Id           = '1001'
    StartTime    = (Get-Date).AddDays(-$Days)
}

$Events = Get-WinEvent $HashProps -ComputerName $ComputerName -ErrorAction SilentlyContinue | 
Where-Object { ($_.Message -like '*BlueScreen*') } | Select-Object -Property *
if ($Events.count -gt 0) {
    Write-Host "Found $($Events.count) BlueScreen events in the past $Days days"
    $Events | Export-Csv -Path $LogPath -Force
} else {
    Write-Host "Found 0 BlueScreen events in the past $Days days"
    #$Events | Export-Csv -Path "$LogPath\!$env:COMPUTERNAME!.csv" -Force
}

$Events
#Get-WinEvent -FilterHashtable $HashProps