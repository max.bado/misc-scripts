﻿Function Get-RegKey {
    # Get the value of a registry key on one or more computers
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$False, ValueFromPipeline=$True, ValueFromPipelineByPropertyName=$True, HelpMessage='What computer would you like to target?')]
        [Alias('Computer')]
        [ValidateNotNullOrEmpty()]
        [string[]]$ComputerName = $env:COMPUTERNAME,
        [Parameter(Mandatory=$True, ValueFromPipeline=$True, ValueFromPipelineByPropertyName=$True, HelpMessage='Scan for what reg key?')]
        [Alias('Key')]
		[string]$RegKey,
        [Parameter(Mandatory=$False)]
        [string]$Property

    )

    [System.Collections.ArrayList]$Global:ComputerObjectList = @()

    if($computername -like "*.txt") {
        $path = $computername
        $ComputerName = Get-Content -Path $path
    }

    $RegKey = $RegKey.Replace("\","\\")


    #$computers = Get-Content -Path $dir\lbitl.txt

    foreach ($computer in $ComputerName) {
    Write-Output "Working on $computer"
        $rkey = $null
        $value = $null
        $registry = $null

        $ping = ping $computer -4 -n 1
        if($ping) {
            
            $registry = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey("LocalMachine", "$computer")

            if($registry.OpenSubKey("$RegKey") -ne $null) {
                Write-Output "Working on $RegKey"
                $rkey = $registry.OpenSubKey("$RegKey")
                $value = $Registry.OpenSubKey("$RegKey").GetValue("$Property")
            } else {
                $key = "Doesn't exist"
            } 

            $compObj = $null
            $compObj = New-Object -TypeName PSObject -Property @{
                Key = $RegKey
                Property = $Property
                Value = $value
                ComputerName = $computer
        
            } #| Export-Csv $dir\keylog.csv -Append

            $null = $ComputerObjectList.Add($compObj)
            #$ComputerObjectList | Format-Table -Property ComputerName,Key,Property,Value
        }
    }

    $ComputerObjectList

}

#$ComputerName = 'PZ205-01WC56317','BC118-14WL55355'
#$ComputerName = 'HV56273-1809EFI'
#Get-RegKey -ComputerName $ComputerName -RegKey 'SYSTEM\CurrentControlSet\Control\SecureBoot\State' -Property 'UEFISecureBootEnabled'
