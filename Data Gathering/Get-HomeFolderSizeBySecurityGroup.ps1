﻿# Yeah, I know this is a strangely very specific script. 
# It seemed like a good idea at the time!

param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$SecurityGroupName,
    [Parameter(Mandatory = $false)]
    [String]$FileServerName,
    [Parameter(Mandatory = $false)]
    [String]$HomeFolderRootPath,
    [Parameter(Mandatory = $false)]
    [pscredential]$Credential
)


$GroupMemberList = Get-ADGroupMember -Identity $SecurityGroupName
$ScriptBlock = {
    $GroupMemberList = $args[0]
    $HomeFolderRootPath = $args[1]
    
    $UserObjectList = foreach($GroupMember in ($GroupMemberList | Where-Object {$_.Name -match 's0|s1|s2'})) {
        Write-Host "Working on $($GroupMember.Name)"
        $UserObject = [pscustomobject]@{
            Username = $GroupMember.Name
            HomeFolderSizeGB = $null
        }

        $HomeFolderPath = (Join-Path -Path $HomeFolderRootPath -ChildPath $GroupMember.Name)
        $HomeFolderSize = (Get-ChildItem -Path $HomeFolderPath -Recurse | Measure-Object -Property Length -Sum).Sum / 1GB

        $UserObject.HomeFolderSize = $HomeFolderSize

        $UserObject
    }

    $UserObjectList
}

if ($Credential) {
    $List = Invoke-Command -ComputerName $FileServerName -ArgumentList $GroupMemberList, $HomeFolderRootPath -ScriptBlock $ScriptBlock -Credential $Credential
} else {
    $List = Invoke-Command -ComputerName $FileServerName -ArgumentList $GroupMemberList, $HomeFolderRootPath -ScriptBlock $ScriptBlock
}

$List
$List | Out-GridView