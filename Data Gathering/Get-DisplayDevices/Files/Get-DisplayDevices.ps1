﻿Add-Type -AssemblyName System.Windows.Forms
$AllScreens = $null
$LogFile = $null
$InvalidProcess = $null
$ValidProcess = $null
$DisplayDevices = $null
$Monitors = $null
$MonitorConnections = $null
$VideoOutputTechnology = $null
$objMonitorConnection = $null
$objMonitor = $null
$DeviceStartIndex = $null
$objDisplayDevice = $null
$OutputString = $null
$regexString = $null

$AllScreens = [System.Windows.Forms.Screen]::AllScreens

[System.Collections.ArrayList]$colObjDisplayDevice = @()
[System.Collections.ArrayList]$colObjMonitor = @()
[System.Collections.ArrayList]$colObjMonitorConnection = @()

$LogFile = "$env:SystemDrive\Temp\DisplayDevices.txt"

$InvalidProcess = (Get-CimInstance -Class Win32_Process | Where-Object {$_.SessionID -eq 0}).Name
$ValidProcess = (Get-CimInstance -Class Win32_Process | 
    Where-Object {($_.SessionId -gt 0) -and ($_.Name -notin $InvalidProcess)}).Name | Select-Object -First 1

& "$PSScriptRoot\ServiceUI.exe" "-process:$ValidProcess" "$env:windir\System32\cmd.exe" "/c $PSScriptRoot\DetectDisplays.exe > $env:SystemDrive\Temp\DisplayDevices.txt"
$DisplayDevices = Get-Content -Path "$env:SystemDrive\Temp\DisplayDevices.txt"
$Monitors = Get-CimInstance -Namespace root/WMI -ClassName WmiMonitorID 
$MonitorConnections = Get-CimInstance -Namespace root/WMI -ClassName WmiMonitorConnectionParams


ForEach ($MonitorConnection in $MonitorConnections)
{

    switch ($MonitorConnection.VideoOutputTechnology) {

        -2 {$VideoOutputTechnology = "-2 | UNINITIALIZED"}
        -1 {$VideoOutputTechnology = "-1 | OTHER"}
        0 {$VideoOutputTechnology = "0 | HD15 (VGA)"}
        1 {$VideoOutputTechnology = "1 | SVIDEO"}
        2 {$VideoOutputTechnology = "2 | COMPOSITE_VIDEO"}
        3 {$VideoOutputTechnology = "3 | COMPONENT_VIDEO"}
        4 {$VideoOutputTechnology = "4 | DVI"}
        5 {$VideoOutputTechnology = "5 | HDMI"}
        6 {$VideoOutputTechnology = "6 | LVDS"}
        8 {$VideoOutputTechnology = "8 | D_JPN"}
        9 {$VideoOutputTechnology = "9 | SDI"}
        10 {$VideoOutputTechnology = "10 | DISPLAYPORT_EXTERNAL"}
        11 {$VideoOutputTechnology = "11 | DISPLAYPORT_EMBEDDED"}
        12 {$VideoOutputTechnology = "12 | UDI_EXTERNAL"}
        13 {$VideoOutputTechnology = "13 | UDI_EMBEDDED"}
        14 {$VideoOutputTechnology = "14 | SDTVDONGLE"}
        15 {$VideoOutputTechnology = "15 | MIRACAST"}

    }
    
    $objMonitorConnection = [pscustomobject]@{
        InstanceName = $MonitorConnection.InstanceName.Trim()
        VideoOutputTechnology = $VideoOutputTechnology
    }

    $null = $colObjMonitorConnection.Add($objMonitorConnection)
}

ForEach ($Monitor in $Monitors) {

    $regexString = '[^\p{L}\p{Nd}/(/}/_//\\/&]'

    $objMonitor = [pscustomobject]@{
        InstanceName = [regex]::Replace(($Monitor.InstanceName.Trim()), $regexString, "")
        ManufacturerName = [regex]::Replace((($Monitor.ManufacturerName | ForEach{[char]$_}) -join "").Trim(), $regexString, "")
        ProductCodeID = [regex]::Replace((($Monitor.ProductCodeID | ForEach{[char]$_}) -join "").Trim(), $regexString, "")
        SerialNumberID = [regex]::Replace((($Monitor.SerialNumberID | ForEach{[char]$_}) -join "").Trim(), $regexString, "")
	    UserFriendlyName = [regex]::Replace((($Monitor.UserFriendlyName | ForEach{[char]$_}) -join "").Trim(), $regexString, "")
        YearOfManufacture = $Monitor.YearOfManufacture 
    }
    
    Add-Member -InputObject $objMonitor -MemberType NoteProperty -Name VideoOutputTechnology -Value $($colObjMonitorConnection | 
        Where-Object {$_.InstanceName -eq $objMonitor.InstanceName}).VideoOutputTechnology

    $null = $colObjMonitor.Add($objMonitor)
}

$i = 0
foreach($line in $DisplayDevices) {

    if($line -like "Device $i*") {

        $DeviceStartIndex = $DisplayDevices.IndexOf($line)
        #$DeviceStart = $line
        #$DeviceEnd = $strDisplayDevices[$DeviceStartIndex + 5]

        $objDisplayDevice = [pscustomobject]@{
            DeviceIndex = $line.Replace('Device','').Trim().Replace(':','')
            DeviceName = $DisplayDevices[$DeviceStartIndex + 1].Replace('DeviceName:','').Trim().Replace("'",'')
            DeviceString = $DisplayDevices[$DeviceStartIndex + 2].Replace('DeviceString:','').Trim().Replace("'",'')
            StateFlags = $DisplayDevices[$DeviceStartIndex + 3].Replace('StateFlags:','').Trim().Replace("'",'')
            DeviceID = $DisplayDevices[$DeviceStartIndex + 4].Replace('DeviceID:','').Trim().Replace("'",'')
            MonitorName = $DisplayDevices[$DeviceStartIndex + 5].Replace('Monitor Name:','').Trim().Replace("'",'')
            
        }

        Add-Member -InputObject $objDisplayDevice -MemberType NoteProperty -Name BitsPerPixel -Value $($AllScreens | Where-Object {$_.DeviceName -eq $objDisplayDevice.DeviceName}).BitsPerPixel
        Add-Member -InputObject $objDisplayDevice -MemberType NoteProperty -Name Bounds -Value $($AllScreens | Where-Object {$_.DeviceName -eq $objDisplayDevice.DeviceName}).Bounds
        Add-Member -InputObject $objDisplayDevice -MemberType NoteProperty -Name Primary -Value $($AllScreens | Where-Object {$_.DeviceName -eq $objDisplayDevice.DeviceName}).Primary
        Add-Member -InputObject $objDisplayDevice -MemberType NoteProperty -Name WorkingArea -Value $($AllScreens | Where-Object {$_.DeviceName -eq $objDisplayDevice.DeviceName}).WorkingArea
        Add-Member -InputObject $objDisplayDevice -MemberType NoteProperty -Name UserFriendlyName -Value $($colObjMonitor | 
            Where-Object {($_.InstanceName).Split('\')[1] -eq $objDisplayDevice.DeviceID.Split('\')[1]} | Select-Object -First 1).UserFriendlyName

        $null = $colObjDisplayDevice.Add($objDisplayDevice)
        $i++
    }   
}

## 

$OutputString = ""
$OutputString += "Monitors"

foreach($item in $colObjMonitor) {

    $OutputString += $item | Format-List | Out-String
}

$OutputString += "Display Devices"

foreach($item in $colObjDisplayDevice) {

    $OutputString += $item | Format-List | Out-String
}

$OutputString | Out-File -FilePath $LogFile


