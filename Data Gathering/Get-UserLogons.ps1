﻿function Get-UserLogons {
    # Retrieve a list of logons by a user based on the UserTrack SQL database
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [String]$UserName,
        [Parameter(Mandatory = $false)]
        [datetime]$StartDate = '1/1/2017',
        [Parameter(Mandatory = $false)]
        [String]$SQLServer,
        [Parameter(Mandatory = $false)]
        [String]$Database = 'usertrack',
        [Parameter(Mandatory = $false)]
        [pscredential]$Credential,
        [Parameter(Mandatory = $false)]
        [Switch]$Unique,
        [Parameter(Mandatory = $false)]
        [Switch]$Latest
    )

    if (!(Get-Module -Name SqlServer -ErrorAction SilentlyContinue)) {
        Import-Module -Name SqlServer
    }
    
    if($Latest) {
        $Query = "SELECT TOP(1) * FROM logtable WHERE user_name = '$UserName' ORDER BY date DESC"
    } else {
        $Query = "SELECT * FROM logtable WHERE user_name = '$UserName' and date >= '$StartDate' ORDER BY date DESC"
    }

    $Result = Invoke-Sqlcmd -ServerInstance $SQLServer -Database $Database -Query $Query #| Out-GridView

    if($Unique) {
        $Result | Sort-Object -Property computer_name -Unique
    } else {
        $Result
    }
}
