﻿# Fix performance counters
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerName = $env:COMPUTERNAME
)


$ResultList = Invoke-Command -ComputerName $ComputerName -ScriptBlock {
    $Result = $null
    $ResultObject = $null
    $Result = . "$env:windir\syswow64\lodctr" '/R'
    $ResultObject = [pscustomobject]@{
        ComputerName = $env:COMPUTERNAME
        Result = $Result
    }

    $ResultObject
}

$ResultList