﻿[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [String]$OUName = 'OU=Labs,DC=server01,DC=com'
)

$OU = Get-ADOrganizationalUnit -Identity $OUName

$ComputerList = Get-ADComputer -Filter * -SearchBase $OU -SearchScope Subtree

foreach($Computer in $ComputerList) {
    Reset-AdmPwdPassword -ComputerName $Computer.Name -Verbose
}