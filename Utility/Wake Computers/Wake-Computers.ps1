#Requires -Version 4
<#
.SYNOPSIS
  This script is designed to wake up sleeping computers via ConfigMgr.
.DESCRIPTION
  The script can attempt to wake up computers by their computer name or by collection. When using the
  -OutputOfflineComputers parameter, a CSV file containing the list of computers targeted by WoL is
  created in "$env:windir\Temp\$($ScriptName)_Offline_<current_date_time>.csv". This script has a 
  dependency on the Get-ComputerDetails.ps1 script.
.PARAMETER ComputerName
  Specifies a list of computers to target by WoL.
.PARAMETER CollectionName
  Specifies the collection to target by WoL. 
.PARAMETER CMServer
  Specifies the ConfigMgr server name. If left blank, script will attempt to discover the server
  name automatically.
.PARAMETER CMSite
  Specifies the ConfigMgr site name. If left blank, script will attempt to discover the site name
  automatically.
.PARAMETER OutputOfflineComputers
  If specified, a CSV file containing the list of computers targeted by WoL is created in
  "$env:windir\Temp\$($ScriptName)_Offline_<current_date_time>.csv". 
.PARAMETER LogFile
  Path to the log file including the file name. Default is "$env:windir\temp\$($ScriptName).log"
.NOTES
  Version:        1.1
  Author:         Max Bado
  Creation Date:  04/14/2020
  Purpose/Change: Added error checking and more verbose logging and messaging.

  Version:        1.0
  Author:         Max Bado
  Creation Date:  04/13/2020
  Purpose/Change: Initial script development
.EXAMPLE
  Attempt to wake up all computers in the "Desktops | All" collection and output a CSV file with the list of computers
  triggered by WoL.
  .\Wake-Computers.ps1 -CMServer 'cmserver01' -CMSite 'cmsite01' -CollectionName 'Desktops | All' -OutputOfflineComputers
  
  Attempt to wake up computers "MU56273" and "MU12345". The script will attempt to discover the ConfigMgr site server and
  site name automatically.
  .\Wake-Computers.ps1 -ComputerName 'MU56273','MU12345'
#>

[CmdletBinding()]
#[CmdletBinding(SupportsShouldProcess=$true)] # Use if you plan on accepting -whatif switch
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $false)][string[]]$ComputerName,
    [Parameter(Mandatory = $false)][string]$CollectionName,
    [Parameter(Mandatory = $false)][string]$CMServer,
    [Parameter(Mandatory = $false)][string]$CMSite,
    [Parameter(Mandatory = $false)][switch]$OutputOfflineComputers,
    [Parameter(Mandatory = $false)][Alias('LogPath')][string]$LogFile
)
#endregion
#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ErrorActionPreference = 'Continue'
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    $LogFile = "$env:windir\Temp\$($ScriptName).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        $LogFile = "$env:windir\Temp\$($ScriptName).log"
        Write-Error -Path $LogFile -Level Info -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}

# Define CMServer and CMSite if none was supplied
if (!$CMServer -or !$CMSite) {
    $SMSClientObject = New-Object -ComObject Microsoft.SMS.Client -Strict

    $CMServer = $SMSClientObject.GetCurrentManagementPoint()
    $CMSite = $SMSClientObject.GetAssignedSite()
}

# Load support script
. "$PSScriptRoot\Get-ComputerDetails.ps1"


#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region --------------------------------------------------[Event Log Write-Log Function]--------------------------------------------------

function Write-Log {

    <#
.Synopsis
   Write-Log writes a message to a specified log file with the current time stamp.
.DESCRIPTION
   The Write-Log function is designed to add logging capability to other scripts.
   In addition to writing output and/or verbose you can write to a log file for
   later debugging.
.NOTES
   Created by: Jason Wasser @wasserja
   Modified: 11/24/2015 09:30:19 AM  

   Changelog:
    * Code simplification and clarification - thanks to @juneb_get_help
    * Added documentation.
    * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
    * Revised the Force switch to work as it should - thanks to @JeffHicks

   To Do:
    * Add error handling if trying to create a log file in a inaccessible location.
    * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
      duplicates.
.PARAMETER Message
   Message is the content that you wish to add to the log file. 
.PARAMETER Path
   The path to the log file to which you would like to write. By default the function will 
   create the path and file if it does not exist. 
.PARAMETER Level
   Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
.PARAMETER NoClobber
   Use NoClobber if you do not wish to overwrite an existing file.
.EXAMPLE
   Write-Log -Message 'Log message' 
   Writes the message to c:\Logs\PowerShellLog.log.
.EXAMPLE
   Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
   Writes the content to the specified log file and creates the path and file specified. 
.EXAMPLE
   Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
   Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
.LINK
   https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
#>

    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path,
        
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
        
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )

    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
        
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }

        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }

        else {
            # Nothing to see here yet.
        }

        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
        
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion

#region -----------------------------------------------------------[Execution]------------------------------------------------------------


Write-Log -Path $LogFile -Level Warn -Message "START $ScriptName"
Write-Log -Path $LogFile -Level Info -Message "Input Parameters:"
Write-Log -Path $LogFile -Level Info -Message "CMServer: $CMServer; CMSite: $CMSite"
if ($CollectionName) {
    Write-Log -Path $LogFile -Level Info -Message "CollectionName: $CollectionName"
}
if ($ComputerName) {
    Write-Log -Path $LogFile -Level Info -Message "ComputerName: $ComputerName"
}
Write-Log -Path $LogFile -Level Info -Message "OutputOfflineComputers: $OutputOfflineComputers"

if (!$ComputerName -and !$CollectionName) {
    Write-Log -Path $LogFile -Level Warn -Message "No machines were specified to be woken up. Ending script."
    break
}

if ($ComputerName -and $CollectionName) {
    Write-Log -Path $LogFile -Level Warn -Message "Please specify either computer name(s) or collection name, not both. Ending script."
    break
}

if (!$CMServer -or !$CMSite) {
    Write-Log -Path $LogFile -Level Warn -Message "The ConfigMgr site server or site name was not specified and could not be discovered automatically. Ending script."
    break
}

Try {    
    if ($ComputerName) {
        $ResourceIDs = foreach ($Computer in $ComputerName) {
            Get-CimInstance  -ComputerName $CMServer -Namespace "root\SMS\Site_$CMSite" -Query "SELECT ResourceID FROM SMS_R_System WHERE NetBiosName = '$Computer'" | Select-Object -ExpandProperty ResourceID
        }
    }

    if ($CollectionName) {
        $CollectionID = Get-CimInstance -ComputerName $CMServer -Namespace "root\SMS\Site_$CMSite" -Query "SELECT CollectionID FROM SMS_Collection WHERE Name = '$CollectionName'" | Select-Object -ExpandProperty CollectionID
    }
    
    # Exit if we didn't find any ResourceIDs or CollectionID
    if (!$ResourceIDs -and !$CollectionID) {
        Write-Log -Path $LogFile -Level Warn -Message "The specified computer name(s) and/or collection name were not found. Ending script."
        break
    }

    # Wake up specified computers via the WMI method
    $WMIConnection = [WMICLASS]"\\$CMServer\root\SMS\Site_$($CMSite):SMS_SleepServer"
    $Params = $WMIConnection.psbase.GetMethodParameters("MachinesToWakeup")

    $Params.MachineIDs = $ResourceIDs
    $Params.CollectionID = $CollectionID
    $return = $WMIConnection.psbase.InvokeMethod("MachinesToWakeup", $Params, $Null) 
    
    if (!$return) {
        Write-Log -Path $LogFile -Level Warn -Message "No machines are online to wake up selected devices"
    }
    
    if ($return.numsleepers -ge 1) {
        Write-Log -Path $LogFile -Level Info -Message "$($return.numsleepers) machines have been scheduled to wake-up as soon as possible"

        # Create the output CSV file if specified
        if ($OutputOfflineComputers) {
            $OfflineComputers = $Null
            if ($CollectionName) {
                $OfflineComputers = Get-ComputerDetails -CMServer $CMServer -CMSite $CMSite -CMCollection $CollectionName -NoPrimaryUser |
                Where-Object { ($_.OnlineStatus -eq 0) -and ($_.Model -ne 'Virtual Machine') }
            } else {
                $OfflineComputers = Get-ComputerDetails -CMServer $CMServer -CMSite $CMSite -ComputerName $ComputerName -NoPrimaryUser |
                Where-Object { ($_.OnlineStatus -eq 0) -and ($_.Model -ne 'Virtual Machine') }
            }

            if ($OfflineComputers) {
                $FormattedDate = Get-Date -Format "yyyy-MM-dd HH-mm-ss"
                $OfflineComputerOutputFilePath = "$env:windir\Temp\$($ScriptName)_Offline_$($FormattedDate).csv"

                $OfflineComputers | Export-Csv -Path $OfflineComputerOutputFilePath -NoClobber -NoTypeInformation -Force
            } else {
                # All specified computers are already online or WoL can't be triggered on them because they are VMs
                Write-Log -Path $LogFile -Level Info -Message "All targeted computers are either already awake or Wake-On-Lan can't be triggered on them because they're VMs"
            }
        }
    } else {
        Write-Log -Path $LogFile -Level Info -Message "All targeted computers are already awake"
    }
} Catch {
    Write-Log -Path $LogFile -Level Error -Message "Error: $($_.Exception)"
    Break
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
} else {
    Write-Log -Path $LogFile -Level Warn -Message 'Completed with Errors.'
}