﻿#Requires -Version 4
<#
.SYNOPSIS
  This script modifies the Microsoft Office recent items reg keys so that the paths point to the user's
  HomeDrive (U:) instead of the HomeShare UNC path (\\server01\users\username).
.DESCRIPTION
  The script iterates through the currently logged on user's HKCU:\SOFTWARE\Microsoft\Office reg key. It
  iterates through several different Office versions, several Office applications, and the File MRU and
  Place MRU keys. If it finds items within those keys which contain $env:HOMESHARE, it replaces that string
  with $env:HOMEDRIVE.
.PARAMETER LogFile
  Path to the log file including the file name. e.g. "C:\Windows\Logs\Software\Modify-OfficeRecentItems.log"
  Default LogFile name is "$($ScriptName)_$($env:Username).log".
.NOTES
  Version:        1.0
  Author:         Max Bado
  Creation Date:  12/04/2018
  Purpose/Change: Initial script development
.EXAMPLE
  Modify-OfficeRecentItems.ps1 -LogFile "$env:TEMP\Modify-OfficeRecentItems_jdoe.log"
#>

[CmdletBinding()]
#[CmdletBinding(SupportsShouldProcess=$true)] # Use if you plan on accepting -whatif switch
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $false)]
    [Alias('LogPath')]
    [string]$LogFile
)
#endregion

#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ErrorActionPreference = 'Continue'
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    $LogFile = "$env:windir\Temp\$($ScriptName)_$($env:Username).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        $LogFile = "$env:windir\Temp\$($ScriptName)_$($env:USERNAME).log"
        Write-Warning -Path $LogFile -Level Info -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}

#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region
function Reset-Log { 
    # https://gallery.technet.microsoft.com/scriptcenter/PowerShell-Script-to-Roll-a96ec7d4
    #function checks to see if file in question is larger than the paramater specified if it is it will roll a log and delete the oldes log if there are more than x logs. 
    param(
        [string]$filename, 
        [int64]$FileSize = 1mb , 
        [int] $LogCount = 5
    ) 
     
    $logRollStatus = $true 
    if (test-path $filename) { 
        $file = Get-ChildItem $filename 
        if ((($file).length) -ige $filesize) { #this starts the log roll  
            $fileDir = $file.Directory 
            $fn = $file.name #this gets the name of the file we started with 
            $files = Get-ChildItem $filedir | ? {$_.name -like "$fn*"} | Sort-Object lastwritetime 
            $filefullname = $file.fullname #this gets the fullname of the file we started with 
            #$logcount +=1 #add one to the count as the base file is one more than the count 
            for ($i = ($files.count); $i -gt 0; $i--) {  
                #[int]$fileNumber = ($f).name.Trim($file.name) #gets the current number of the file we are on 
                $files = Get-ChildItem $filedir | ? {$_.name -like "$fn*"} | Sort-Object lastwritetime 
                $operatingFile = $files | ? {($_.name).trim($fn) -eq $i} 
                if ($operatingfile) 
                {$operatingFilenumber = ($files | ? {($_.name).trim($fn) -eq $i}).name.trim($fn)} 
                else 
                {$operatingFilenumber = $null} 
 
                if (($operatingFilenumber -eq $null) -and ($i -ne 1) -and ($i -lt $logcount)) { 
                    $operatingFilenumber = $i 
                    $newfilename = "$filefullname.$operatingFilenumber" 
                    $operatingFile = $files | ? {($_.name).trim($fn) -eq ($i - 1)} 
                    write-host "moving to $newfilename" 
                    move-item ($operatingFile.FullName) -Destination $newfilename -Force 
                } elseif ($i -ge $logcount) { 
                    if ($operatingFilenumber -eq $null) {  
                        $operatingFilenumber = $i - 1 
                        $operatingFile = $files | ? {($_.name).trim($fn) -eq $operatingFilenumber} 
                        
                    } 
                    write-host "deleting " ($operatingFile.FullName) 
                    remove-item ($operatingFile.FullName) -Force 
                } elseif ($i -eq 1) { 
                    $operatingFilenumber = 1 
                    $newfilename = "$filefullname.$operatingFilenumber" 
                    write-host "moving to $newfilename" 
                    move-item $filefullname -Destination $newfilename -Force 
                } else { 
                    $operatingFilenumber = $i + 1  
                    $newfilename = "$filefullname.$operatingFilenumber" 
                    $operatingFile = $files | ? {($_.name).trim($fn) -eq ($i - 1)} 
                    write-host "moving to $newfilename" 
                    move-item ($operatingFile.FullName) -Destination $newfilename -Force    
                } 
                     
            } 
 
                     
        } else 
        { $logRollStatus = $false} 
    } else { 
        $logrollStatus = $false 
    } 
    $LogRollStatus 
} 
#endregion
#region --------------------------------------------------[Event Log Write-Log Function]--------------------------------------------------

function Write-Log {

    <#
.Synopsis
   Write-Log writes a message to a specified log file with the current time stamp.
.DESCRIPTION
   The Write-Log function is designed to add logging capability to other scripts.
   In addition to writing output and/or verbose you can write to a log file for
   later debugging.
.NOTES
   Created by: Jason Wasser @wasserja
   Modified: 11/24/2015 09:30:19 AM  

   Changelog:
    * Code simplification and clarification - thanks to @juneb_get_help
    * Added documentation.
    * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
    * Revised the Force switch to work as it should - thanks to @JeffHicks

   To Do:
    * Add error handling if trying to create a log file in a inaccessible location.
    * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
      duplicates.
.PARAMETER Message
   Message is the content that you wish to add to the log file. 
.PARAMETER Path
   The path to the log file to which you would like to write. By default the function will 
   create the path and file if it does not exist. 
.PARAMETER Level
   Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
.PARAMETER NoClobber
   Use NoClobber if you do not wish to overwrite an existing file.
.EXAMPLE
   Write-Log -Message 'Log message' 
   Writes the message to c:\Logs\PowerShellLog.log.
.EXAMPLE
   Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
   Writes the content to the specified log file and creates the path and file specified. 
.EXAMPLE
   Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
   Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
.LINK
   https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
#>

    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path,
        
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
        
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )

    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
        
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }

        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }

        else {
            # Nothing to see here yet.
        }

        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
        
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion

#region -----------------------------------------------------------[Execution]------------------------------------------------------------

# Variable Declaration

# List of Office versions
$OfficeVersionList = '11.0', '12.0', '14.0', '15.0', '16.0'

# List of Office applications
$OfficeApplicationList = 'Access', 'Excel', 'PowerPoint', 'Publisher', 'Word'

# Office registry path
$OfficeRegPath = 'HKCU:\Software\Microsoft\Office'

# MRU keys
$MRUList = 'File MRU', 'Place MRU'

# Roll log if necessary
$null = Reset-Log -filename $LogFile -FileSize 1MB -LogCount 5

# Homeshare serve:
$HomeServer = $env:HOMESHARE.Split('\')[2]

Write-Log -Path $LogFile -Level Warn -Message "START $ScriptName"
Write-Log -Path $LogFile -Level Info -Message "User HomeShare: $env:HOMESHARE"
Write-Log -Path $LogFile -Level Info -Message "User HomeDrive: $env:HOMEDRIVE"
Write-Log -Path $LogFile -Level Info -Message "User HomeShare Server: $HomeServer"
Write-Log -Path $LogFile -Level Info -Message "Checking the following Office versions: $($OfficeVersionList -join ', ')"
Write-Log -Path $LogFile -Level Info -Message "Checking the following Office applications: $($OfficeApplicationList -join ', ')"
Write-Log -Path $LogFile -Level Info -Message "Checking the following Office MRU items: $($MRUList -join ', ')"

Try {        
    foreach ($OfficeVersion in $OfficeVersionList) {
        $OfficeVersionRegPath = $null
        $OfficeVersionRegPath = "$OfficeRegPath\$OfficeVersion"
        foreach ($OfficeApplication in $OfficeApplicationList) {
            $OfficeApplicationRegPath = $null
            $OfficeApplicationRegPath = "$OfficeVersionRegPath\$OfficeApplication"

            # Iterate through MRU List items if they exist and modify the entries
            $UserMRU = $null
            $UserMRU = Get-Item -Path "$OfficeApplicationRegPath\User MRU" -ErrorAction SilentlyContinue
            if ($UserMRU) {
                $IdentityList = $null
                $IdentityList = Get-ChildItem -Path $UserMRU.PSPath -ErrorAction SilentlyContinue
                if ($IdentityList) {
                    Write-Log -Path $LogFile -Level Info -Message "Found $($IdentityList.count) Identity key(s) in $($UserMRU.Name)."
                    foreach ($Identity in $IdentityList) {
                        # Find the items and modify them
                        foreach ($MRU in $MRUList) {
                            $MRURegPath = "$($Identity.PSPath)\$MRU"
                            $MRURegKey = $null
                            $MRURegKey = Get-Item -Path $MRURegPath
                            if ($MRURegKey) {
                                Write-Log -Path $LogFile -Level Info -Message "Checking if $($MRURegKey.Name) has any items which need to be changed."
                                foreach ($Item in $MRURegKey.Property) {
                                    $ItemValue = $null
                                    $ItemValue = $($MRURegKey.GetValue($Item))
                                
                                    # Check if we need to change the item
                                    if ($ItemValue -like "`*$env:HOMESHARE*") {
                                        # We need to account for the Homeshare Server being in upper or lower case in the path                                        
                                        $NewItemValue = $ItemValue.Replace($($env:HOMESHARE), $env:HOMEDRIVE).Replace($($env:HOMESHARE.Replace($HomeServer, $HomeServer.ToUpper())), $env:HOMEDRIVE).Replace($($env:HOMESHARE.Replace($HomeServer, $HomeServer.ToLower())), $env:HOMEDRIVE)
                                        Write-Host "Old Value: $ItemValue"
                                        Write-Host "New Value: $NewItemvalue"

                                        # Set the new value
                                        Set-ItemProperty -Path $MRURegKey.PSPath -Name $Item -Value $NewItemValue
                                        Write-Log -Path $LogFile -Level Warn -Message "Changing value of '$Item' in key '$($MRURegKey.Name)' from '$($ItemValue)' to '$($NewItemValue)'"
                                    }
                                }
                            } else {
                                # No items to work on
                            }
                        }
                    }
                } else {
                    # No identies to iterate through
                }
            } else {
                # User MRU key does not exist here
            }
        }
    }

    Write-Log -Path $LogFile -Level Warn -Message "END $ScriptName"
   
} Catch {
    Write-Log -Path $LogFile -Level Error -Message "Error: $($_.Exception)"
    Break
}
if ($pscmdlet.ShouldProcess("Target of action", "Action will happen")) {
    #do action
} else {
    #don't do action but describe what would have been done
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
} else {
    Write-Log -Path $LogFile -Level Warn -Message 'Completed with Errors.'
}