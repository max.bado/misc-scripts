﻿#Requires -Version 4
<#
.SYNOPSIS
  The purpose of this script is to trigger driver updates via Windows Update.
.DESCRIPTION
  This script uses the Windows Update COM object to search for, download, and install driver updates
  which are available from Windows Update. This script is intended to be used inside a ConfigMgr OSD
  Task Sequence (although it can be used outside of an OSD as well) and write its execution result to 
  an SMS Variable so that the Task Sequence can use the variable to trigger a reboot if needed.
.PARAMETER ShowUpdates
  Specifying the ShowUpdates switch will write the list of found driver updates to the console. This
  uses the Out-Host cmdlet, so the output is not written to STDOUT.
.PARAMETER DownloadOnly
  Specifying the DownloadOnly switch will cause the script to download but not install the updates.
.PARAMETER SMSVariable
  Specifying a value for SMSVariable will cause the script to attempt to set the value of the specified
  variable to the script's execution result. Possible result values are "RebootRequired" and "Success".
.PARAMETER LogFile
  Path to the log file. This parameter is not mandatory. The default location for the log file is 
  "$env:windir\Temp\$(ScriptName).log"
.INPUTS
  None
.OUTPUTS
  Script execution result: "RebootRequired" or "Success"
.NOTES
  Version:        1.0
  Author:         Max Bado
  Creation Date:  11/27/2018
  Purpose/Change: Initial script development
.EXAMPLE  
  1. Find, download, and install driver updates inside of an OSD Task Sequence and set the value of the installation result
     to the OSDDriverUpdateResult variable.
     .\Update-Drivers.ps1 -SMSVariable 'OSDDriverUpdateResult'

  2. Find and download driver updates, but don't perform the installation.
     .\Update-Drivers.ps1 -DownloadOnly

  3. Find, download, and install driver updates. Display the list of driver updates in the console and redirect the log to a
     custom location.
     .\Update-Drivers.ps1 -ShowUpdates -LogFile "$env:Temp\Update-Drivers.log"
#>

[CmdletBinding()]
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $false, HelpMessage = "Specifying this switch will write the list of updates to the console.")]
    [switch]$ShowUpdates,
    [Parameter(Mandatory = $false, HelpMessage = "Specifying this switch will cause the updates to download but not install.")]
    [switch]$DownloadOnly,
    [Parameter(Mandatory = $false, HelpMessage = "To be used inside an OSD TaskSequence. Sets the specified variable to the Script Result.")]
    [string]$SMSVariable,    
    [Parameter(Mandatory = $false)]
    [string]$LogFile
)
#endregion

#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ErrorActionPreference = 'Continue'
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    $LogFile = "$env:windir\Temp\$($ScriptName).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        $LogFile = "$env:windir\Temp\$($ScriptName).log"
        Write-Warning -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}

#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region -------------------------------------------------------[Write-Log Function]-------------------------------------------------------
function Write-Log {
    
    <#
    .Synopsis
       Write-Log writes a message to a specified log file with the current time stamp.
    .DESCRIPTION
       The Write-Log function is designed to add logging capability to other scripts.
       In addition to writing output and/or verbose you can write to a log file for
       later debugging.
    .NOTES
       Created by: Jason Wasser @wasserja
       Modified: 11/24/2015 09:30:19 AM  
    
       Changelog:
        * Code simplification and clarification - thanks to @juneb_get_help
        * Added documentation.
        * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
        * Revised the Force switch to work as it should - thanks to @JeffHicks
    
       To Do:
        * Add error handling if trying to create a log file in a inaccessible location.
        * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
          duplicates.
    .PARAMETER Message
       Message is the content that you wish to add to the log file. 
    .PARAMETER Path
       The path to the log file to which you would like to write. By default the function will 
       create the path and file if it does not exist. 
    .PARAMETER Level
       Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
    .PARAMETER NoClobber
       Use NoClobber if you do not wish to overwrite an existing file.
    .EXAMPLE
       Write-Log -Message 'Log message' 
       Writes the message to c:\Logs\PowerShellLog.log.
    .EXAMPLE
       Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
       Writes the content to the specified log file and creates the path and file specified. 
    .EXAMPLE
       Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
       Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
    .LINK
       https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
    #>
    
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,
    
        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path = 'C:\Logs\PowerShellLog.log',
            
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
            
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )
    
    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
            
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }
    
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }
    
        else {
            # Nothing to see here yet.
        }
    
        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
            
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion
#region -------------------------------------------------------[Get-DriverUpdate Function]-------------------------------------------------------
function Get-DriverUpdate {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]$UpdateSession
    )

    # Create Service Manager object
    $UpdateServiceManager = New-Object -ComObject Microsoft.Update.ServiceManager
    
    # Add the Microsoft Update Service ID to the list of Services
    $UpdateServiceList = $UpdateServiceManager.Services
    $MicrosoftUpdateServiceID = '7971f918-a847-4430-9279-4a52d1efe18d'
    #'9482F4B4-E343-43B6-B170-9A65BC822C77'
    # Check if we need to add the Microsoft Update service
    if(!($UpdateServiceList | Where-Object {$_.ServiceID -eq $MicrosoftUpdateServiceID})) {
        $null = $UpdateServiceManager.AddService2($MicrosoftUpdateServiceID,7,"")
    }       
    
    # Create the Update Session Searcher object
    $UpdateSearcher = $UpdateSession.CreateUpdateSearcher()

    # Set Searcher properties
    $UpdateSearcher.ServiceID = $MicrosoftUpdateServiceID
    $UpdateSearcher.SearchScope =  1 # MachineOnly
    $UpdateSearcher.ServerSelection = 3 # Third Party

    # Define search criteria
    $Criteria = "IsInstalled=0 and Type='Driver'"

    # Search for updates
    $SearchResult = $UpdateSearcher.Search($Criteria)          
    $UpdateList = $SearchResult.Updates

    # Return the update list
    $UpdateList
}
#endregion
#region -------------------------------------------------------[Download-Update Function]-------------------------------------------------------
function Download-Update {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]$UpdateList,
        [Parameter(Mandatory = $true)]$UpdateSession
    )

    # Create Update Downloader object
    $UpdateDownloader = $UpdateSession.CreateUpdateDownloader()    
    $UpdateDownloader.Updates = New-Object -ComObject Microsoft.Update.UpdateColl

    # Populate Update Downloader object
    foreach($Update in $UpdateList) {
        $null = $UpdateDownloader.Updates.Add($Update)
    }

    # Download Updates
    $null = $UpdateDownloader.Download()
}
#endregion
#region -------------------------------------------------------[Install-Update Function]-------------------------------------------------------
function Install-Update {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $true)]$UpdateList,
        [Parameter(Mandatory = $true)]$UpdateSession
    )

    # Create Update Installer object
    $UpdateInstaller = $UpdateSession.CreateUpdateInstaller()
    $UpdateInstaller.Updates = New-Object -ComObject Microsoft.Update.UpdateColl
    
    # Populate Update Installer object
    foreach($Update in $UpdateList) {
        $null = $UpdateInstaller.Updates.Add($Update)
    }

    # Install updates
    $InstallationResult = $UpdateInstaller.Install()

    # Return install result
    $InstallationResult
}
#endregion
#endregion
#region -----------------------------------------------------------[Execution]------------------------------------------------------------

## VARIABLE DECLARATION

Write-Log -Path $LogFile -Level Warn -Message "-----------------------------------------------------------START $ScriptName------------------------------------------------------------"

## MAIN LOGIC

Try {
    # Create Update Session
    $UpdateSession = New-Object -ComObject Microsoft.Update.Session    

    # Get list of driver updates
    $DriverUpdateList = Get-DriverUpdate -UpdateSession $UpdateSession

    # Check if any updates were found
    if($DriverUpdateList) {
        Write-Log -Path $LogFile -Message "Found the following the updates:
$(foreach($DriverUpdate in $DriverUpdateList){
    "`nTitle: $($DriverUpdate.Title)"
    "`nDescription: $($DriverUpdate.Description)"
    "`nLastDeploymentChangeTime: $($DriverUpdate.LastDeploymentChangeTime)"
    "`nDriverClass: $($DriverUpdate.DriverClass)"
    "`nDriverHardwareID: $($DriverUpdate.DriverHardwareID)"
    "`nDriverManufacturer: $($DriverUpdate.DriverManufacturer)"
    "`nDriverModel: $($DriverUpdate.DriverModel)"
    "`nDriverProvider: $($DriverUpdate.DriverProvider)"
    "`nDriverVerDate: $($DriverUpdate.DriverVerDate)`n
    
    "
})"

        # Write update list to the console if specified
        if($ShowUpdates) {
            $DriverUpdateList | Format-List -Property Title, Description, LastDeploymentChange, DriverClass, DriverHardwareID, DriverManufacturer, DriverModel, DriverProvider, DriverVerDate | Out-Host
        }

        # Download the updates
        Download-Update -UpdateList $DriverUpdateList -UpdateSession $UpdateSession
        Write-Log -Path $LogFile -Message "Finished downloading updates."

        if($DownloadOnly) {
            # DownloadOnly was specified, exiting script at this point.
            Write-Log -Path $LogFile -Message "DownloadOnly switch was specified. Ending script."
            break
        }

        # Install the updates
        $DriverUpdateInstallationResult = Install-Update -UpdateList $DriverUpdateList -UpdateSession $UpdateSession
        Write-Log -Path $LogFile -Message "Finished installing updates."

    } else {
        Write-Log -Path $LogFile -Message "No updates found. Ending script."
        break
    }

    # Set the output
    if($DriverUpdateInstallationResult.RebootRequired) {
        $ResultStatus = 'RebootRequired'
    } else {
        $ResultStatus = 'Success'
    }
    
    # Set an OSD Variable if specified
    if($SMSVariable) {
        try {
            $TSEnvironment = New-Object -ComObject Microsoft.SMS.TSEnvironment -ErrorAction Stop
            $TSEnvironment.Value($SMSVariable) = $ResultStatus
            Write-Log -Path $LogFile -Message "Setting SMS variable `'$SMSVariable`" to `"$ResultStatus`"."
        }
        catch [System.Exception] {
            Write-Warning -Message "Unable to construct Microsoft.SMS.TSEnvironment object"
        }
    }
       
    # Return the result
    Write-Log -Path $LogFile -Message "Result: $ResultStatus."
    $ResultStatus

    Write-Log -Path $LogFile -Level Warn -Message "-----------------------------------------------------------END $ScriptName------------------------------------------------------------"

    # Copy log to _SMSTSLogPath if running in a task sequence
    if($TSEnvironment) {
        Copy-Item -Path $LogFile -Destination $TSEnvironment.Value("_SMSTSLogPath")
    }
} Catch {
    Write-Log -Path $LogFile -Level Error -Message "Error: $($_.Exception)"    
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
}

#endregion