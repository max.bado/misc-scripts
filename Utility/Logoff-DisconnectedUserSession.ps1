﻿[CmdletBinding()]
#[CmdletBinding(SupportsShouldProcess=$true)] # Use if you plan on accepting -whatif switch
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $false)]
    [string]$ComputerName = $env:ComputerName,
    [Parameter(Mandatory = $false)]
    [string]$UserName,
    [Parameter(Mandatory = $false)]
    [string]$SessionID
)

function Get-LoggedOnUsers {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $false)]
        [string]$ComputerName = $env:ComputerName
    )
    
    #$users = @()
    # Query using quser, 2>$null to hide "No users exists...", then skip to the next server
    $quser = quser /server:$ComputerName 2>$null
    if(!($quser)){
        Continue
    }
    
    #Remove column headers
    $quser = $quser[1..$($quser.Count)]
    $UserList = foreach($user in $quser){
        $usersObj = [PSCustomObject]@{
            Server = $null
            Username = $null
            SessionName = $null
            SessionId = $Null
            SessionState = $null
            LogonTime = $null
            IdleTime = $null
        }
        $quserData = $user -split "\s+"
    
        #We have to splice the array if the session is disconnected (as the SESSIONNAME column quserData[2] is empty)
        if(($user | select-string "Disc") -ne $null){
            #User is disconnected
            $quserData = ($quserData[0..1],"null",$quserData[2..($quserData.Length -1)]) -split "\s+"
        }
    
        # Server
        $usersObj.Server = $ComputerName
        # Username
        $usersObj.Username = $quserData[1]
        # SessionName
        $usersObj.SessionName = $quserData[2]
        # SessionID
        $usersObj.SessionID = $quserData[3]
        # SessionState
        $usersObj.SessionState = $quserData[4]
        # IdleTime
        $quserData[5] = $quserData[5] -replace "\+",":" -replace "\.","0:0" -replace "Disc","0:0"
        if($quserData[5] -like "*:*"){
            $usersObj.IdleTime = [timespan]"$($quserData[5])"
        }elseif($quserData[5] -eq "." -or $quserData[5] -eq "none"){
            $usersObj.idleTime = [timespan]"0:0"
        }else{
            $usersObj.IdleTime = [timespan]"0:$($quserData[5])"
        }
        # LogonTime
        $usersObj.LogonTime = (Get-Date "$($quserData[6]) $($quserData[7]) $($quserData[8] )")
    
        $usersObj
        #$users += $usersObj    
    }
    
    return $UserList
 
}

function Logoff-DisconnectedSession {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $false)]
        [string]$ComputerName = $env:ComputerName,
        [Parameter(Mandatory = $false)]
        [string]$UserName,
        [Parameter(Mandatory = $false)]
        [string]$SessionID
    )
    
    $DisconnectedSession = $null
    $DisconnectedSessionList = $null
    $FilteredDisconnectedSessionList = $null
    $DisconnectedSessionList = Get-LoggedOnUsers -ComputerName $ComputerName | Where-Object {$_.SessionState -eq 'Disc'}
    if($DisconnectedSessionList) {
        # Found disconnected sessions.
        if(!$UserName -and !$SessionID) {
            # No UserName or SessionID was specified. Logging off all disconnected sessions.
            foreach($DisconnectedSession in $DisconnectedSessionList) {    
                Write-Host "Found disconnected session:
                Username: $($DisconnectedSession.Username)
                SessionID: $($DisconnectedSession.SessionId)
                SessionState: $($DisconnectedSession.SessionState)
                LogonTime: $($DisconnectedSession.LogonTime)" -ForegroundColor Yellow
        
                Write-Host "Running: logoff /SERVER:$ComputerName $($DisconnectedSession.SessionId)" -ForegroundColor Yellow
                & logoff /SERVER:$ComputerName $DisconnectedSession.SessionId
                if($?) {
                    Write-Host "$($DisconnectedSession.Username) has been logged off successfully." -ForegroundColor Green
                } else {
                    Write-Host "An error occurred trying to disconnect $($DisconnectedSession.Username)." -ForegroundColor Red
                }
            }
        } else {
            # Either UserName or SessionID was specified. Only disconnecting sessions that match
            # these parameters.
            $FilteredDisconnectedSessionList = $DisconnectedSessionList | Where-Object {($_.Username -eq $UserName) -or ($_.SessionID -eq $SessionID)}
            foreach($DisconnectedSession in $FilteredDisconnectedSessionList) {    
                Write-Host "Found disconnected session:
                Username: $($DisconnectedSession.Username)
                SessionID: $($DisconnectedSession.SessionId)
                SessionState: $($DisconnectedSession.SessionState)
                LogonTime: $($DisconnectedSession.LogonTime)" -ForegroundColor Yellow
        
                Write-Host "Running: logoff /SERVER:$ComputerName $($DisconnectedSession.SessionId)" -ForegroundColor Yellow
                & logoff /SERVER:$ComputerName $DisconnectedSession.SessionId
                if($?) {
                    Write-Host "$($DisconnectedSession.Username) has been logged off successfully." -ForegroundColor Green
                } else {
                    Write-Host "An error occurred trying to disconnect $($DisconnectedSession.Username)." -ForegroundColor Red
                }
            }
        }
    } else {
        Write-Host "No disconnected sessions were found on $ComputerName."
    }
}

Logoff-DisconnectedSession -ComputerName $ComputerName -UserName $Username -SessionID $SessionID

