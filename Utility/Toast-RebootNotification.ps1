﻿# I abandoned work on this ages ago. Likely doesn't work.

# import BurntToast module
if (Get-Module -ListAvailable -Name 'BurntToast') {
    Import-Module -Name 'BurntToast'
} else {
    return $false
}

# determine days since last reboot
$lastbootuptime = (Get-CimInstance -Namespace 'root\cimv2' -Class 'win32_OperatingSystem').LastBootUpTime
<#$x = [regex]::Match($lastbootuptime,'(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(.*)')

$lastboot = Get-Date `
  -Year $x.Groups[1].Value `
  -Month $x.Groups[2].Value `
  -Day $x.Groups[3].Value `
  -Hour $x.Groups[4].Value `
  -Minute $x.Groups[5].Value `
  -Second $x.Groups[6].Value
#>
$span = New-TimeSpan -Start $lastbootuptime -End (Get-Date)
$days = [int]$span.TotalDays

# display reminder if 3+ days since last reboot
if ($days -ge 3) {
    # create text objects for the message content
    $text1 = New-BTText -Content 'Restart Recommended'
    $text2 = New-BTText -Content "It has been $days days since your last restart. IM recommends restarting once a day for best PC performance. MU HelpDesk: x4357."

    # reference the company logo that I prestaged in the module folder
    $path = (Get-Module -Name BurntToast).ModuleBase
    $image1 = New-BTImage -Source "$path\Images\MULogo.png" -AppLogoOverride -Crop Circle

    # select the audio tone that should be played when the notification displays
    $audio1 = New-BTAudio -Path 'C:\Windows\media\Alarm03.wav'

    # assemble the notification object
    $binding1 = New-BTBinding -Children $text1, $text2 -AppLogoOverride $image1
    $visual1 = New-BTVisual -BindingGeneric $binding1
    $content1 = New-BTContent -Visual $visual1 -Audio $audio1 -Scenario IncomingCall

    # submit the notification object to be displayed
    Submit-BTNotification -Content $content1 -UniqueIdentifier "MURestart"
}

return $true