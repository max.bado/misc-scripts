# Remove files with broken permissions
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$Path,

    [Parameter(Mandatory = $false)]
    [Switch]$Recurse
)

Import-Module -Name NTFSSecurity

if ($Recurse) {
    $TargetItems = Get-ChildItem -Path $Path -Recurse -Force
} else {
    $TargetItems = Get-ChildItem -Path $Path
}

foreach ($TargetItem in $TargetItems) {
    $ACL = $null
    $ACL = Get-Acl -Path $TargetItem.FullName
    Set-Acl -Path $TargetItem.FullName -AclObject $ACL
 
    Set-NTFSOwner -Account 'Administrator' -Path $TargetItem.FullName
 
    Clear-NTFSAccess -Path $TargetItem.FullName
     
    Enable-NTFSAccessInheritance -Path $TargetItem.FullName
}

if ($Recurse) {
    Remove-Item -Path $TargetItems -Recurse -Force
} else {
    Remove-Item -Path $TargetItems -Force
}
