﻿# Remove specific file or folders from computers that a certain user has logged into
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$Username = 'johndoe',

    [Parameter(Mandatory = $false)]
    [String]$LogonCutoffDate = '9/1/2018',

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$TargetItems = @("$env:SystemDrive:\Users\$UserName\AppData\Roaming\ESRI", "$env:SystemDrive:\Users\$UserName\AppData\Roaming\Adobe")
)

. "$PSScriptRoot\Get-UserLogons.ps1"

if ($LogonCutoffDate) {
    $LoggedOnComputerList = Get-UserLogons -UserName $UserName | Where-Object {$_.date -gt $LogonCutoffDate} | Select-Object -Property computer_name -Unique
} else {
    $LoggedOnComputerList = Get-UserLogons -UserName $UserName | Select-Object -Property computer_name -Unique
}

$LoggedOnComputerList = $LoggedOnComputerList.Trim()

$ScriptBlock = {
    param($TargetItems)
    Remove-Item -Path $TargetItems
}

Invoke-Command -ComputerName $LoggedOnComputerList -ScriptBlock $ScriptBlock -ArgumentList $TargetItems
