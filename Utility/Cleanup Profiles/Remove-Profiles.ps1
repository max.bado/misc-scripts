﻿#Requires -Version 4
<#
.SYNOPSIS
  This script removes user profiles from a computer using DelProf2 according to a config file. DelProf2 is expected to live in
  the same directory as the script. The script must be run on the computer from which profiles will be deleted.
.DESCRIPTION
  The script reads in a JSON config file which declares a list of standard files and folders, a list of user profiles to exclude,
  and a list of profiles to include (the include list accepts wildcards). The script creates an exclude list of all user profiles 
  which contain files and folders which do NOT match the list of standard files and folders. All other user profiles which match
  the Profile Include criteria and DON'T MATCH the Profile Exclude list that was just created are removed using DelProf2.
  The -Days parameter can be used to further filter this list by days of inactivity.

  The list of profiles excluded due to containing non-standard files and folders is exported to a CSV and placed on the network
  share specified in the $LogSharePath parameter.

  The DelProf2 output is also placed on the $LogSharePath parameter.

.PARAMETER Days
  Specify days of inactivity before a profile can be considered for deletion.
.PARAMETER ConfigFilePath
  Path to the JSON config file. Default is $PSScriptRoot\Config.json.
.PARAMETER LogSharePath
  Path to network share where the script output is placed.
.PARAMETER LogFile
  Path to the local log file including the file name. e.g. "C:\Windows\Logs\Software\Remove-Profiles.log"
  Default LogFile path is "$env:windir\Temp\$($ScriptName).log".
.PARAMETER ListMode
  WhatIf mode. Use this switch to have the script do everything except actually delete the profiles.
.NOTES
  Version:        1.0
  Author:         Max Bado
  Creation Date:  07/30/2019
  Purpose/Change: Initial script development
.EXAMPLE
  Run script with all defaults
  Remove-Profiles.ps1

  Run script with custom config file path, custom network share path, and only remove profiles inactive for more than 30 days
  Remove-Profiles.ps1 -Days 30 -ConfigFilePath "\\server01\ConfigShare$\config.json" -LogSharePath "\\server01\LogShare$"
#>

[CmdletBinding()]
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
param (
    [Parameter(Mandatory = $false)]
    [int]$Days,
    [Parameter(Mandatory = $false)]
    [string]$ConfigFilePath = "$PSScriptRoot\Config.json",
    [Parameter(Mandatory = $false)]
    [string]$LogSharePath = "\\server01\Remove-ProfilesLogs$",
    [Parameter(Mandatory = $false)]
    [string]$LogFile,
    [Parameter(Mandatory = $false)]
    [switch]$ListMode
)
#endregion

#region ----------------------------------------------------------[Declarations]----------------------------------------------------------
$ErrorActionPreference = 'Continue'
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName
$Computer = $env:COMPUTERNAME

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    $LogFile = "$env:windir\Temp\$($ScriptName).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        $LogFile = "$env:windir\Temp\$($ScriptName).log"
        Write-Warning -Path $LogFile -Level Info -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}
#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------

#region --------------------------------------------------[Event Log Write-Log Function]--------------------------------------------------

function Write-Log {

    <#
.Synopsis
   Write-Log writes a message to a specified log file with the current time stamp.
.DESCRIPTION
   The Write-Log function is designed to add logging capability to other scripts.
   In addition to writing output and/or verbose you can write to a log file for
   later debugging.
.NOTES
   Created by: Jason Wasser @wasserja
   Modified: 11/24/2015 09:30:19 AM  

   Changelog:
    * Code simplification and clarification - thanks to @juneb_get_help
    * Added documentation.
    * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
    * Revised the Force switch to work as it should - thanks to @JeffHicks

   To Do:
    * Add error handling if trying to create a log file in a inaccessible location.
    * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
      duplicates.
.PARAMETER Message
   Message is the content that you wish to add to the log file. 
.PARAMETER Path
   The path to the log file to which you would like to write. By default the function will 
   create the path and file if it does not exist. 
.PARAMETER Level
   Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
.PARAMETER NoClobber
   Use NoClobber if you do not wish to overwrite an existing file.
.EXAMPLE
   Write-Log -Message 'Log message' 
   Writes the message to c:\Logs\PowerShellLog.log.
.EXAMPLE
   Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
   Writes the content to the specified log file and creates the path and file specified. 
.EXAMPLE
   Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
   Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
.LINK
   https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
#>

    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path,
        
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
        
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )

    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
        
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }

        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }

        else {
            # Nothing to see here yet.
        }

        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
        
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion

#region --------------------------------------------------[Log Rollover Reset-Log Function]--------------------------------------------------
function Reset-Log { 
    # https://gallery.technet.microsoft.com/scriptcenter/PowerShell-Script-to-Roll-a96ec7d4
    #function checks to see if file in question is larger than the paramater specified if it is it will roll a log and delete the oldes log if there are more than x logs. 
    param(
        [string]$filename, 
        [int64]$FileSize = 1mb , 
        [int] $LogCount = 5
    ) 
     
    $logRollStatus = $true 
    if (test-path $filename) { 
        $file = Get-ChildItem $filename 
        if ((($file).length) -ige $filesize) { #this starts the log roll  
            $fileDir = $file.Directory 
            $fn = $file.name #this gets the name of the file we started with 
            $files = Get-ChildItem $filedir | ? {$_.name -like "$fn*"} | Sort-Object lastwritetime 
            $filefullname = $file.fullname #this gets the fullname of the file we started with 
            #$logcount +=1 #add one to the count as the base file is one more than the count 
            for ($i = ($files.count); $i -gt 0; $i--) {  
                #[int]$fileNumber = ($f).name.Trim($file.name) #gets the current number of the file we are on 
                $files = Get-ChildItem $filedir | ? {$_.name -like "$fn*"} | Sort-Object lastwritetime 
                $operatingFile = $files | ? {($_.name).trim($fn) -eq $i} 
                if ($operatingfile) 
                {$operatingFilenumber = ($files | ? {($_.name).trim($fn) -eq $i}).name.trim($fn)} 
                else 
                {$operatingFilenumber = $null} 
 
                if (($operatingFilenumber -eq $null) -and ($i -ne 1) -and ($i -lt $logcount)) { 
                    $operatingFilenumber = $i 
                    $newfilename = "$filefullname.$operatingFilenumber" 
                    $operatingFile = $files | ? {($_.name).trim($fn) -eq ($i - 1)} 
                    write-host "moving to $newfilename" 
                    move-item ($operatingFile.FullName) -Destination $newfilename -Force 
                } elseif ($i -ge $logcount) { 
                    if ($operatingFilenumber -eq $null) {  
                        $operatingFilenumber = $i - 1 
                        $operatingFile = $files | ? {($_.name).trim($fn) -eq $operatingFilenumber} 
                        
                    } 
                    write-host "deleting " ($operatingFile.FullName) 
                    remove-item ($operatingFile.FullName) -Force 
                } elseif ($i -eq 1) { 
                    $operatingFilenumber = 1 
                    $newfilename = "$filefullname.$operatingFilenumber" 
                    write-host "moving to $newfilename" 
                    move-item $filefullname -Destination $newfilename -Force 
                } else { 
                    $operatingFilenumber = $i + 1  
                    $newfilename = "$filefullname.$operatingFilenumber" 
                    $operatingFile = $files | ? {($_.name).trim($fn) -eq ($i - 1)} 
                    write-host "moving to $newfilename" 
                    move-item ($operatingFile.FullName) -Destination $newfilename -Force    
                } 
                     
            } 
 
                     
        } else 
        { $logRollStatus = $false} 
    } else { 
        $logrollStatus = $false 
    } 
    $LogRollStatus 
} 
#endregion

#endregion

#region -----------------------------------------------------------[Execution]------------------------------------------------------------

# Roll log if necessary
$null = Reset-Log -filename $LogFile -FileSize 1MB -LogCount 5

try {
    # Read in config file
    $ErrorActionPreference = 'Stop'
    Write-Log -Level Info -Path $LogFile -Message "Start $ScriptName"
    Write-Log -Level Info -Path $LogFile -Message "Config file path: $ConfigFilePath"

    $ConfigFile = Get-Item -Path $ConfigFilePath
    $ConfigFileContent = Get-Content -Path $ConfigFilePath -Raw
    $ConfigFileJson = ConvertFrom-Json -InputObject $ConfigFileContent
    $StandardFolderList = $ConfigFileJson.StandardFolders
    $StandardFileList = $ConfigFileJson.StandardFiles
    $ExcludedUserProfileList = $ConfigFileJson.ExcludedUserProfiles
    $IncludedUserProfileList = $ConfigFileJson.IncludedUserProfiles
    $IncludedUserProfileListString = ($IncludedUserProfileList | ForEach-Object {$_.Insert(0, '^').Replace('*','.*')}) -join '|'

    Write-Log -Level Info -Path $LogFile -Message "Standard folder list: $($StandardFolderList -join '; ')"
    Write-Log -Level Info -Path $LogFile -Message "Standard file list: $($StandardFileList -join '; ')"
    Write-Log -Level Info -Path $LogFile -Message "Excluded user profile list: $($ExcludedUserProfileList -join '; ')"
    Write-Log -Level Info -Path $LogFile -Message "Included user profile list string: $($IncludedUserProfileListString)"

    $UserProfileList = Get-ChildItem -Path "$($env:SystemDrive)\Users" -Directory -Exclude $ExcludedUserProfileList        
    
    $NonStandardUserProfileList = foreach($UserProfile in $UserProfileList) {
        # Check if user profile has any non-standard folders in the root
        $ProfileFolderList = $null
        $ProfileFolderList = Get-ChildItem -Path $UserProfile.FullName -Directory
        $ProfileStandardFolderList = $null
        $ProfileStandardFolderList = foreach($StandardFolder in $StandardFolderList) {
            Get-Item -Path "$(Join-Path -Path $UserProfile.FullName -ChildPath $StandardFolder)" -ErrorAction SilentlyContinue | Where-Object {$_.Attributes -like '*Directory*'}
        }

        $NonStandardFolderList = $null
        $NonStandardFolderList = foreach($ProfileFolder in $ProfileFolderList) {
            if($ProfileFolder.Name -notin $ProfileStandardFolderList.Name) {
                $ProfileFolder.FullName
                #Copy-Item -Path $ProfileFolder.FullName -Destination "D:\Profile Backup\$Computer\$($UserProfile.Name)" -Container -Force -Recurse
            }
        }
    
        # Check if user profile has any non-standard files in the root
        $ProfileFileList = $null
        $ProfileFileList = Get-ChildItem -Path $UserProfile.FullName -File
        $ProfileStandardFileList = $null
        $ProfileStandardFileList = foreach($StandardFile in $StandardFileList) {
            Get-Item -Path "$(Join-Path -Path $UserProfile.FullName -ChildPath $StandardFile)" -ErrorAction SilentlyContinue | Where-Object {$_.Attributes -notlike '*Directory*'}
        }

        $NonStandardFileList = $null
        $NonStandardFileList = foreach($ProfileFile in $ProfileFileList) {
            if($ProfileFile.Name -notin $ProfileStandardFileList.Name) {
                $ProfileFile.FullName
            }
        }
    
        # Create list of non-standard user profiles
        if($NonStandardFolderList -or $NonStandardFileList) {
            $NonStandardItemsObject = [pscustomobject]@{
                Profile = $UserProfile.Name               
                NonStandardFolderList = $NonStandardFolderList
                NonStandardFileList = $NonStandardFileList
            }
    
            $NonStandardItemsObject
        }
    
    }
    
    # Create DelProf arguments
    $DelProfArgs = @(
        "/c:$Computer",
        "/u",
        "/ntuserini"
    )

    if($ListMode) {
        $DelProfArgs += "/l"
    }
    
    if($Days) {
        $DelProfArgs += "/d:$Days"
    }
    
    $UserProfilesToDelete = foreach($UserProfile in ($UserProfileList | Where-Object {($_.Name -match "$($IncludedUserProfileListString)") -and ($_.Name -notin $NonStandardUserProfileList.Profile)})) {
        Write-Log -Level Info -Path $LogFile -Message "Including profile: $($UserProfile.Name)"
        $UserProfile.Name
    }

    foreach($UserProfileToDelete in $UserProfilesToDelete) {
        $DelProfArgs += "/id:$($UserProfileToDelete)"
    }

    Write-Log -Level Warn -Path $LogFile -Message "DelProf args: $($DelProfArgs -join ' ')"
    
    # Format excluded profile list
    $Output = foreach($Item in $NonStandardUserProfileList) {
        [pscustomobject]@{
            Profile = $Item.Profile
            FolderList = $Item.NonStandardFolderList -join '; '
            FileList = $Item.NonStandardFileList -join '; '
        }
    }
    
    $CurrentTimeString = $(Get-Date -Format yyyy-MM-ddTHH-mm-ss)
    
    # Export the excluded profile list
    if($Output) {
        $Output | Export-Csv -Path "$LogSharePath\$($Computer)_excluded_profiles_$CurrentTimeString.csv" -NoTypeInformation
    }
    
    # Remove matching profiles and output log
    if($UserProfilesToDelete) {
        $DelProfLog = & "$PSScriptRoot\delprof2.exe" $DelProfArgs

        if($DelProfLog) {
            $DelProfLog | Out-File -FilePath "$LogSharePath\$($Computer)_delprof_log_$CurrentTimeString.log"
        }
    } else {
        Write-Log -Level Warn -Path $LogFile -Message "No profiles to delete"
    }

} catch {
    Write-Log -Level Warn -Path $LogFile -Message $_.Exception
    Copy-Item -Path $LogFile -Destination "$LogSharePath\$($Computer)_ERROR.log" -Force
    break
}
#endregion

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
    Copy-Item -Path $LogFile -Destination "$LogSharePath\$($Computer).log" -Force
} else {
    Write-Log -Path $LogFile -Level Warn -Message 'Completed with Errors.'
    Copy-Item -Path $LogFile -Destination "$LogSharePath\$($Computer).log" -Force
}