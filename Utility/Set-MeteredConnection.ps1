﻿Param(           
    [parameter(Mandatory = $true, Position = 0)]
    [ValidateSet("DisplayNetworkCardOnly", "MeteredOn", "MeteredOff")]
    [string]$MeteredAction,

    [parameter(Mandatory = $false)]
    [string]$NetWorkCardLike = ""
)

If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "You do not have Administrator rights to run this script!`nPlease re-run this script as an Administrator!"
    Exit
}


if ($MeteredAction -eq "MeteredOn") { $MeteredChoice = 2 } else { $MeteredChoice = 0 }
if ($NetWorkCardLike -eq "") {
    $NetCards = Get-NetAdapter | select InterfaceDescription, InterfaceGuid
} else {
    $NetCards = Get-NetAdapter | Where-Object { $_.InterfaceDescription -Like "*$($NetWorkCardLike)*" } | select InterfaceDescription, InterfaceGuid
}
$RegistryName = "UserCost"
Foreach ($card in $NetCards) {
    if ($MeteredAction -eq "DisplayNetworkCardOnly") {
        Write-Host "Display Network Card: '$($card.InterfaceDescription)' ID:'$($card.InterfaceGuid)'"

    } else {
        Write-Host "Set $($MeteredAction) to connection $($card.InterfaceDescription)"
        $registryPath = "HKLM:\SOFTWARE\Microsoft\DusmSvc\Profiles\$($card.InterfaceGuid)\*"
        New-ItemProperty -Path $registryPath -Name $RegistryName -Value $MeteredChoice -PropertyType DWORD -Force | Out-Null
    }
}
# your settings take effect when you restart "date usage" DusmSvc  
Restart-Service -Name DusmSvc -Force