﻿# Parse the garbage that ConfigMgr outputs when it display the results of running a script
# against a number of machines. This script is not yet generalized. It is targeting a
# specific output from a specific script.
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$InputItemPath = "$env:USERPROFILE\Desktop\DisplayPowerRegResults.garbage",

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$OutputCsvPath = "$env:USERPROFILE\Desktop\DisplayPowerRegResults.csv"
)

if (Test-Path -Path $OutputCsvPath -ErrorAction SilentlyContinue) {
    Write-Host "'$OutputCsvPath' already exists. Stopping script."
    # Remove-Item -Path $CsvFilePath -Force -ErrorAction SilentlyContinue
    break
}

$Content = Get-Content -Path $InputItemPath
#$Split = [regex]::Split($Content, "(?=\[)") # Split on } while including it in the split
$List = for($i = 0; $i -lt $Content.Count; $i++) {
    #Write-Host $i
    if($Content[$i] -like '*[[]*') {
        # start of item
        $Start = $i
        #Write-Host "Start $Start"
        $End = $null
    }

    if($Content[$i] -like '*[]]*') {
        # end of item
        $End = $i
        #Write-Host "Start $Start"
        #Write-Host "End $End"
    }

    #if(($null -eq $Start) -and ($Content[$i] -like '*{*')) {
        
    #}

    if(($null -ne $Start) -and ($null -ne $End)) {
        $Entry = $null
        $Entry = $Content[$Start..$End]
        #Write-Host "Start $Start End $End"
        #Write-Host $Entry
        
        $EntrySplit = $Entry[0].Split()
        $EntryName = $EntrySplit[0]
        $EntryStatus = $EntrySplit[1]
        $EntryExitCode = $EntrySplit[2]

        $EntryStart = "["
        $Entry[0] = $EntryStart
        #$Entry.Insert(0, "{")
        #$Entry.Insert($Entry.Count, "}")
        $EntryString = $null
        $EntryString = $Entry -join ' '
        $EntryString = @"
{
"Name": "$EntryName",
"Status": "$EntryStatus",
"ExitCode": "$EntryExitCode",
"Data": $EntryString
}

"@
        #Write-Host $EntryString

        $Object = $null
        $Object = ConvertFrom-Json -InputObject $EntryString
        $Object

        $Start = $null
        $End = $null
    }
    


    #$Object = $null
    #$Object = ConvertFrom-Json -InputObject $Item
    #Export-Csv -InputObject $Object -Path $CsvFilePath -NoTypeInformation -Force -Append
}

$Counts = $List | ForEach-Object {$_.Data.Count}
$MaxCount = ($Counts | Sort-Object -Descending)[0]
$CsvTemplate = [pscustomobject]@{
    Name = $null
    Status = $null
    ExitCode = $null
}


for ($i = 1; $i -le $MaxCount; $i++) {
    $FieldName1 = "PSPath$i"
    Add-Member -InputObject $CsvTemplate -MemberType NoteProperty -Name $FieldName1 -Value $null
    $FieldName2 = "PowerSchemeGUID$i"
    Add-Member -InputObject $CsvTemplate -MemberType NoteProperty -Name $FieldName2 -Value $null
    $FieldName3 = "PowerSchemeDescription$i"
    Add-Member -InputObject $CsvTemplate -MemberType NoteProperty -Name $FieldName3 -Value $null
    $FieldName4 = "PowerSchemeFriendlyName$i"
    Add-Member -InputObject $CsvTemplate -MemberType NoteProperty -Name $FieldName4 -Value $null
    $FieldName5 = "ACSettingIndex$i"
    Add-Member -InputObject $CsvTemplate -MemberType NoteProperty -Name $FieldName5 -Value $null
    $FieldName6 = "DCSettingIndex$i"
    Add-Member -InputObject $CsvTemplate -MemberType NoteProperty -Name $FieldName6 -Value $null
}

$JsonTemplate = ConvertTo-Json -InputObject $CsvTemplate

foreach ($Item in $List) {
    $RowTemplate = $null    
    $RowTemplate = ConvertFrom-Json -InputObject $JsonTemplate
    $RowTemplate.Name = $Item.Name
    $RowTemplate.Status = $Item.Status
    $RowTemplate.ExitCode = $Item.ExitCode

    $DataCount = $Item.Data.Count
    for ($i = 0; $i -lt $DataCount; $i++) {
        $FieldName1 = "PSPath$($i+1)"
        $RowTemplate.$FieldName1 = $Item.Data[$i].PSPath

        $FieldName2 = "PowerSchemeGUID$($i+1)"
        $RowTemplate.$FieldName2 = $Item.Data[$i].PowerSchemeGUID

        $FieldName3 = "PowerSchemeDescription$($i+1)"
        $RowTemplate.$FieldName3 = $Item.Data[$i].PowerSchemeDescription

        $FieldName4 = "PowerSchemeFriendlyName$($i+1)"
        $RowTemplate.$FieldName4 = $Item.Data[$i].PowerSchemeFriendlyName

        $FieldName5 = "ACSettingIndex$($i+1)"
        $RowTemplate.$FieldName5 = $Item.Data[$i].ACSettingIndex

        $FieldName6 = "DCSettingIndex$($i+1)"
        $RowTemplate.$FieldName6 = $Item.Data[$i].DCSettingIndex
    }

    Export-Csv -InputObject $RowTemplate -Path $CsvFilePath -NoTypeInformation -Force -Append
}

