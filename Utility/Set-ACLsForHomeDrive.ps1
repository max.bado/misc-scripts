﻿[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [String]$UserName = "johndoe",
    [Parameter(Mandatory = $false)]
    [String]$UserDomain = "domain01",
    [Parameter(Mandatory = $false)]
    [string]$TargetPath = "\\server01\users\$UserName"
)

$colRights = [System.Security.AccessControl.FileSystemRights]::FullControl
$InheritanceFlag = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit, ObjectInherit"
$PropagationFlag = [System.Security.AccessControl.PropagationFlags]::None 
$objType =[System.Security.AccessControl.AccessControlType]::Allow 

$User = "$UserDomain\$UserName"

$objUser = New-Object System.Security.Principal.NTAccount($User) 

$objACE = New-Object System.Security.AccessControl.FileSystemAccessRule `
    ($objUser, $colRights, $InheritanceFlag, $PropagationFlag, $objType) 

$objACL = Get-ACL -Path $TargetPath
$objACL.AddAccessRule($objACE) 

Set-ACL -Path $TargetPath -AclObject $objACL