﻿#region Write-Log Function
function Write-Log {
    
    <#
    .Synopsis
        Write-Log writes a message to a specified log file with the current time stamp.
    .DESCRIPTION
        The Write-Log function is designed to add logging capability to other scripts.
        In addition to writing output and/or verbose you can write to a log file for
        later debugging.
    .NOTES
        Created by: Jason Wasser @wasserja
        Modified: 11/24/2015 09:30:19 AM  
    
        Changelog:
        * Code simplification and clarification - thanks to @juneb_get_help
        * Added documentation.
        * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
        * Revised the Force switch to work as it should - thanks to @JeffHicks
    
        To Do:
        * Add error handling if trying to create a log file in a inaccessible location.
        * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
            duplicates.
    .PARAMETER Message
        Message is the content that you wish to add to the log file. 
    .PARAMETER Path
        The path to the log file to which you would like to write. By default the function will 
        create the path and file if it does not exist. 
    .PARAMETER Level
        Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
    .PARAMETER NoClobber
        Use NoClobber if you do not wish to overwrite an existing file.
    .EXAMPLE
        Write-Log -Message 'Log message' 
        Writes the message to c:\Logs\PowerShellLog.log.
    .EXAMPLE
        Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
        Writes the content to the specified log file and creates the path and file specified. 
    .EXAMPLE
        Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
        Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
    .LINK
        https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
    #>
    
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,
    
        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path = 'C:\Logs\PowerShellLog.log',
            
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
            
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )
    
    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
            
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }
    
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }
    
        else {
            # Nothing to see here yet.
        }
    
        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
            
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion

#region Get-RegistryKeyTimestamp function
Function Get-RegistryKeyTimestamp {
    <#
        .SYNOPSIS
            Retrieves the registry key timestamp from a local or remote system.
 
        .DESCRIPTION
            Retrieves the registry key timestamp from a local or remote system.
 
        .PARAMETER RegistryKey
            Registry key object that can be passed into function.
 
        .PARAMETER SubKey
            The subkey path to view timestamp.
 
        .PARAMETER RegistryHive
            The registry hive that you will connect to.
 
            Accepted Values:
            ClassesRoot
            CurrentUser
            LocalMachine
            Users
            PerformanceData
            CurrentConfig
            DynData
 
        .NOTES
            Name: Get-RegistryKeyTimestamp
            Author: Boe Prox
            Version History:
                1.0 -- Boe Prox 17 Dec 2014
                    -Initial Build
 
        .EXAMPLE
            $RegistryKey = Get-Item "HKLM:\System\CurrentControlSet\Control\Lsa"
            $RegistryKey | Get-RegistryKeyTimestamp | Format-List
 
            FullName      : HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Lsa
            Name          : Lsa
            LastWriteTime : 12/16/2014 10:16:35 PM
 
            Description
            -----------
            Displays the lastwritetime timestamp for the Lsa registry key.
 
        .EXAMPLE
            Get-RegistryKeyTimestamp -Computername Server1 -RegistryHive LocalMachine -SubKey 'System\CurrentControlSet\Control\Lsa' |
            Format-List
 
            FullName      : HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Lsa
            Name          : Lsa
            LastWriteTime : 12/17/2014 6:46:08 AM
 
            Description
            -----------
            Displays the lastwritetime timestamp for the Lsa registry key of the remote system.
 
        .INPUTS
            System.String
            Microsoft.Win32.RegistryKey
 
        .OUTPUTS
            Microsoft.Registry.Timestamp
    #>
    [OutputType('Microsoft.Registry.Timestamp')]
    [cmdletbinding(
        DefaultParameterSetName = 'ByValue'
    )]
    Param (
        [parameter(ValueFromPipeline=$True, ParameterSetName='ByValue')]
        [Microsoft.Win32.RegistryKey]$RegistryKey,
        [parameter(ParameterSetName='ByPath')]
        [string]$SubKey,
        [parameter(ParameterSetName='ByPath')]
        [Microsoft.Win32.RegistryHive]$RegistryHive,
        [parameter(ParameterSetName='ByPath')]
        [string]$Computername
    )
    Begin {
        #region Create Win32 API Object
        Try {
            [void][advapi32]
        } Catch {
            #region Module Builder
            $Domain = [AppDomain]::CurrentDomain
            $DynAssembly = New-Object System.Reflection.AssemblyName('RegAssembly')
            $AssemblyBuilder = $Domain.DefineDynamicAssembly($DynAssembly, [System.Reflection.Emit.AssemblyBuilderAccess]::Run) # Only run in memory
            $ModuleBuilder = $AssemblyBuilder.DefineDynamicModule('RegistryTimeStampModule', $False)
            #endregion Module Builder
 
            #region DllImport
            $TypeBuilder = $ModuleBuilder.DefineType('advapi32', 'Public, Class')
 
            #region RegQueryInfoKey Method
            $PInvokeMethod = $TypeBuilder.DefineMethod(
                'RegQueryInfoKey', #Method Name
                [Reflection.MethodAttributes] 'PrivateScope, Public, Static, HideBySig, PinvokeImpl', #Method Attributes
                [IntPtr], #Method Return Type
                [Type[]] @(
                    [Microsoft.Win32.SafeHandles.SafeRegistryHandle], #Registry Handle
                    [System.Text.StringBuilder], #Class Name
                    [UInt32 ].MakeByRefType(),  #Class Length
                    [UInt32], #Reserved
                    [UInt32 ].MakeByRefType(), #Subkey Count
                    [UInt32 ].MakeByRefType(), #Max Subkey Name Length
                    [UInt32 ].MakeByRefType(), #Max Class Length
                    [UInt32 ].MakeByRefType(), #Value Count
                    [UInt32 ].MakeByRefType(), #Max Value Name Length
                    [UInt32 ].MakeByRefType(), #Max Value Name Length
                    [UInt32 ].MakeByRefType(), #Security Descriptor Size           
                    [long].MakeByRefType() #LastWriteTime
                ) #Method Parameters
            )
 
            $DllImportConstructor = [Runtime.InteropServices.DllImportAttribute].GetConstructor(@([String]))
            $FieldArray = [Reflection.FieldInfo[]] @(       
                [Runtime.InteropServices.DllImportAttribute].GetField('EntryPoint'),
                [Runtime.InteropServices.DllImportAttribute].GetField('SetLastError')
            )
 
            $FieldValueArray = [Object[]] @(
                'RegQueryInfoKey', #CASE SENSITIVE!!
                $True
            )
 
            $SetLastErrorCustomAttribute = New-Object Reflection.Emit.CustomAttributeBuilder(
                $DllImportConstructor,
                @('advapi32.dll'),
                $FieldArray,
                $FieldValueArray
            )
 
            $PInvokeMethod.SetCustomAttribute($SetLastErrorCustomAttribute)
            #endregion RegQueryInfoKey Method
 
            [void]$TypeBuilder.CreateType()
            #endregion DllImport
        }
        #endregion Create Win32 API object
    }
    Process {
        #region Constant Variables
        $ClassLength = 255
        [long]$TimeStamp = $null
        #endregion Constant Variables
 
        #region Registry Key Data
        If ($PSCmdlet.ParameterSetName -eq 'ByPath') {
            #Get registry key data
            $RegistryKey = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey($RegistryHive, $Computername).OpenSubKey($SubKey)
            If ($RegistryKey -isnot [Microsoft.Win32.RegistryKey]) {
                Throw "Cannot open or locate $SubKey on $Computername"
            }
        }
 
        $ClassName = New-Object System.Text.StringBuilder $RegistryKey.Name
        $RegistryHandle = $RegistryKey.Handle
        #endregion Registry Key Data
 
        #region Retrieve timestamp
        $Return = [advapi32]::RegQueryInfoKey(
            $RegistryHandle,
            $ClassName,
            [ref]$ClassLength,
            $Null,
            [ref]$Null,
            [ref]$Null,
            [ref]$Null,
            [ref]$Null,
            [ref]$Null,
            [ref]$Null,
            [ref]$Null,
            [ref]$TimeStamp
        )
        Switch ($Return) {
            0 {
               #Convert High/Low date to DateTime Object
                $LastWriteTime = [datetime]::FromFileTime($TimeStamp)
 
                #Return object
                $Object = [pscustomobject]@{
                    FullName = $RegistryKey.Name
                    Name = $RegistryKey.Name -replace '.*\\(.*)','$1'
                    LastWriteTime = $LastWriteTime
                }
                $Object.pstypenames.insert(0,'Microsoft.Registry.Timestamp')
                $Object
            }
            122 {
                Throw "ERROR_INSUFFICIENT_BUFFER (0x7a)"
            }
            Default {
                Throw "Error ($return) occurred"
            }
        }
        #endregion Retrieve timestamp
    }
}
#endregion

#region Get-MRUIdentities function
function Get-MRUIdentities {
    [cmdletbinding()]
    Param (
        [parameter(Mandatory=$false)]
        [string]$ComputerName = $env:COMPUTERNAME,
        [parameter(Mandatory=$false)]
        [string]$UserName = $env:USERNAME,
        [parameter(Mandatory=$false)]
        [ValidateSet('Access','Excel','PowerPoint','Word')]
        [string[]]$Applications = @('Word', 'Excel'),
        [parameter(Mandatory=$false)]
        [string]$OldMRUIdentity,
        [parameter(Mandatory=$false)]
        [string]$NewMRUIdentity
    )

    # Convert UserName to SID
    $UserObject = [System.Security.Principal.NTAccount]::new($UserName)
    $UserSID = $UserObject.Translate([System.Security.Principal.SecurityIdentifier])

    # Mount HKU Registry as PSDrive
    $RegHKU = New-PSDrive -Name 'HKU' -PSProvider Registry -Root 'HKEY_USERS' -ErrorAction SilentlyContinue

    # Find User's Registry Hive
    $UserRegHKU = Get-Item -Path "HKU:\$($UserSID.Value)"
    $Collection = [System.Collections.Generic.List[pscustomobject]]::new()

    foreach($Application in $Applications) {

        $MRUTypes = 'File MRU','Place MRU'
    
        # Get MRUIdentity Keys for current application
        $RegMRUIdentities = Get-ChildItem -Path "$($UserRegHKU.PSPath)\Software\Microsoft\Office\16.0\$Application\User MRU"

        # Iterate through each MRU Identity we found
        foreach($RegMRUIdentity in $RegMRUIdentities) {
        
            # Find MRU Types
            $RegMRUTypes = Get-ChildItem -Path "$($RegMRUIdentity.PSPath)"

            # Iterate through each MRU Type
            foreach($RegMRUType in $RegMRUTypes) {

                # Get Last Write Timestamps for registry keys
                $TimeStamp = Get-RegistryKeyTimestamp -RegistryKey $RegMRUType

                # Create custom object
                $MRURegObject = $null
                $MRURegObject = [pscustomobject]@{
                    UserName = $UserName
                    UserSID = $UserSID.Value
                    Application = $Application
                    MRUIdentity = $RegMRUIdentity.PSChildName
                    MRUType = $RegMRUType.PSChildName
                    TimeStamp = $TimeStamp.LastWriteTime
                    RegKey = $RegMRUType
                }

                # Add the object to our collection
                $Collection.Add($MRURegObject)
            } # END foreach $RegMRUTypes
        } # END foreach $RegMRUIdentities
    } # END foreach Application

    $Collection
}
#endregion

#region Merge-MRUIdentities function
function Merge-MRUIdentities {

    [cmdletbinding()]
    Param (
        [parameter(Mandatory=$false)]
        [string]$ComputerName = $env:COMPUTERNAME,
        [parameter(Mandatory=$false)]
        $OldMRUIdentity = $FileMRUExcel[1].RegKey,
        [parameter(Mandatory=$false)]
        $NewMRUIdentity = $FileMRUExcel[0].RegKey
    )

    # Mount HKU Registry as PSDrive
    $RegHKU = New-PSDrive -Name 'HKU' -PSProvider Registry -Root 'HKEY_USERS' -ErrorAction SilentlyContinue

    # Parse values in the OldMRUIdentity regkey and add them to a list
    [System.Collections.ArrayList]$OldApplicationMRUTypeList = @()

    $OldMRUIdentity.GetValueNames() | ForEach-Object {
        $Name = $_
        $Value = $OldMRUIdentity.GetValue($_)
        $File = $OldMRUIdentity.GetValue($_).Split('*')[1]

        $MRUObj = [pscustomobject]@{
            Name = $Name
            Value = $Value
            File = $File
        }

        $null = $OldApplicationMRUTypeList.Add($MRUObj)
    }

    # Parse values in the NewMRUIdentity regkey and add them to a list
    [System.Collections.ArrayList]$NewApplicationMRUTypeList = @()

    $NewMRUIdentity.GetValueNames() | ForEach-Object {
        $Name = $_
        $Value = $NewMRUIdentity.GetValue($_)
        $File = $NewMRUIdentity.GetValue($_).Split('*')[1]

        $MRUObj = [pscustomobject]@{
            Name = $Name
            Value = $Value
            File = $File
        }

        $null = $NewApplicationMRUTypeList.Add($MRUObj)
    }

    # Count of items in the current (new) MRU list
    $NewApplicationMRUTypeCount = ($NewApplicationMRUTypeList | Measure-Object).Count

    # Create list of duplicates to remove from old list
    $DuplicatesToRemove = $OldApplicationMRUTypeList | ForEach-Object {
        if($_.File -in $NewApplicationMRUTypeList.File) {
            return $_
        }
    }

    # Remove duplicates and adjust item index in old list
    $DuplicatesToRemove | ForEach-Object {
        $indexOfRemove = $OldApplicationMRUTypeList.IndexOf($_)
    
        $OldApplicationMRUTypeList[$indexOfRemove..$($OldApplicationMRUTypeList.Count - 1)] | ForEach-Object {
            $index = [int]$_.Name.Split(' ')[1]
            $NewName = "Item $($index - 1)"
            $_.Name = $NewName
        }

        $OldApplicationMRUTypeList.Remove($_)
    }

    # Shift all items in old list by the number of items in the current (new) list
    $OldApplicationMRUTypeList | ForEach-Object {
        $index = [int]$_.Name.Split(' ')[1]
        $NewName = "Item $($index + $NewApplicationMRUTypeCount)"
        $_.Name = $NewName
    }

    # Create merged list
    [System.Collections.ArrayList]$MergedApplicationMRUTypeList = @()
    $NewApplicationMRUTypeList | ForEach-Object {$null = $MergedApplicationMRUTypeList.Add($_)}
    $OldApplicationMRUTypeList | ForEach-Object {$null = $MergedApplicationMRUTypeList.Add($_)}

    # Iterate through the merged list and set values in the $NewMRUIdentity key
    $MergedApplicationMRUTypeList | ForEach-Object {
        
        #Write-Host "$($NewMRUIdentity) | $($_.Name) : $($_.Value)"
        Set-ItemProperty "$($NewMRUIdentity.PSPath)" -Name $_.Name -Value $_.Value -Force
    }
}
#endregion

$ScriptBlock = {
    #region Main Logic
    $MRUCollection = Get-MRUIdentities
    $Applications = @('Access','Excel','PowerPoint','Word')
    $MRUTypes = @('File MRU','Place MRU')
    foreach($Application in $Applications) {
        foreach($MRUType in $MRUTypes) {
            $CurrentApplicationMRUType = $null
            $Top2MRURegKeys = $null

            # Filter list by Application and MRU Type
            $CurrentApplicationMRUType = $MRUCollection | Where-Object {($_.Application -eq $Application) -and ($_.MRUType -eq $MRUType)}

            # Select Top 2 from the filtered list
            $Top2MRURegKeys = $CurrentApplicationMRUType | Sort-Object -Property TimeStamp -Descending | Select-Object -First 2

            #Write-Host "Working on $Application, $MRUType"

            if($CurrentApplicationMRUType) {
                # New MRU
                #Write-Host "New MRU:"
                #$Top2MRURegKeys[0]

                # Old MRU
                #Write-Host "Old MRU:"
                #$Top2MRURegKeys[1]

                # Merge MRUs
                #Write-Host "Merging Old with the New."
                Merge-MRUIdentities -OldMRUIdentity $Top2MRURegKeys[1].RegKey -NewMRUIdentity $Top2MRURegKeys[0].RegKey

            } else {
                #Write-Host "IT'S EMPTY YO!" -ForegroundColor Red
            }
        }
    }
}
#endregion

$ScriptBlock.Invoke()


