﻿[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$OutputCsvPath = "$env:USERPROFILE\Desktop\testaccounts.csv",
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$NamePrefix = "test",
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$NumberOfAccounts = 100,
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$HomeDirectoryRootPath = "\\server02\testusers\",
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$Domain = "domain01",
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$HomeDriveLetter = "M:",
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$LogonScriptPath = "logon.bat",
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$OUPath = 'OU=TestAccounts,DC=domain01,DC=home',
    [Parameter(Mandatory = $false)]
    [String]$GroupName = 'testgroup'    
)


# Create username / password csv
$hashtable = @{}
1..$NumberOfAccounts | ForEach-Object {
    $Name = "$($NamePrefix)$($_)"
    $Password = [guid]::NewGuid().ToString().Substring(0,8)        
    $hashtable[$Name] = $Password
    [pscustomobject]@{
        Name = $Name
        Domain = $Domain
        Password = $Password
    } | Export-Csv -Path $OutputCsvPath -NoTypeInformation -Append
}


# Create the accounts
$list = Import-Csv -Path $OutputCsvPath

foreach($item in $list) {

    $Password = ConvertTo-SecureString -AsPlainText $item.Password -Force

    New-ADUser -Name $item.Name -Server $Domain -HomeDirectory $HomeDirectory -HomeDrive $HomeDriveLetter -AccountPassword $Password -ScriptPath $LogonScriptPath -Enabled $true -Path $OUPath -Verbose

    ## Create Home Directory and set permissions
    $HomeDirectory = "$HomeDirectoryRootPath\$($item.Name)"
    New-Item -Path $HomeDirectory -ItemType Directory

    $colRights = [System.Security.AccessControl.FileSystemRights]::FullControl

    $InheritanceFlag = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit, ObjectInherit"
    $PropagationFlag = [System.Security.AccessControl.PropagationFlags]::None 

    $objType =[System.Security.AccessControl.AccessControlType]::Allow 

    $objUser = New-Object System.Security.Principal.NTAccount("$Domain\$($item.Name)") 

    $objACE = New-Object System.Security.AccessControl.FileSystemAccessRule ($objUser, $colRights, $InheritanceFlag, $PropagationFlag, $objType) 

    #$objACL = Get-ACL "C:\Scripts\Test.ps1" 
    # $objACL = Get-ACL -Path  .\settingspackages\
    $objACL = Get-ACL -Path  "$HomeDirectoryRootPath\$($item.Name)"
    $objACL.AddAccessRule($objACE) 

    # Set-ACL -Path .\settingspackages $objACL
    Set-ACL -Path "$HomeDirectoryRootPath\$($item.Name)" $objACL
}


# Add accounts to group
if ($GroupName) {
    $TestGroup = Get-ADGroup -Identity $GroupName
    $TestAccounts = Get-ADUser -Filter "SamAccountName -like '$NamePrefix*'" -Server $Domain -Properties *
    Add-ADGroupMember -Identity $TestGroup -Members $TestAccounts
}