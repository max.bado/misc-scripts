﻿<#
Use the test accounts created with Create-BulkTestAccounts to start RDP sessions to multiple
computers at the same time. Script can be run in the Logon, Logoff, and Cleanup phases.
#>
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$InputUserCsvPath = "$env:USERPROFILE\Desktop\testaccounts.csv",

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$UserComputerCsvPath = "$env:USERPROFILE\Desktop\testaccountsremoteconnection.csv",

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerDNSearchStrings = @("*Labs*"),

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerNameSearchStringsInclude = @("HH2*","HH3*","E3*","PZ207*"),

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerNameSearchStringsExclude = @("*WL01*","*ML*"),

    [Parameter(Mandatory = $false)]
    [Switch]$Randomize,

    [Parameter(Mandatory = $false)]
    [ValidateSet("Logon", "Logoff", "Cleanup")]
    [ValidateNotNullOrEmpty()]
    [String]$Phase = 'Logon',

    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$DelProfPath = "$env:windir\System32\DelProf2.exe"
)

if (!(Get-Module -Name PoshRSJob -ErrorAction SilentlyContinue)) {
    Import-Module -Name PoshRSJob
}
. "$PSScriptRoot\Get-LoggedOnUsers.ps1"
. "$PSScriptRoot\Invoke-Ping.ps1"

$UserList = Import-Csv -Path $InputUserCsvPath

if ($Phase -eq 'Logon') {
    # Generate list of computers that are online and don't have anyone logged in
    Write-Host "Retrieving computer list that matches provided search strings"
    $ComputerList = Get-ADComputer -Filter *
    
    $ComputerListDNFiltered = foreach ($SearchString in $ComputerDNSearchStrings) {
        $ComputerList | Where-Object {$_.DistinguishedName -like $SearchString}
    }
    $ComputerListDNFiltered = $ComputerListDNFiltered | Select-Object -Unique

    $ComputerListCNIncludeFiltered = foreach ($SearchString in $ComputerNameSearchStringsInclude) {
        $ComputerListDNFiltered | Where-Object {$_.Name -like $SearchString}
    }
    $ComputerListCNIncludeFiltered = $ComputerListCNIncludeFiltered | Select-Object -Unique

    $ComputerListCNExcludeFiltered = foreach ($SearchString in $ComputerNameSearchStringsExclude) {
        $ComputerListCNIncludeFiltered | Where-Object {$_.Name -like $SearchString}
    }
    $ComputerListCNExcludeFiltered = $ComputerListCNExcludeFiltered | Select-Object -Unique

    $ComputerList = $ComputerListCNIncludeFiltered | Where-Object {$_ -notin $ComputerListCNExcludeFiltered}
    Write-Host "Found $($ComputerList.Count) matching computers"

    Write-Host "Checking computer online status"
    $PingResult = Invoke-Ping -ComputerName $ComputerList.Name -Timeout 5 -Detail WSMAN
    $Online = ($PingResult | Where-Object {$_.WSMAN -eq "True"})
    Write-Host "$($Online.Count) computers are online"
    
    Write-Host "Checking which machines has users currently logged in"
    $LoggedOnUserResult = Get-LoggedOnUsers -ComputerName $Online.Name
    Write-Host "$($LoggedOnUserResult.Count) machines have user sessions"

    $NoLoggedOnUser = $Online | Where-Object {$_.Name -notin $LoggedOnUserResult.ComputerName}
    Write-Host "$($LoggedOnUserResult.Count) machines have no user sessions"

    if ($Randomize) {
        Write-Host "Randomizing available computer list"
        $NoLoggedOnUser = $NoLoggedOnUser | Sort-Object {Get-Random}
    }


    # Open RDP sessions
    Write-Host "Creating computer / user associations and saving to '$($UserComputerCsvPath)' and opening RDP sessions"
    $i = 0
    $UserList = $UserList | Sort-Object -Property Name -Descending
    foreach($User in $UserList) {
        $Computer = $NoLoggedOnUser[$i]
        $User = $item.Name
        $Password = $item.Password
        $Domain = $item.Domain

        # Add entry into the CSV file
        [pscustomobject]@{
            Computer = $Computer.Name
            Domain = $Domain
            User = $User
        } | Export-Csv -Path $UserComputerCsvPath -NoTypeInformation -Append

        # Create user computer association within the credential manager so that RDP doesn't prompt us
        cmdkey /generic:TERMSRV/$($Computer.Name) /user:$($Domain)\$($User) /pass:$($Password)
        
        # Open RDP session
        mstsc /v:$($Computer.Name)

        $i++
    }
}

if ($Phase -eq 'Logoff') {    
    # Disconnect
    $remoteconnectioncsv = Import-Csv -Path $UserComputerCsvPath

    $LoggedOnUserSessions = Get-LoggedOnUsers -ComputerName $remoteconnectioncsv.Computer
    
    $ScriptBlock = {
        param($UserComputer)
        $User = $UserComputer.User
        $Computer = $UserComputer.Computer
        $Sessions = $Using:LoggedOnUserSessions
        $TargetSessions = $Sessions | Where-Object {($_.ComputerName -eq $ComputerName)}
        $TargetSession = $TargetSessions.Sessions | Where-Object {($_.Username -eq $User) }

        Write-Host "Logging off session $($TargetSession.SessionId) for $User on $Computer"
        Start-Process -FilePath 'logoff' -ArgumentList "/SERVER:$($Computer)","$($TargetSession.SessionId)"
    }

    $Jobs = $remoteconnectioncsv | Start-RSJob -ScriptBlock $ScriptBlock
    Write-Host "Started $($Jobs.Count) logoff jobs"
    while ('Running' -in $Jobs.State) {
        $CompletedJobs = $Jobs | Where-Object {$_.State -eq 'Completed'}
        $RunningJobs = $Jobs | Where-Object {$_.State -eq 'Running'}
        Write-Host "Logoff jobs running: $($RunningJobs.Count); Logoff jobs completed: $($CompletedJobs.Count)"
        Start-Sleep -Seconds 2
    }

    $Result = $Jobs | Receive-RSJob
    $Result
    # foreach($item in $remoteconnectioncsv) {
    #     #$item.Name
    #     #$item.Password
    #     $Computer = $item.Computer
    #     $User = $item.User
    #     $LoggedOnUserSessions = Get-LoggedOnUsers -server $item.Computer
    #     $TargetSession = $LoggedOnUserSessions | Where-Object {$_.Username -eq $User}
    #     Write-Host "Logging off session $($TargetSession.SessionId) for $User on $Computer"
    #     #& 'logoff' "/SERVER:$($Computer)" "$($TargetSession.SessionId)"
    #     Start-Process -FilePath 'logoff' -ArgumentList "/SERVER:$($Computer)","$($TargetSession.SessionId)"
    # }
}

if ($Phase -eq 'Cleanup') {
    $remoteconnectioncsv = Import-Csv -Path $UserComputerCsvPath

    # Delete Profiles
    $ScriptBlock = {
        param($UserComputer)
        $DelProfPath = $Using:DelProfPath
        $User = $UserComputer.User
        $Computer = $UserComputer.Computer

        Start-Process -FilePath $DelProfPath -ArgumentList "/c:$($Computer)", "/id:$($User)", "/u"
    }
    
    $Jobs = $remoteconnectioncsv | Start-RSJob -ScriptBlock $ScriptBlock
    Write-Host "Started $($Jobs.Count) cleanup jobs"
    while ('Running' -in $Jobs.State) {
        $CompletedJobs = $Jobs | Where-Object {$_.State -eq 'Completed'}
        $RunningJobs = $Jobs | Where-Object {$_.State -eq 'Running'}
        Write-Host "Cleanup jobs running: $($RunningJobs.Count); Cleanup jobs completed: $($CompletedJobs.Count)"
        Start-Sleep -Seconds 2
    }

    $Result = $Jobs | Receive-RSJob
    $Result
}

