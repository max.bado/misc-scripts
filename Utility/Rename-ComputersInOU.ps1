﻿[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [String]$OUName = 'OU=Labs,DC=server01,DC=com',
    [Parameter(Mandatory = $false)]
    [String]$TargetString = 'E120',
    [Parameter(Mandatory = $false)]
    [String]$ReplaceString = 'E240C',
    [Parameter(Mandatory = $false)]
    [pscredential]$Credential = 'server01\johndoe'
)

$Computers = Get-ADComputer -Filter * -SearchBase $OUName

foreach($Computer in $Computers) {
    if($Computer.Name -like $TargetString) {
        Rename-Computer -ComputerName $Computer.Name -NewName $($Computer.Name.Replace($TargetString, $ReplaceString)) -DomainCredential $Credential -Force -Restart
    }
}