﻿param(
    [Parameter(Mandatory = $true, Position = 1)]
    [string]$password,
    [Parameter(Mandatory = $false, Position = 2)]
    [string[]]$settings
)
#$settings = Get-Content $config
#$settings = @('DASH Support,Enabled')

foreach ($setting in $settings) {
    $run = "$setting,$password,ascii,us"
    $Response = (gwmi -class Lenovo_SetBiosSetting -namespace root\wmi).SetBiosSetting("$run").return
    write-output ("$setting`n$Response")
    $Response = (gwmi -class Lenovo_SaveBiosSettings -namespace root\wmi).SaveBiosSettings("$password,ascii,us").return
    write-output ("$setting`n$Response")
    if ($Response -like "*error*") {
        exit 1
    }
}