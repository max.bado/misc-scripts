﻿<#
Close open handles that match the SearchString. Assumes you have handle.exe available in your PATH.
Be careful when using this since you can do some real damage closing handles that shouldn't be
getting closed.
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$SearchString = 'PDQInv'
)


$Handles = (handle $SearchString)
if ($Handles -like "*No matching handles found.*") {
    Write-Host "No matching handles found."
    break
}

$Handles = $Handles[5..($Handles.Count - 1)] | Where-Object {$_ -ne [String]$null}

foreach($Handle in $Handles) {

    $HandleSplit = $Handle.Split() | Where-Object {$_ -ne [String]$null}
    $ProcName = $HandleSplit[0]
    $ProcID = $HandleSplit[2]
    $HandleType = $HandleSplit[4]
    $HandleID = $HandleSplit[5].Replace(':','')
    $HandlePath = $HandleSplit[6..$HandleSplit.Count] -join ''

    $objHandle = [pscustomobject]@{
        ProcName = $ProcName
        ProcID = $ProcID
        HandleType = $HandleType
        HandleID = $HandleID
        HandlePath = $HandlePath
    }

    & 'handle.exe' '-c' "$($objHandle.HandleID)" '-p' "$($objHandle.ProcID)" -y
}