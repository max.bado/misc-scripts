﻿[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    [string]$LibraryName,
    [Parameter(Mandatory = $false)]
    [string]$Path = '\\server01\scans'
)

[Reflection.Assembly]::LoadFrom("$PSScriptRoot\Microsoft.WindowsAPICodePack.Shell.dll")
[Reflection.Assembly]::LoadFrom("$PSScriptRoot\Microsoft.WindowsAPICodePack.dll")

$NetworkScansLibrary = New-Object Microsoft.WindowsAPICodePack.Shell.ShellLibrary –Argument $LibraryName, $true
$NetworkScansLibrary.Add($Path)