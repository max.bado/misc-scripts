﻿# Open Remote Control sessions to multiple computers at the same time
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerName = @($env:COMPUTERNAME),

    [Parameter(Mandatory = $false)]
    [String]$OU = 'OU=Test Room,OU=LABS,DC=domain01,DC=com',

    [Parameter(Mandatory = $false)]
    [String]$Throttle = 10
)

. "$PSScriptRoot\Invoke-Ping.ps1"

if ($OU) {
    $ComputerName = Get-ADComputer -Filter * -SearchBase $OU | Select-Object -ExpandProperty Name
}

Write-Host "Checking computer online status"
$PingResult = Invoke-Ping -ComputerName $ComputerList.Name -Timeout 5 -Detail WSMAN
$Online = ($PingResult | Where-Object {$_.WSMAN -eq "True"})
Write-Host "$($Online.Count) computers are online"


for($i = 0; $i -lt $ComputerName.Count; $i++) {

    $Computer = $ComputerName[$i]
    Start-Process -FilePath "${env:ProgramFiles(x86)}\Microsoft Configuration Manager\AdminConsole\bin\i386\CmRcViewer.exe" -ArgumentList $Computer

    if ($Throttle) {
        if($i % $Throttle -eq 0) {
            Read-Host "Connected to $i machines. Press enter to kill these Remote Control sessions and continue with next batch..."
            Stop-Process -Name CmRcViewer -Force
        }
    }
}

if (!$Throttle) {
    Read-Host "Press enter to kill all Remote Control sessions..."
    Stop-Process -Name CmRcViewer -Force
}