function Get-UserInfo {
    # Retrieve a user's AD account info
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [String]$UserName
    )

    $UserInfo = $null

    # Retrieve Domain list
    $DomainInfo = Get-ADDomain    
    $DomainList = @(
        [pscustomobject]@{
            NetBIOSName = $DomainInfo.NetBIOSName
            DNSName = $DomainInfo.DNSRoot
        }
    )
    
    $DomainList += foreach ($ChildDomain in $DomainInfo.ChildDomains) {
        $DomainInfo = Get-ADDomain -Identity $ChildDomain
        [pscustomobject]@{
            NetBIOSName = $DomainInfo.NetBIOSName
            DNSName = $DomainInfo.DNSRoot
        }
    }

    # Parse the Username
    if($UserName -match '\\') {
        $UserDomain = $UserName.Split('\')[0]
        $UserName = $UserName.Split('\')[1]
    } elseif ($UserName -match '@') {
        $UserDomain = $UserName.Split('@')[1]
        $UserName = $UserName.Split('@')[0]
    }

    # Query AD for the User Info
    foreach ($Domain in $DomainList) {
        try {
            $UserInfo = Get-ADUser -Identity $UserName -Server $Domain.DNSName -Properties HomeDirectory, DisplayName -ErrorAction Stop
            Add-Member -InputObject $UserInfo -MemberType NoteProperty -Name DomainName -Value $Domain.NetBIOSName -Force
            Add-Member -InputObject $UserInfo -MemberType NoteProperty -Name DomainDNSName -Value $Domain.DNSName -Force
            break
        } catch {

        }
    }

    if (!$UserInfo) {
        Write-Warning -Message "User does not exist."
    }

    return $UserInfo
}