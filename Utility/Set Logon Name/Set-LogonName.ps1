﻿# This script changes the displayed last logged on user on the login screen
[CmdletBinding()]
param
(
    [Parameter(Mandatory = $false)][string]$UserName = $null
)

. "$PSScriptRoot\Get-UserInfo.ps1"

if (($UserName -eq '') -or ($UserName -eq $null) -or ($UserName -eq [string]$null)) {
    
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnUser -Value ""
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnSAMUser -Value ""
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnUserSID -Value ""
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name SelectedUserSID -Value ""
    Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnDisplayName -Value ""
    break
} else {
    $User = Get-UserInfo -UserName $UserName
    $UserName = $User.SAMAccountName
    $UserDomain = $User.DomainName
    $SID = $User.SID
    $DisplayName = $User.DisplayName
}

Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnUser -Value "$UserDomain\$UserName"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnSAMUser -Value "$UserDomain\$UserName"
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnUserSID -Value $SID
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name SelectedUserSID -Value $SID
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI' -Name LastLoggedOnDisplayName -Value $DisplayName

if (!(Get-ItemProperty -Path  'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\UserTile' -Name $SID -ErrorAction SilentlyContinue)) {
    New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\UserTile' -Name $SID -Value '{60B78E88-EAD8-445C-9CFD-0B87F74EA6CD}'
}

Write-Host "LastLoggedOnUser: $UserDomain\$UserName"
Write-Host "LastLoggedOnSAMUser: $UserDomain\$UserName"
Write-Host "LastLoggedOnUserSID: $SID"
Write-Host "SelectedUserSID: $SID"
Write-Host "LastLoggedOnDisplayName: $DisplayName"
