﻿# Start RemoteRegistry service on remote computers
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerName = @($env:COMPUTERNAME)
)

Invoke-Command -ComputerName $ComputerName -ScriptBlock {
    Set-Service -Name RemoteRegistry -StartupType Automatic -Status Running
}
