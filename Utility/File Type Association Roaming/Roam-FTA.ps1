#Requires -Version 4
<#
.SYNOPSIS
  This script is designed to roam User File Associations (FTA) between computers using the
  command line utilities SetUserFTA.exe and GetUserFTA.exe designed by Christoph Kolbicz
  (http://kolbi.cz/blog/).
.DESCRIPTION
  This script should be run as a User Logon and Logoff script via a User targeted Group Policy.
  The script depends on SetUserFTA.exe and GetUserFTA.exe, so these files must exist in the
  script directory. 

  Upon Logon, the script checks to see if SetUserFTA.config exists inside of
  $env:HOMESHARE\FileTypeAssociations\. If the directory doesn't exist, the script will create it
  and export any registry entries in HKCU:\SOFTWARE\Classes\Applications. These registry entries
  need to exist on the system in order to use SetUserFTA.exe successfully.
  If SetUserFTA.config is not found, the script exports an initial config. If it's found,
  the script imports the config along with any .reg files in the directory.
  
  Upon Logoff, the script exports SetUserFTA.config and any registry entries in
  HKCU:\SOFTWARE\Classes\Applications into $env:HOMESHARE\FileTypeAssociations\.

  TODO: 
  - Parse and edit import config file so as to not import FTAs for applications that aren't installed.
  - Keep FTA config profiles for multiple computers if different FTAs are needed.
.PARAMETER Action
  The possible choices for this parameter are 'Logon' and 'Logoff'. Depending on which choice
  is set, the script will perform either Logon or Logoff operations.
.PARAMETER FTAConfigPath
  Path for storing the user's SetUserFTA.config file. Defaults to $env:HOMESHARE\FileTypeAssociations.
.PARAMETER FTARegPath
  Path for storing the .reg files associated with non-standard applications that need to exist in
  HKCU:\SOFTWARE\Classes\Applications in order for FTAs to be set. Defaults to 
  $env:HOMESHARE\FileTypeAssociations.
.PARAMETER DefaultFTAConfigPath
  The path to a default config file. The default config file is only imported if no config file
  currently exists or the current config file is older than the CutoffDate.
.PARAMETER CutoffDate
  SetUserFTA.config files with a modify date older than the CutoffDate are treated as out-of-date
  and ignored and a new config file is created.
.PARAMETER LogFile
  The default location for the Log is $env:windir\Temp\$ScriptName_$($env:USERNAME).log
.INPUTS
  None
.OUTPUTS
  $env:HOMESHARE\$FTAConfigPath\SetUserFTA.config
  $env:HOMESHARE\$FTARegPath\$Application.reg
  $env:windir\Temp\Roam-FTA_$($env:USERNAME).log
.NOTES
  Version:        1.1
  Author:         Max Bado
  Creation Date:  7/21/2018
  Purpose/Change: Added support for CutoffDate and Default Config

  Version:        1.0
  Author:         Max Bado
  Creation Date:  2/11/2018
  Purpose/Change: Initial script development
.EXAMPLE  
  .\Roam-FTA.ps1 -Action Logon
  .\Roam-FTA.ps1 -Action Logon -DefaultFTAConfigPath ".\DefaultFTA.config" -CutoffDate "08/29/2018"
  .\Roam-FTA.ps1 -Action Logoff
#>

[CmdletBinding()]
#[CmdletBinding(SupportsShouldProcess=$true)] # Use if you plan on accepting -whatif switch
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $true)][ValidateSet('Logon', 'Logoff')][string]$Action,
    [Parameter(Mandatory = $false)][string]$FTAConfigPath = "$env:HOMESHARE\FileTypeAssociations",
    [Parameter(Mandatory = $false)][string]$FTARegPath = "$env:HOMESHARE\FileTypeAssociations",
    [Parameter(Mandatory = $false)][string]$DefaultFTAConfigPath,
    [Parameter(Mandatory = $false)][datetime]$CutoffDate,
    [Parameter(Mandatory = $false)][Alias('LogPath')][string]$LogFile
)
#endregion

#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ErrorActionPreference = 'Continue'

$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    $LogFile = "$env:windir\Temp\$($ScriptName)_$($env:USERNAME).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        $LogFile = "$env:windir\Temp\$($ScriptName)_$($env:USERNAME).log"
        Write-Log -Path $LogFile -Level Info -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}

Write-Output "LogFile Path: $LogFile"
#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region ---------------------------------------------------[Get-RegFileContent Function]----------------------------------------------------
function Get-RegFileContent {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)][Alias('FilePath')][string]$Path   
    )

    if (!(Test-Path -Path $Path)) {
        Throw "The File Path '$Path' does not exist."
        break
    }

    $RegContent = Get-Content -Path $Path

    # Test to make sure this is a valid .reg file.
    if (($RegContent[0] -ne 'Windows Registry Editor Version 5.00') -and ($RegContent[0] -ne 'REGEDIT4')) {
        Throw "This is not a valid .reg file. Stopping."
        break
    }

    # Adding custom namespace to be able to decrypt the binary data from Binary registry properties.
    # Pulled from here:
    # https://www.sysadmins.lv/blog-en/convert-data-between-binary-hex-and-base64-in-powershell.aspx
    $signature = @"
[DllImport("Crypt32.dll", CharSet = CharSet.Auto, SetLastError = true)]
public static extern bool CryptStringToBinary(
    string pszString,
    int cchString,
    int dwFlags,
    byte[] pbBinary,
    ref int pcbBinary,
    int pdwSkip,
    ref int pdwFlags
);
[DllImport("Crypt32.dll", CharSet = CharSet.Auto, SetLastError = true)]
public static extern bool CryptBinaryToString(
    byte[] pbBinary,
    int cbBinary,
    int dwFlags,
    StringBuilder pszString,
    ref int pcchString
);
"@
    Add-Type -MemberDefinition $signature -Namespace PKI -Name Crypt32 -UsingNamespace "System.Text"
    $encoding = @{'Base64Header' = 0; 'Base64' = 1; 'HexRaw' = 12; 'Hex' = 4; 'HexAddr' = 10;
        'HexAscii' = 5; 'HexAddrAscii' = 11
    }

    $RegCol = for ($i = 0; $i -lt $RegContent.Length; $i++) {
        # For some reason the open square bracket needs to be escaped with a backtick
        # even in a literal, non-expanded string.
        if (($RegContent[$i].Trim() -like '`[HKEY*]') -or ($RegContent[$i].Trim() -like '`[-HKEY*]')) {
            
            $RegKey = $null
            $RegKey = $RegContent[$i].Trim()
        
            if ($RegKey -like '`[HKEY*]') {
                $KeyAction = 'Add'
            }
            if ($RegKey -like '`[-HKEY*]') {
                # The - in front of HKEY indicates key deletion.
                $KeyAction = 'Remove'
            }

            $RegEntry = [pscustomobject]@{
                Key    = $RegKey
                Action = $KeyAction            
            }

            # Check to see if the key has any properties
            $KeyProps = for ($j = $i + 1; $j -lt $RegContent.Length; $j++) {
                #Write-Host "j = $j"
                #Write-Host "Line: $($RegContent[$j])"
                if (($RegContent[$j].Trim() -like '`[HKEY*]') -or ($RegContent[$j].Trim() -like '`[-HKEY*]')) {
                    # This is the start of a new key.
                    break
                }
            
                if (($RegContent[$j].Trim() -notlike '"*') -and ($RegContent[$j].Trim() -notlike '@*')) {
                    # Unless this is a multiline property value (which we deal with later on in the script),
                    # anything that doesn't start with a ", @, or [ is ignored when importing a .reg file.
                    # So we're going to ignore it too... Continue to next line.
                    continue
                }

                # It's only a property if the line is not $null...
                if (($RegContent[$j] -ne $null) -and ($RegContent[$j] -ne [string]$null)) {
                    $KeyPropAction = 'Add'
                    $KeyPropType = $null
                    $KeyProp = $null
                    $KeyPropName = $null
                    $KeyPropValue = $null
                    $friendlyValueHex = $null

                    if ($RegContent[$j].Trim() -like '@*') {
                        $KeyPropName = '(Default)'
                        $split = $RegContent[$j].Trim().Split('=')
                        $KeyPropValue = ($split[1..$split.Count] -join '').Trim()
                    } else {
                        $KeyProp = $RegContent[$j].Trim()
                        #Write-Host "KeyProp: $KeyProp"

                        # Don't ask me to explain this pattern... I copied it from:
                        # https://stackoverflow.com/questions/171480/regex-grabbing-values-between-quotation-marks
                        # The pattern grabs any match between 2 quotation marks (including the quotes) while
                        # honoring the \ escape character.
                        $Pattern = [regex]::new('(["])(?:(?=(\\?))\2.)*?\1')
                        $Match = [regex]::Matches($KeyProp, $Pattern)
                        
                        if ($Match.count -gt 1) {
                            #Write-Host "Match -gt 1"
                            #Write-Host $Match
                            # This is a REG_SZ property
                            # Remove the enclosing quotes
                            $KeyPropName = $Match[0].Value.Remove(($Match[0].Length - 1), 1).Remove(0, 1)
                            
                            # Replace the escape characters
                            $KeyPropName = $KeyPropName.Replace('\"', '"').Replace('\\', '\')

                            $KeyPropValue = $Match[1].Value

                        } else {
                            #Write-Host "Match -lt 1"
                            # Not a REG_SZ property
                            # Remove the enclosing quotes
                            $KeyPropName = $Match[0].Value.Remove(($Match[0].Length - 1), 1).Remove(0, 1)

                            # Replace the escape characters
                            $KeyPropName = $KeyPropName.Replace('\"', '"').Replace('\\', '\')

                            $KeyPropValue = $KeyProp.Replace($($Match[0].Value), '').Split('=')[1].Trim()
                        }
                    }

                    #Write-Host "KeyPropValue = $KeyPropValue"
                    # Figure out what type of property this is
                    switch -Wildcard ($KeyPropValue) {
                        ( { $_ -eq '-' }) {
                            # This indicates property deletion
                            $KeyPropAction = 'Remove'                             
                            break
                        }
                        ( { $_ -like 'hex(0)*' }) {
                            # No Type
                            $KeyPropType = 'REG_NONE'
                        }

                        ( { $_ -like 'hex(1)*' }) {
                            # String (in hex)
                            $KeyPropType = 'REG_SZ'
                        }

                        ( { ($_[0] -eq '"') -and ($_[-1] -eq '"') }) {
                            # String
                            $KeyPropType = 'REG_SZ'
                            $KeyPropValue = $KeyPropValue.Remove(($KeyPropValue.Length - 1), 1).Remove(0, 1)
                            $KeyPropValue = $KeyPropValue.Replace('\"', '"').Replace('\\', '\')
                        }

                        ( { $_ -like 'hex(2)*' }) {
                            # Expandable String
                            $KeyPropType = 'REG_EXPAND_SZ'
                        }

                        ( { $_ -like 'hex(3)*' }) {
                            # Binary (hex)
                            $KeyPropType = 'REG_BINARY'
                        }

                        ( { $_ -like 'hex:*' }) {
                            # Binary
                            $KeyPropType = 'REG_BINARY'
                        }

                        ( { $_ -like 'hex(4)' }) {
                            # DWORD in little endian format (hex)
                            $KeyPropType = 'REG_DWORD'
                        }

                        ( { $_ -like 'dword*' }) {
                            # DWORD in little endian format
                            $KeyPropType = 'REG_DWORD'
                        }

                        ( { $_ -like 'hex(5)' }) {
                            # DWORD in big endian format
                            $KeyPropType = 'REG_DWORD_BIG_ENDIAN'
                        }

                        ( { $_ -like 'hex(6)' }) {
                            # Symbolic link (UNICODE)
                            $KeyPropType = 'REG_LINK'
                        }

                        ( { $_ -like 'hex(7)*' }) {
                            # Multiple strings, delimited by \0, terminated by \0\0 (ASCII)
                            $KeyPropType = 'REG_MULTI_SZ'
                        }

                        ( { $_ -like 'hex(8)*' }) {
                            # resource list? huh?
                            $KeyPropType = 'REG_RESOURCE_LIST'
                        }

                        ( { $_ -like 'hex(9)*' }) {
                            # full resource descriptor? huh?
                            $KeyPropType = 'REG_FULL_RESOURCE_DESCRIPTOR'
                        }

                        ( { $_ -like 'hex(10)*' }) {                        
                            $KeyPropType = 'REG_RESOURCE_REQUIREMENTS_LIST'
                        }

                        ( { $_ -like 'hex(a)*' }) {                        
                            $KeyPropType = 'REG_RESOURCE_REQUIREMENTS_LIST'
                        }

                        ( { $_ -like 'hex(11)*' }) {
                            # QWORD in little endia format
                            $KeyPropType = 'REG_QWORD'
                        }

                        ( { $_ -like 'hex(b)*' }) {
                            # QWORD in little endia format
                            $KeyPropType = 'REG_QWORD'
                        }                    
                    }
                    
                    #Write-Host "KeyPropType: $KeyPropType"
                    # Special case for multiline property values
                    if ($RegContent[$j][-1] -eq '\') {

                        $commentCounter = 0
                        $emptySpaceCounter = 0
                        # This property spans multiple lines...
                        $KeyMultiLineProp = for ($k = $j + 1; $k -lt $RegContent.Length; $k++) {
                            #Write-Host "Before parse: $($RegContent[$k])"
                            if (($RegContent[$k].Trim() -like '`[HKEY*]') -or ($RegContent[$k].Trim() -like '`[-HKEY*]')) {
                                # This is the start of a new key.
                                break
                            }
                            if ($RegContent[$k].Trim() -like '"*') {
                                # This is the start of a new property.
                                #Write-Host 'Start of a new property. Breaking'
                                break
                            }
                            if ($RegContent[$k].Trim() -like ';*') {

                                # There is a comment in the middle of the multiline value. Ignore the comment
                                # and move to the next line.
                                #Write-Host 'Comment. Incrementing commentCounter and continuing.'
                                $commentCounter++
                                continue
                            }
                            if ($RegContent[$k].Trim() -eq [string]$null) {
                                # Empty space in between multine string. Ignore the line, increment the empty space,
                                # counter, and continue.
                                #Write-Host 'Emtpy line. Incrementing emptySpaceCounter and continuing.'
                                $emptySpaceCounter++
                                continue
                            }

                            #Write-Host "After parse: $($RegContent[$k])"
                            $RegContent[$k]
                        }
                    
                        # Add the lines for the multiline property value into an array
                        $KeyPropValueArray = @($KeyPropValue)
                        foreach ($item in $KeyMultiLineProp) {
                            $KeyPropValueArray += $item
                        }                    

                        $Value = $KeyPropValueArray
                        
                        # Increment the counter so that we're able to move onto the next property or key correctly.
                        $j = $j + $KeyMultiLineProp.Count + $commentCounter + $emptySpaceCounter
                    } else {
                        $Value = $KeyPropValue
                    }

                    switch ($KeyPropType) {
                        ( { $_ -match 'REG_MULTI_SZ|REG_EXPAND_SZ' }) {
                            #Write-Host "Trying to piece together: $Value"
                            
                            $friendlyValueHex = ($Value -join '').Split(':')[1] -replace ('\\\s*', '')
                            #Write-Host "After piecing together: $friendlyValueHex"

                            [byte[]]$bytes = $friendlyValueHex -split ',' | ForEach-Object { [int]"0x$_" }
                            
                            # Convert back to a string and split on [chart]$null to preserve the line breaks from the original value.
                            $friendlyValue = ([Text.Encoding]::Unicode.GetString($bytes)).Split([char]$null)

                        }

                        ( { $_ -eq 'REG_BINARY' }) {
                            $friendlyValueHex = ($Value -join '').Split(':')[1].Replace('\  ', '')

                            if ($friendlyValueHex -ne [string]$null) {
                                [byte[]]$array = $friendlyValueHex -split ',' | ForEach-Object { [int]"0x$_" }

                                $friendlyValue = [pscustomobject]@{ }
                                # https://www.sysadmins.lv/blog-en/convert-data-between-binary-hex-and-base64-in-powershell.aspx
                                #$encodingList = 'Base64Header', 'Base64', 'HexRaw', 'Hex', 'HexAddr', 'HexAscii', 'HexAddrAscii'
                                foreach ($item in $encoding.Keys) {

                                    $pcchString = 0
                                    # call the CryptBinaryToString function to get string length. pszString is $null for the first call
                                    if ([PKI.Crypt32]::CryptBinaryToString($array, $array.Length, $encoding[$item], $null, [ref]$pcchString)) {
                                        # instantiate a StringBuilder object with required size
                                        $SB = New-Object Text.StringBuilder $pcchString
                                        # call the function again and pass StringBuilder into pszString parameter
                                        [void][PKI.Crypt32]::CryptBinaryToString($array, $array.Length, $encoding[$item], $sb, [ref]$pcchString)
                                        # display produced output
                                        #$friendlyValue = $SB.ToString()
                                
                                    } else {
                                        Write-Warning $((New-Object ComponentModel.Win32Exception ([Runtime.InteropServices.Marshal]::GetLastWin32Error())).Message)
                                    }

                                    Add-Member -InputObject $friendlyValue -MemberType NoteProperty -Name $item -Value $SB.ToString()
                                }
                            } else {
                                $friendlyValue = $null
                            }
                        }

                        ( { $_ -eq 'REG_DWORD' }) {
                            $decimal = [Convert]::ToInt32(($Value.Split(':')[1]), 16)
                            $hex = $hex = [Convert]::ToString($decimal, 16)
                            $friendlyValue = [pscustomobject]@{
                                Hex     = $hex
                                Decimal = $decimal
                            }
                        }

                        ( { $_ -eq 'REG_QWORD' }) {
                            
                            $friendlyValueHex = ($Value -join '').Split(':')[1].Replace('\  ', '')
                            $hexArray = $friendlyValueHex -split ','
                            $hexString = $hexArray[-1.. - ($hexArray.Length)] -join ''
                            $decimal = [Convert]::ToInt64($hexString, 16)
                            $hex = [Convert]::ToString($decimal, 16)
                            $friendlyValue = [pscustomobject]@{
                                Hex     = $hex
                                Decimal = $decimal
                            }                        
                        }

                        default { $friendlyValue = $null }
                    }

                    $KeyPropObject = [pscustomobject]@{
                        Name          = $KeyPropName
                        Type          = $KeyPropType
                        Value         = $Value
                        FriendlyValue = $friendlyValue
                        Action        = $KeyPropAction
                    }

                    $KeyPropObject
                }
            }

            Add-Member -InputObject $RegEntry -MemberType NoteProperty -Name 'Properties' -Value $KeyProps
            $RegEntry
        }
    }

    return $RegCol
}
#endregion
#region --------------------------------------------------[Convert-RegistryPath Function]---------------------------------------------------
Function Convert-RegistryPath {
    <#
    .SYNOPSIS
        Converts the specified registry key path to a format that is compatible with built-in PowerShell cmdlets.
    .DESCRIPTION
        Converts the specified registry key path to a format that is compatible with built-in PowerShell cmdlets.
        Converts registry key hives to their full paths. Example: HKLM is converted to "Registry::HKEY_LOCAL_MACHINE".
    .PARAMETER Key
        Path to the registry key to convert (can be a registry hive or fully qualified path)
    .PARAMETER SID
        The security identifier (SID) for a user. Specifying this parameter will convert a HKEY_CURRENT_USER registry key to the HKEY_USERS\$SID format.
        Specify this parameter from the Invoke-HKCURegistrySettingsForAllUsers function to read/edit HKCU registry settings for all users on the system.
    .EXAMPLE
        Convert-RegistryPath -Key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1AD147D0-BE0E-3D6C-AC11-64F6DC4163F1}'
    .EXAMPLE
        Convert-RegistryPath -Key 'HKLM:SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1AD147D0-BE0E-3D6C-AC11-64F6DC4163F1}'
    .NOTES
    .LINK
        http://psappdeploytoolkit.com
    #>
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [string]$Key,
        [Parameter(Mandatory = $false)]
        [ValidateNotNullorEmpty()]
        [string]$SID
    )
        
    Begin {
        ## Get the name of this function and write header
        [string]${CmdletName} = $PSCmdlet.MyInvocation.MyCommand.Name
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -CmdletBoundParameters $PSBoundParameters -Header
    }
    Process {
        ## Convert the registry key hive to the full path, only match if at the beginning of the line
        If ($Key -match '^HKLM:\\|^HKCU:\\|^HKCR:\\|^HKU:\\|^HKCC:\\|^HKPD:\\') {
            #  Converts registry paths that start with, e.g.: HKLM:\
            $key = $key -replace '^HKLM:\\', 'HKEY_LOCAL_MACHINE\'
            $key = $key -replace '^HKCR:\\', 'HKEY_CLASSES_ROOT\'
            $key = $key -replace '^HKCU:\\', 'HKEY_CURRENT_USER\'
            $key = $key -replace '^HKU:\\', 'HKEY_USERS\'
            $key = $key -replace '^HKCC:\\', 'HKEY_CURRENT_CONFIG\'
            $key = $key -replace '^HKPD:\\', 'HKEY_PERFORMANCE_DATA\'
        } ElseIf ($Key -match '^HKLM:|^HKCU:|^HKCR:|^HKU:|^HKCC:|^HKPD:') {
            #  Converts registry paths that start with, e.g.: HKLM:
            $key = $key -replace '^HKLM:', 'HKEY_LOCAL_MACHINE\'
            $key = $key -replace '^HKCR:', 'HKEY_CLASSES_ROOT\'
            $key = $key -replace '^HKCU:', 'HKEY_CURRENT_USER\'
            $key = $key -replace '^HKU:', 'HKEY_USERS\'
            $key = $key -replace '^HKCC:', 'HKEY_CURRENT_CONFIG\'
            $key = $key -replace '^HKPD:', 'HKEY_PERFORMANCE_DATA\'
        } ElseIf ($Key -match '^HKLM\\|^HKCU\\|^HKCR\\|^HKU\\|^HKCC\\|^HKPD\\') {
            #  Converts registry paths that start with, e.g.: HKLM\
            $key = $key -replace '^HKLM\\', 'HKEY_LOCAL_MACHINE\'
            $key = $key -replace '^HKCR\\', 'HKEY_CLASSES_ROOT\'
            $key = $key -replace '^HKCU\\', 'HKEY_CURRENT_USER\'
            $key = $key -replace '^HKU\\', 'HKEY_USERS\'
            $key = $key -replace '^HKCC\\', 'HKEY_CURRENT_CONFIG\'
            $key = $key -replace '^HKPD\\', 'HKEY_PERFORMANCE_DATA\'
        }
            
        If ($PSBoundParameters.ContainsKey('SID')) {
            ## If the SID variable is specified, then convert all HKEY_CURRENT_USER key's to HKEY_USERS\$SID				
            If ($key -match '^HKEY_CURRENT_USER\\') { $key = $key -replace '^HKEY_CURRENT_USER\\', "HKEY_USERS\$SID\" }
        }
            
        ## Append the PowerShell drive to the registry key path
        If ($key -notmatch '^Registry::') { [string]$key = "Registry::$key" }
            
        If ($Key -match '^Registry::HKEY_LOCAL_MACHINE|^Registry::HKEY_CLASSES_ROOT|^Registry::HKEY_CURRENT_USER|^Registry::HKEY_USERS|^Registry::HKEY_CURRENT_CONFIG|^Registry::HKEY_PERFORMANCE_DATA') {
            ## Check for expected key string format
            Write-Log -Path $LogFile -Level Info -Message "Return fully qualified registry key path [$key]."
            Write-Output -InputObject $key
        } Else {
            #  If key string is not properly formatted, throw an error
            Throw "Unable to detect target registry hive in string [$key]."
        }
    }
    End {
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -Footer
    }
}
    
#endregion
#region ----------------------------------------------------[Set-RegistryKey Function]-----------------------------------------------------
Function Set-RegistryKey {
    <#
    .SYNOPSIS
        Creates a registry key name, value, and value data; it sets the same if it already exists.
    .DESCRIPTION
        Creates a registry key name, value, and value data; it sets the same if it already exists.
    .PARAMETER Key
        The registry key path.
    .PARAMETER Name
        The value name.
    .PARAMETER Value
        The value data.
    .PARAMETER Type
        The type of registry value to create or set. Options: 'Binary','DWord','ExpandString','MultiString','None','QWord','String','Unknown'. Default: String.
    .PARAMETER SID
        The security identifier (SID) for a user. Specifying this parameter will convert a HKEY_CURRENT_USER registry key to the HKEY_USERS\$SID format.
        Specify this parameter from the Invoke-HKCURegistrySettingsForAllUsers function to read/edit HKCU registry settings for all users on the system.
    .PARAMETER ContinueOnError
        Continue if an error is encountered. Default is: $true.
    .EXAMPLE
        Set-RegistryKey -Key $blockedAppPath -Name 'Debugger' -Value $blockedAppDebuggerValue
    .EXAMPLE
        Set-RegistryKey -Key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce' -Name 'Debugger' -Value $blockedAppDebuggerValue -Type String
    .EXAMPLE
        Set-RegistryKey -Key 'HKCU\Software\Microsoft\Example' -Name 'Data' -Value (0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x02,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x02,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x00,0x01,0x01,0x01,0x02,0x02,0x02) -Type 'Binary'
    .EXAMPLE
        Set-RegistryKey -Key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Example' -Value '(Default)'
    .NOTES
    .LINK
        http://psappdeploytoolkit.com
    #>
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [string]$Key,
        [Parameter(Mandatory = $false)]
        [ValidateNotNullOrEmpty()]
        [string]$Name,
        [Parameter(Mandatory = $false)]
        $Value,
        [Parameter(Mandatory = $false)]
        [ValidateSet('Binary', 'DWord', 'ExpandString', 'MultiString', 'None', 'QWord', 'String', 'Unknown')]
        [Microsoft.Win32.RegistryValueKind]$Type = 'String',
        [Parameter(Mandatory = $false)]
        [ValidateNotNullorEmpty()]
        [string]$SID,
        [Parameter(Mandatory = $false)]
        [ValidateNotNullOrEmpty()]
        [boolean]$ContinueOnError = $true
    )
        
    Begin {
        ## Get the name of this function and write header
        [string]${CmdletName} = $PSCmdlet.MyInvocation.MyCommand.Name
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -CmdletBoundParameters $PSBoundParameters -Header
    }
    Process {
        Try {
            [string]$RegistryValueWriteAction = 'set'
                
            ## If the SID variable is specified, then convert all HKEY_CURRENT_USER key's to HKEY_USERS\$SID
            If ($PSBoundParameters.ContainsKey('SID')) {
                [string]$key = Convert-RegistryPath -Key $key -SID $SID
            } Else {
                [string]$key = Convert-RegistryPath -Key $key
            }
                
            ## Replace forward slash character to allow forward slash in name of registry key rather than creating new subkey
            $key = $key.Replace('/', "$([char]0x2215)")
                
            ## Create registry key if it doesn't exist
            If (-not (Test-Path -LiteralPath $key -ErrorAction 'Stop')) {
                Try {
                    Write-Log -Path $LogFile -Level Info -Message "Create registry key [$key]."
                    $null = New-Item -Path $key -ItemType 'Registry' -Force -ErrorAction 'Stop'
                } Catch {
                    Throw
                }
            }
                
            If ($Name) {
                ## Set registry value if it doesn't exist
                If (-not (Get-ItemProperty -LiteralPath $key -Name $Name -ErrorAction 'SilentlyContinue')) {
                    Write-Log -Path $LogFile -Level Info -Message "Set registry key value: [$key] [$name = $value]."
                    $null = New-ItemProperty -LiteralPath $key -Name $name -Value $value -PropertyType $Type -ErrorAction 'Stop'
                }
                ## Update registry value if it does exist
                Else {
                    [string]$RegistryValueWriteAction = 'update'
                    If ($Name -eq '(Default)') {
                        ## Set Default registry key value with the following workaround, because Set-ItemProperty contains a bug and cannot set Default registry key value
                        $null = $(Get-Item -LiteralPath $key -ErrorAction 'Stop').OpenSubKey('', 'ReadWriteSubTree').SetValue($null, $value)
                    } Else {
                        Write-Log -Path $LogFile -Level Info -Message "Update registry key value: [$key] [$name = $value]."
                        $null = Set-ItemProperty -LiteralPath $key -Name $name -Value $value -ErrorAction 'Stop'
                    }
                }
            }
        } Catch {
            If ($Name) {
                Write-Log -Path $LogFile -Level Error -Message "Failed to $RegistryValueWriteAction value [$value] for registry key [$key] [$name]. `n$(Resolve-Error)"
                If (-not $ContinueOnError) {
                    Throw "Failed to $RegistryValueWriteAction value [$value] for registry key [$key] [$name]: $($_.Exception.Message)"
                }
            } Else {
                Write-Log -Path $LogFile -Level Error -Message "Failed to set registry key [$key]. `n$(Resolve-Error)"
                If (-not $ContinueOnError) {
                    Throw "Failed to set registry key [$key]: $($_.Exception.Message)"
                }
            }
        }
    }
    End {
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -Footer
    }
}
    
#endregion
#region --------------------------------------------------[Remove-RegistryKey Function]----------------------------------------------------
Function Remove-RegistryKey {
    <#
    .SYNOPSIS
        Deletes the specified registry key or value.
    .DESCRIPTION
        Deletes the specified registry key or value.
    .PARAMETER Key
        Path of the registry key to delete.
    .PARAMETER Name
        Name of the registry value to delete.
    .PARAMETER Recurse
        Delete registry key recursively.
    .PARAMETER SID
        The security identifier (SID) for a user. Specifying this parameter will convert a HKEY_CURRENT_USER registry key to the HKEY_USERS\$SID format.
        Specify this parameter from the Invoke-HKCURegistrySettingsForAllUsers function to read/edit HKCU registry settings for all users on the system.
    .PARAMETER ContinueOnError
        Continue if an error is encountered. Default is: $true.
    .EXAMPLE
        Remove-RegistryKey -Key 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\RunOnce'
    .EXAMPLE
        Remove-RegistryKey -Key 'HKLM:SOFTWARE\Microsoft\Windows\CurrentVersion\Run' -Name 'RunAppInstall'
    .NOTES
    .LINK
        http://psappdeploytoolkit.com
    #>
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true)]
        [ValidateNotNullorEmpty()]
        [string]$Key,
        [Parameter(Mandatory = $false)]
        [ValidateNotNullOrEmpty()]
        [string]$Name,
        [Parameter(Mandatory = $false)]
        [switch]$Recurse,
        [Parameter(Mandatory = $false)]
        [ValidateNotNullorEmpty()]
        [string]$SID,
        [Parameter(Mandatory = $false)]
        [ValidateNotNullOrEmpty()]
        [boolean]$ContinueOnError = $true
    )
        
    Begin {
        ## Get the name of this function and write header
        [string]${CmdletName} = $PSCmdlet.MyInvocation.MyCommand.Name
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -CmdletBoundParameters $PSBoundParameters -Header
    }
    Process {
        Try {
            ## If the SID variable is specified, then convert all HKEY_CURRENT_USER key's to HKEY_USERS\$SID
            If ($PSBoundParameters.ContainsKey('SID')) {
                [string]$Key = Convert-RegistryPath -Key $Key -SID $SID
            } Else {
                [string]$Key = Convert-RegistryPath -Key $Key
            }
                
            If (-not ($Name)) {
                If (Test-Path -LiteralPath $Key -ErrorAction 'Stop') {
                    If ($Recurse) {
                        Write-Log -Path $LogFile -Level Info -Message "Delete registry key recursively [$Key]."
                        $null = Remove-Item -LiteralPath $Key -Force -Recurse -ErrorAction 'Stop'
                    } Else {
                        If ($null -eq (Get-ChildItem -LiteralPath $Key -ErrorAction 'Stop')) {
                            ## Check if there are subkeys of $Key, if so, executing Remove-Item will hang. Avoiding this with Get-ChildItem.
                            Write-Log -Path $LogFile -Level Info -Message "Delete registry key [$Key]."
                            $null = Remove-Item -LiteralPath $Key -Force -ErrorAction 'Stop'
                        } Else {
                            Throw "Unable to delete child key(s) of [$Key] without [-Recurse] switch."
                        }
                    }
                } Else {
                    Write-Log -Path $LogFile -Level Error -Message "Unable to delete registry key [$Key] because it does not exist."
                }
            } Else {
                If (Test-Path -LiteralPath $Key -ErrorAction 'Stop') {
                    Write-Log -Path $LogFile -Level Info -Message "Delete registry value [$Key] [$Name]."
                        
                    If ($Name -eq '(Default)') {
                        ## Remove (Default) registry key value with the following workaround because Remove-ItemProperty cannot remove the (Default) registry key value
                        $null = (Get-Item -LiteralPath $Key -ErrorAction 'Stop').OpenSubKey('', 'ReadWriteSubTree').DeleteValue('')
                    } Else {
                        $null = Remove-ItemProperty -LiteralPath $Key -Name $Name -Force -ErrorAction 'Stop'
                    }
                } Else {
                    Write-Log -Path $LogFile -Level Warn -Message "Unable to delete registry value [$Key] [$Name] because registry key does not exist."
                }
            }
        } Catch [System.Management.Automation.PSArgumentException] {
            Write-Log -Path $LogFile -Level Warn -Message "Unable to delete registry value [$Key] [$Name] because it does not exist."
        } Catch {
            If (-not ($Name)) {
                Write-Log -Path $LogFile -Level Error -Message "Failed to delete registry key [$Key]."
                If (-not $ContinueOnError) {
                    Throw "Failed to delete registry key [$Key]: $($_.Exception.Message)"
                }
            } Else {
                Write-Log -Path $LogFile -Level Error -Message "Failed to delete registry value [$Key] [$Name]."
                If (-not $ContinueOnError) {
                    Throw "Failed to delete registry value [$Key] [$Name]: $($_.Exception.Message)"
                }
            }
        }
    }
    End {
        #Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -Footer
    }
}
#endregion
#region -------------------------------------------------------[Write-Log Function]-------------------------------------------------------
function Write-Log {
    
    <#
    .Synopsis
       Write-Log writes a message to a specified log file with the current time stamp.
    .DESCRIPTION
       The Write-Log function is designed to add logging capability to other scripts.
       In addition to writing output and/or verbose you can write to a log file for
       later debugging.
    .NOTES
       Created by: Jason Wasser @wasserja
       Modified: 11/24/2015 09:30:19 AM  
    
       Changelog:
        * Code simplification and clarification - thanks to @juneb_get_help
        * Added documentation.
        * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
        * Revised the Force switch to work as it should - thanks to @JeffHicks
    
       To Do:
        * Add error handling if trying to create a log file in a inaccessible location.
        * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
          duplicates.
    .PARAMETER Message
       Message is the content that you wish to add to the log file. 
    .PARAMETER Path
       The path to the log file to which you would like to write. By default the function will 
       create the path and file if it does not exist. 
    .PARAMETER Level
       Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
    .PARAMETER NoClobber
       Use NoClobber if you do not wish to overwrite an existing file.
    .EXAMPLE
       Write-Log -Message 'Log message' 
       Writes the message to c:\Logs\PowerShellLog.log.
    .EXAMPLE
       Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
       Writes the content to the specified log file and creates the path and file specified. 
    .EXAMPLE
       Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
       Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
    .LINK
       https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
    #>
    
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,
    
        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path = 'C:\Logs\PowerShellLog.log',
            
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
            
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )
    
    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
            
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }
    
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }
    
        else {
            # Nothing to see here yet.
        }
    
        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    
        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
            
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion
#endregion

#region -----------------------------------------------------------[Execution]------------------------------------------------------------

## VARIABLE DECLARATION

$GetUserFTAPath = "$PSScriptRoot\GetUserFTA.exe"
$SetUserFTAPath = "$PSScriptRoot\SetUserFTA.exe"

if ($DefaultFTAConfigPath) {
    $DefaultFTAConfig = Get-Item -Path $DefaultFTAConfigPath
}

# Pattern to match everything between quotes
$RegexPattern = [regex]::new('(["])(?:(?=(\\?))\2.)*?\1')

## MAIN LOGIC

Try {
    Write-Log -Path $LogFile -Level Info -Message "Script was invoked with Action: $Action."
    
    # Get a list of user's current FTA's on this machine
    #$currentUserFTA = & $GetUserFTAPath

    # Create File Type Associations config directory if it doesn't exist
    Write-Log -Path $LogFile -Level Info -Message "Checking if FTA Config directory exists at '$FTAConfigPath'."
    if (Test-Path -Path $FTAConfigPath -ErrorAction SilentlyContinue) {
        Write-Log -Path $LogFile -Level Info -Message "'$FTAConfigPath' exists."
    } else {
        Write-Log -Path $LogFile -Level Warn -Message "'$FTAConfigPath' does not exist. Creating directory and making it hidden."
        $null = New-Item -Path $FTAConfigPath -ItemType Directory -Force
        $null = Set-ItemProperty -Path $FTAConfigPath -Name Attributes -Value Hidden
    }

    # Create File Type Associations registry directory if it doesn't exist
    Write-Log -Path $LogFile -Level Info -Message "Checking if FTA Registry directory exists at '$FTARegPath'."
    if (Test-Path -Path $FTARegPath -ErrorAction SilentlyContinue) {
        Write-Log -Path $LogFile -Level Info -Message "'$FTARegPath' exists."
    } else {
        Write-Log -Path $LogFile -Level Warn -Message "'$FTARegPath' does not exist. Creating directory and making it hidden."
        $null = New-Item -Path $FTARegPath -ItemType Directory -Force
        $null = Set-ItemProperty -Path $FTARegPath -Name Attributes -Value Hidden
    }

    switch ($Action) {
        'Logon' {
            Write-Log -Path $LogFile -Level Warn -Message "-----------------------------------------------------------[Logon]------------------------------------------------------------"

            # Check if there is a SetUserFTA.config file to import.
            Write-Log -Path $LogFile -Level Info -Message "Checking if SetUserFTA.config exists in '$FTAConfigPath'."
            $FTAConfig = Get-Item -Path "$FTAConfigPath\SetUserFTA.config" -ErrorAction SilentlyContinue

            if ($FTAConfig -and ($FTAConfig.LastWriteTime -ge $CutoffDate)) {
                # SetUserFTA.config exists and last modify date is after CutoffDate. We're going to import it in order to set User File Type Associations.
                Write-Log -Path $LogFile -Level Info -Message "Found SetUserFTA.config and its last modify date is after $CutoffDate."

                # Grab a list of the registry files in $FTARegPath
                $RegFileList = Get-ChildItem -Path $FTARegPath -Filter '*.reg'
                # Check if the Application exists on the system.
                foreach ($RegFile in $RegFileList) {
                    Write-Log -Path $LogFile -Level Info -Message "Found the following registry file: $($RegFile.Name)"

                    # Set Import flag to false when starting to parse a reg file.
                    $ImportFlag = $false

                    # Parse the .reg files to get a list of non-empty keys to import.
                    $RegKeyList = Get-RegFileContent -Path $RegFile.FullName

                    foreach ($RegKey in ($RegKeyList | Where-Object { $_.Properties })) {
                        # Break if file already marked for import.
                        if ($ImportFlag) { Write-Log -Path $LogFile -Level Warn -Message "'$($RegFile.Name)' has already been marked for import. Moving on..."; break }

                        Write-Log -Path $LogFile -Level Info -Message "Found the following registry key with properties: $($RegKey.Key)"
                        foreach ($RegKeyProperty in $RegKey.Properties) {
                            # Break if file already marked for import.
                            if ($ImportFlag) { Write-Log -Path $LogFile -Level Warn -Message "'$($RegFile.Name)' has already been marked for import. Moving on..."; break }

                            Write-Log -Path $LogFile -Level Info -Message "Parsing property value: $($RegKeyProperty.value)"

                            # Parse value with the regex pattern. The pattern will grab anything between two quotes.
                            $StringMatchList = [regex]::Matches($RegKeyProperty.Value, $RegexPattern).Value.Replace('"', '')

                            foreach ($StringMatch in $StringMatchList) {
                                # Break if file already marked for import.
                                if ($ImportFlag) { Write-Log -Path $LogFile -Level Warn -Message "'$($RegFile.Name)' has already been marked for import. Moving on..."; break }

                                Write-Log -Path $LogFile -Level Info -Message "Checking if '$StringMatch' is an Application path that exists on the system."

                                if (Test-Path -Path $StringMatch) {
                                    Write-Log -Path $LogFile -Level Warn -Message "'$StringMatch' exists on the system. Flagging $($RegFile.Name) for import."
                                    
                                    $ImportFlag = $true
                                    break
                                } else {
                                    Write-Log -Path $LogFile -Level Warn -Message "'$StringMatch' is either not an Application path or the path does not exist on the system."
                                }
                            }
                        }
                    }

                    # Import if flag is set.
                    if ($ImportFlag) {
                        Write-Log -Path $LogFile -Level Info -Message "'$($RegFile.Name)' is marked for import. Importing reg key."
                        #Set-RegKey -RegKeys $RegKeyList
                        
                        foreach ($RegKey in $RegKeyList) {
                            # Format key so that Powershell can set it with New-Item.
                            $FormattedKey = $RegKey.Key -Replace ('\[', '') -replace ('\]', '')
                        
                            foreach ($RegKeyProperty in $RegKey.Properties) {
                                Write-Log -Path $LogFile -Level Info -Message "Setting '$($RegKeyProperty.Name)' to '$($RegKeyProperty.Value)' in '$($RegKey.Key)'."
                                $null = Set-RegistryKey -Key $FormattedKey -Name $RegKeyProperty.Name -Value $RegKeyProperty.Value 
                            }                            
                        }
                        #>
                    }
                }

                # Import SetUserFTA.config.
                Write-Log -Path $LogFile -Level Info -Message "Setting FTAs using SetUserFTA.config."
                $null = & $SetUserFTAPath "$FTAConfigPath\SetUserFTA.config"
            } else {
                # SetUserFTA.config doesn't exist or the last modify date is before the CutoffDate, so we need to create an initial config file.
                Write-Log -Path $LogFile -Level Warn -Message "SetUserFTA.config does not exist or the last modify date is before the CutoffDate. There is nothing to import. Creating baseline SetUserFTA.config."
                
                if ($DefaultFTAConfig) {
                    # DefaultFTAConfigPath was specified and resolves, we're going to set FTAs to this default.
                    Write-Log -Path $LogFile -Level Info -Message "DefaultFTAConfigPath was specified. Setting FTAs from '$($DefaultFTAConfig.FullName)'."

                    # Import SetUserFTA.config
                    $null = & $SetUserFTAPath $DefaultFTAConfig.FullName
                }
                
                $UserFTAContent = & $GetUserFTAPath

                # Parse the UserFTA config content to see if any of the ProgIDs are in the form of Applications\Application.exe. If this is the case,
                # the associated registry entries need to be exported from HKCU:\SOFTWARE\Classes\Applications.
                Write-Log -Path $LogFile -Level Info -Message "Parsing UserFTA config content to see if associated registry entries need to be exported."
                $ProgIDToExportList = foreach ($ProgID in $UserFTAContent) {
                    $FileExtension = $ProgID.split(',')[0].Trim()
                    $FileProgID = $ProgID.split(',')[1].Trim()

                    if ($FileProgID -like 'Applications\*') {
                        # Adding this entry to the list.
                        Write-Log -Path $LogFile -Level Info -Message "File Extension '$FileExtension' has ProgID '$FileProgID'. Adding to list of registry keys that will need to be exported."
                        $ProgID
                    }
                }

                # Export reg keys
                foreach ($ProgIDToExport in $ProgIDToExportList) {
                    $Extension = $ProgIDToExport.split(',')[0].Trim()
                    $ProgID = $ProgIDToExport.split(',')[1].Trim()
                    $Application = $ProgIDToExport.split('\')[1].Trim()
                    $RegKey = "HKCU\SOFTWARE\Classes\Applications\$Application"

                    Write-Log -Path $LogFile -Level Info -Message "Creating '$Application.reg' in '$FTARegPath'."
                    # Export the registry entry if it doesn't already exist.
                    if (!(Test-Path -Path "$FTARegPath\$Application.reg")) {
                        $null = REG EXPORT $RegKey "$FTARegPath\$Application.reg" /y
                    } else {
                        Write-Log -Path $LogFile -Level Warn -Message "'$Application.reg' already exists in '$FTARegPath'. Skipping."
                    }
                }

                # Export UserFTA config content
                Write-Log -Path $LogFile -Level Info -Message "Creating 'SetUserFTA.config' in '$FTAConfigPath'."
                # Encoding has to be Default because the Application can't read config files with Byte Order Marks (BOM)
                $null = $UserFTAContent | Out-File -FilePath "$FTAConfigPath\SetUserFTA.config" -Encoding Default -Force
				
                # Run SetUserFTA.exe to set the defaults
                $null = & $SetUserFTAPath "$FTAConfigPath\SetUserFTA.config"
            }
        }

        'Logoff' {
            Write-Log -Path $LogFile -Level Warn -Message "-----------------------------------------------------------[Logoff]------------------------------------------------------------"

            # Export SetUserFTA.config
            Write-Log -Path $LogFile -Level Info -Message "Running GetUserFTA to create a config to export."
            $UserFTAContent = & $GetUserFTAPath
            
            # Parse the UserFTA config content to see if any of the ProgIDs are in the form of Applications\Application.exe. If this is the case,
            # the associated registry entries need to be exported from HKCU:\SOFTWARE\Classes\Applications.
            Write-Log -Path $LogFile -Level Info -Message "Parsing UserFTA config content to see if associated registry entries need to be exported."
            $ProgIDToExportList = foreach ($ProgID in $UserFTAContent) {
                $FileExtension = $ProgID.split(',')[0].Trim()
                $FileProgID = $ProgID.split(',')[1].Trim()
            
                if ($FileProgID -like 'Applications\*') {
                    # Adding this entry to the list.
                    Write-Log -Path $LogFile -Level Info -Message "File Extension '$FileExtension' has ProgID '$FileProgID'. Adding to list of registry keys that will need to be exported."
                    $ProgID
                }
            }
            
            # Export reg keys
            foreach ($ProgIDToExport in $ProgIDToExportList) {
                $Extension = $ProgIDToExport.split(',')[0].Trim()
                $ProgID = $ProgIDToExport.split(',')[1].Trim()
                $Application = $ProgIDToExport.split('\')[1].Trim()
                $RegKey = "HKCU\SOFTWARE\Classes\Applications\$Application"
            
                Write-Log -Path $LogFile -Level Info -Message "Creating '$Application.reg' in '$FTARegPath'."
                # Export the registry entry if it doesn't already exist.
                if (!(Test-Path -Path "$FTARegPath\$Application.reg")) {
                    $null = REG EXPORT $RegKey "$FTARegPath\$Application.reg" /y
                } else {
                    Write-Log -Path $LogFile -Level Warn -Message "'$Application.reg' already exists in '$FTARegPath'. Skipping."
                }
            }

            # Export UserFTA config content
            Write-Log -Path $LogFile -Level Info -Message "Creating 'SetUserFTA.config' in '$FTAConfigPath'."
            # Encoding has to be Default because the Application can't read config files with Byte Order Marks (BOM)
            $null = $UserFTAContent | Out-File -FilePath "$FTAConfigPath\SetUserFTA.config" -Encoding Default
        }
    }
} Catch {
    Write-Log -Path $LogFile -Level Error -Message "Error: $($_.Exception)"
    Break
}
if ($pscmdlet.ShouldProcess("Target of action", "Action will happen")) {
    #do action
} else {
    #don't do action but describe what would have been done
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
}

#endregion