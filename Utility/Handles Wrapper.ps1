﻿# Wrapper for handle.exe so that the data is returned as an object instead of a string
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$SearchString = 'PDQInv'
)

$HandlesUnFormatted = Invoke-Expression -Command "cmd.exe /c handle.exe -accepteula $SearchString" | Select-Object -Skip 5 # First 5 lines are gunf we don't need
$HandlesUnFormatted = $HandlesUnFormatted | Where-Object {$_ -ne ''}

$Handles = $HandlesUnFormatted | Select-Object -Property @{ N = "Process"; E = { ($_ -split '(?<!:)\s+')[0] } },
                                                   @{ N = "ID"; E = { (($_ -split '(?<!:)\s+')[1] -replace 'pid:\s*') } },
                                                   @{ N = "Type"; E = { ($_ -split '(?<!:)\s+')[2] -replace 'type:\s*' } },
                                                   @{ N = "Path"; E = { ($_ -split '(?<!:)\s+')[3] -replace '^[^ ]*: ' } },
                                                   @{ N = "Handle"; E = { (($_ -split '(?<!:)\s+')[3] -split ':')[0] } }

$Handles