﻿#Requires -Version 4
<#
.SYNOPSIS
  This script is designed to perform a machine reboot during a specified time range.
.DESCRIPTION
  The script will only invoke a reboot if the script is run between the time specified in RebootStartTime and
  RebootEndTime. If the computer has already been rebooted during the specified time range, the script will 
  not invoke another reboot.
.PARAMETER RebootStartTime
  Specifies the valid time of day after which the reboot may occur. Default is 6:00AM.
.PARAMETER RebootEndTime
  Specifies the valid time of day after which the reboot may NOT occur. Default is 6:59AM. 
.PARAMETER LogFile
  Path to the log file including the file name. Default is "$env:windir\temp\$($ScriptName).log"
.NOTES
  Version:        1.0
  Author:         Max Bado
  Creation Date:  02/04/2019
  Purpose/Change: Initial script development
.EXAMPLE
  Run the script so that it attempts to reboot the machine only between 5:00AM and 5:30AM and specify a
  custom Log file path.
  .\Invoke-DailyReboot.ps1 -RebootStartTime '5:00AM' -RebootEndTime '5:30AM' -LogFile "$env:temp\ScriptLog.log"
  
  Run the script with all defaults.
  .\Invoke-DailyReboot.ps1
#>

[CmdletBinding()]
#[CmdletBinding(SupportsShouldProcess=$true)] # Use if you plan on accepting -whatif switch
#region ---------------------------------------------------------[Script Parameters]------------------------------------------------------
# https://msdn.microsoft.com/en-us/powershell/reference/5.1/microsoft.powershell.core/about/about_functions_advanced_parameters
Param (
    [Parameter(Mandatory = $false)][datetime]$RebootStartTime = '6:00AM',
    [Parameter(Mandatory = $false)][datetime]$RebootEndTime = '6:59AM',
    [Parameter(Mandatory = $false)][Alias('LogPath')][string]$LogFile
)
#endregion
#region ----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here
$ErrorActionPreference = 'Continue'
$ScriptName = (Get-Item -Path $MyInvocation.MyCommand.Source).BaseName

# Set LogFile Path if it wasn't specified.
if (!$LogFile) {
    $LogFile = "$env:windir\Temp\$($ScriptName).log"
} else {
    # Check to make sure running account has permission to write to log directory
    Try {
        [io.file]::OpenWrite($LogFile).Close()
    } Catch {
        $OldLogFile = $LogFile
        $LogFile = "$env:windir\Temp\$($ScriptName)_$($env:USERNAME).log"
        Write-Error -Path $LogFile -Level Info -Message "Unable to write to $OldLogFile. New Log File location: $LogFile."
    }    
}

#endregion

#region -----------------------------------------------------------[Functions]------------------------------------------------------------
#region --------------------------------------------------[Event Log Write-Log Function]--------------------------------------------------

function Write-Log {

    <#
.Synopsis
   Write-Log writes a message to a specified log file with the current time stamp.
.DESCRIPTION
   The Write-Log function is designed to add logging capability to other scripts.
   In addition to writing output and/or verbose you can write to a log file for
   later debugging.
.NOTES
   Created by: Jason Wasser @wasserja
   Modified: 11/24/2015 09:30:19 AM  

   Changelog:
    * Code simplification and clarification - thanks to @juneb_get_help
    * Added documentation.
    * Renamed LogPath parameter to Path to keep it standard - thanks to @JeffHicks
    * Revised the Force switch to work as it should - thanks to @JeffHicks

   To Do:
    * Add error handling if trying to create a log file in a inaccessible location.
    * Add ability to write $Message to $Verbose or $Error pipelines to eliminate
      duplicates.
.PARAMETER Message
   Message is the content that you wish to add to the log file. 
.PARAMETER Path
   The path to the log file to which you would like to write. By default the function will 
   create the path and file if it does not exist. 
.PARAMETER Level
   Specify the criticality of the log information being written to the log (i.e. Error, Warning, Informational)
.PARAMETER NoClobber
   Use NoClobber if you do not wish to overwrite an existing file.
.EXAMPLE
   Write-Log -Message 'Log message' 
   Writes the message to c:\Logs\PowerShellLog.log.
.EXAMPLE
   Write-Log -Message 'Restarting Server.' -Path c:\Logs\Scriptoutput.log
   Writes the content to the specified log file and creates the path and file specified. 
.EXAMPLE
   Write-Log -Message 'Folder does not exist.' -Path c:\Logs\Script.log -Level Error
   Writes the message to the specified log file as an error message, and writes the message to the error pipeline.
.LINK
   https://gallery.technet.microsoft.com/scriptcenter/Write-Log-PowerShell-999c32d0
#>

    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true,
            ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias("LogContent")]
        [string]$Message,

        [Parameter(Mandatory = $false)]
        [Alias('LogPath')]
        [string]$Path,
        
        [Parameter(Mandatory = $false)]
        [ValidateSet("Error", "Warn", "Info")]
        [string]$Level = "Info",
        
        [Parameter(Mandatory = $false)]
        [switch]$NoClobber
    )

    Begin {
        # Set VerbosePreference to Continue so that verbose messages are displayed.
        $VerbosePreference = 'Continue'
    }
    Process {
        
        # If the file already exists and NoClobber was specified, do not write to the log.
        if ((Test-Path $Path) -AND $NoClobber) {
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name."
            Return
        }

        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path.
        elseif (!(Test-Path $Path)) {
            Write-Verbose "Creating $Path."
            $NewLogFile = New-Item $Path -Force -ItemType File
        }

        else {
            # Nothing to see here yet.
        }

        # Format Date for our Log File
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss"

        # Write message to error, warning, or verbose pipeline and specify $LevelText
        switch ($Level) {
            'Error' {
                Write-Error $Message
                $LevelText = 'ERROR:'
            }
            'Warn' {
                Write-Warning $Message
                $LevelText = 'WARNING:'
            }
            'Info' {
                Write-Verbose $Message
                $LevelText = 'INFO:'
            }
        }
        
        # Write log entry to $Path
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append
    }
    End {
    }
}
#endregion

#region -----------------------------------------------------------[Execution]------------------------------------------------------------

#$WaitSeconds = 30 # Amount of seconds to wait between checks during the wait loop
#$TotalWaitSeconds = 0 # Counter for how long we've been waiting for the reboot
#$CheckinIntervalSeconds = 300 # Interval between "keep alive" messages written to the log
#$RebootEndTime = $RebootStartTime.AddMinutes($MaximumOffsetMinutes)
$CurrentTime = Get-Date
$LastBootUpTime = (Get-CimInstance -ClassName Win32_OperatingSystem).LastBootUpTime

Write-Log -Path $LogFile -Level Warn -Message "START $ScriptName"
Try {    
    Write-Log -Path $LogFile -Level Info -Message "RebootStartTime: $RebootStartTime"
    Write-Log -Path $LogFile -Level Info -Message "RebootEndTime: $RebootEndTime"
    Write-Log -Path $LogFile -Level Info -Message "CurrentTime: $CurrentTime"
    Write-Log -Path $LogFile -Level Info -Message "LastBootUpTime: $LastBootUpTime"

    if(($CurrentTime -lt $RebootStartTime) -or ($CurrentTime -gt $RebootEndTime)) {
        # The reboot falls outside of the specified timeframe. Ending script.
        Write-Log -Path $LogFile -Level Warn -Message "The CurrentTime, $(Get-Date), falls outside of the valid time range, between $RebootStartTime and $RebootEndTime. Ending script."
        break
    }

    if(($LastBootUpTime -gt $RebootStartTime) -and ($LastBootUpTime -lt $RebootEndTime)) {
        # Machine has already been rebooted during the specified timeframe
        Write-Log -Path $LogFile -Level Warn -Message "Machine has been rebooted at $LastBootUpTime which falls within the valid time range, between $RebootStartTime and $RebootEndTime. Ending script."
        break
    }

    Write-Log -Path $LogFile -Level Warn -Message "The CurrentTime, $(Get-Date), falls between the RebootStartTime, $RebootStartTime, and the RebootEndTime, $RebootEndTime."
    Write-Log -Path $LogFile -Level Warn -Message "The machine's LastBootUpTime, $LastBootUpTime, is outside the given start and end time. Rebooting the machine."    
    Write-Log -Path $LogFile -Level Warn -Message "END $ScriptName"
} Catch {
    Write-Log -Path $LogFile -Level Error -Message "Error: $($_.Exception)"
    Break
}

#clean up any variables, closing connection to databases, or exporting data
If ($?) {
    Write-Log -Path $LogFile -Level Info -Message 'Completed Successfully.'
    Restart-Computer -Force
} else {
    Write-Log -Path $LogFile -Level Warn -Message 'Completed with Errors.'
}