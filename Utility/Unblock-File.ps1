﻿# Test files to see if they have the Alternate Streams property and unblock them if they do.
# This script is not generalized.
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String]$DirectoryPath = '\\hercules\iso$'
)

$rootDirs = Get-ChildItem -Path $DirectoryPath -Directory

Add-Type -Namespace Win32 -Name PInvoke -MemberDefinition @"
        // http://msdn.microsoft.com/en-us/library/windows/desktop/aa363915(v=vs.85).aspx
        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteFile(string name);
        public static int Win32DeleteFile(string filePath) {
            bool is_gone = DeleteFile(filePath); return Marshal.GetLastWin32Error();}
 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetFileAttributes(string lpFileName);
        public static bool Win32FileExists(string filePath) {return GetFileAttributes(filePath) != -1;}
"@


[System.Collections.ArrayList]$colBlockedFiles = @()

foreach($item in $rootDirs) {

    "Working on $($item.FullName)"

    Get-ChildItem -Path $item.FullName -Recurse -Filter '*.ps1' | ForEach-Object {
        
        if(($_.PSChildName -eq 'AppDeployToolkitHelp.ps1') -or 
        ($_.PSChildName -eq 'AppDeployToolkitMain.ps1') -or
        ($_.PSChildName -like 'Deploy-Application*.ps1')) {
            
            if([Win32.PInvoke]::Win32FileExists($_.FullName + ':Zone.Identifier')) {

                "$($_.FullName) appears to be blocked."
                $null = $colBlockedFiles.Add($_.FullName)
            }

            #Unblock-File -Path $_.FullName -Verbose
        }
    }
}