﻿# Fix issue where Group Policy won't update
[CmdletBinding()]
param (
    [Parameter(Mandatory = $false)]
    [ValidateNotNullOrEmpty()]
    [String[]]$ComputerName = $env:COMPUTERNAME
)

Invoke-Command -ComputerName $ComputerName -ScriptBlock {
    Remove-Item -Path "$env:windir\System32\GroupPolicy\Machine\Registry.pol" -Force

    gpupdate /force
    
    Restart-Computer -Force
}
